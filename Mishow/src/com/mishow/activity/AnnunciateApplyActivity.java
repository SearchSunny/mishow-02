package com.mishow.activity;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.net.HttpUrlConstant;
import com.mishow.net.NetworkUtil;
import com.mishow.preferenses.SharedPreferencesUtil;
import com.mishow.request.core.Response;
import com.mishow.request.listener.OnResponseListener;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.Constants;
import com.mishow.utils.ToastUtil;
import com.mishow.volley.http.request.FastJsonRequest;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：通告 报名
 */
public class AnnunciateApplyActivity extends BaseActivity {

	private Button btn_back;
	private TextView title_tv;
	
	private Button btn_apply;
	
	private TextView text_annun_title;
	/** 报名理由 **/
	private EditText edit_apply_desc;
	
	private SharedPreferencesUtil mShPreferencesUtil;
	
	private String mAnnouncementId;
	private String mAnnouncementName;
	
	public static void intoActivity(Context context,String announcementId,String announcName){
		
		Intent intent = new Intent(context,AnnunciateApplyActivity.class);
		intent.putExtra(Constants.ANNOUN_ID, announcementId);
		intent.putExtra(Constants.ANNOUN_NAME, announcName);
		context.startActivity(intent);
		
	}
	
	@Override
	protected void initData() {
		
		mShPreferencesUtil = new SharedPreferencesUtil(mContext);
		mAnnouncementId = getIntent().getStringExtra(Constants.ANNOUN_ID);
		mAnnouncementName = getIntent().getStringExtra(Constants.ANNOUN_NAME);
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		setContentView(R.layout.activity_annunciate_apply);
		title_tv = (TextView)findViewById(R.id.title_tv);
		btn_back =  (Button)findViewById(R.id.btn_back);
		
		text_annun_title = (TextView)findViewById(R.id.text_annun_title);
		
		edit_apply_desc = (EditText)findViewById(R.id.edit_apply_desc);
		btn_apply = (Button)findViewById(R.id.btn_apply);
		
	}

	@Override
	protected void setAttribute() {
		
		title_tv.setText("我要报名");
		text_annun_title.setText(mAnnouncementName);
		btn_back.setOnClickListener(onClickListener);
		btn_apply.setOnClickListener(onClickListener);
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_apply://我要报名
				if (edit_apply_desc.length() > 0) {
					doSaveAnnunciateApply();
				}else{
					ToastUtil.show(mContext,"请输入报名理由");
				}
				break;
			default:
				break;
			}
			
		}
	};
	
	
	/**
	 * 通告报名
	 */
	private void doSaveAnnunciateApply(){
		
		if (!AndroidUtil.isNetworkAvailable(this)) {
			ToastUtil.show(mContext,
					getResources().getString(R.string.no_connector));
			return;
		}
		showProgress("", "", true);
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", mShPreferencesUtil.getUserId());
		params.put("announcementId",mAnnouncementId);//通告ID
		params.put("enlist_text",edit_apply_desc.getText().toString().trim());//报名理由
		

		Map<String, String> lastParams = NetworkUtil.generaterPostRequestParams(mContext, params);

		String postUrl = HttpUrlConstant.getPostUrl(this,HttpUrlConstant.SAVE_ANNOUNCEMENT_ENLIST);
		OnResponseListener<Response> listener = new OnResponseListener<Response>(this) {

			@Override
			public void onSuccess(Response response) {

				ToastUtil.show(mContext, response.getBody().getMessage());
				finish();
			}

			@Override
			public void onError(String code, String message) {

				ToastUtil.show(mContext, message);
			}

			@Override
			public void onCompleted() {

				dismissProgress();
			}
		};
		executeRequest(new FastJsonRequest<Response>(
				Request.Method.POST, postUrl, Response.class,
				listener, listener).setParams(lastParams));
		
	}

}
