package com.mishow.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.adapter.NearbyRedsFragmentAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：通告 报名列表
 */
public class AnnunciateApplyListActivity extends BaseActivity {

	private Button btn_back;
	private TextView title_tv;
	
	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_applys;
	/** 数据为空 **/
	private EmptyDataLayout ll_applys_empty;
	/** 报名列表 **/
	private MyListView lv_applys;
	/** 报名列表Adapter **/
	private NearbyRedsFragmentAdapter mRedsFragmentAdapter;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,AnnunciateApplyListActivity.class);
		context.startActivity(intent);
		
	}
	
	
	@Override
	protected void initData() {
		
		mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 15; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mRedsFragmentAdapter = new NearbyRedsFragmentAdapter(mContext, mRedsBeans);
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_annunciate_list);
		title_tv = (TextView)findViewById(R.id.title_tv);
		btn_back =  (Button)findViewById(R.id.btn_back);
		
		refreshLayout_applys = (PullPushRefreshLayout)findViewById(R.id.refreshLayout_applys);
		ll_applys_empty = (EmptyDataLayout)findViewById(R.id.ll_applys_empty);
		lv_applys = (MyListView)findViewById(R.id.lv_applys);
		
	}

	@Override
	protected void setAttribute() {
		title_tv.setText("报名列表");
		
		btn_back.setOnClickListener(onClickListener);
		lv_applys.setAdapter(mRedsFragmentAdapter);
	}
	
	
private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			
			default:
				break;
			}
			
		}
	};

}
