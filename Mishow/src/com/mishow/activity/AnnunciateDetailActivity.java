package com.mishow.activity;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.bean.Announcement;
import com.mishow.bean.AnnouncementDetails;
import com.mishow.net.HttpUrlConstant;
import com.mishow.net.NetworkUtil;
import com.mishow.request.listener.OnResponseListener;
import com.mishow.request.response.AnnouncementDetailsResponse;
import com.mishow.request.response.AnnouncementResponse;
import com.mishow.request.response.AnnouncementResponse.Result;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.Constants;
import com.mishow.utils.ToastUtil;
import com.mishow.volley.http.request.FastJsonRequest;
import com.mishow.widget.share.ShareDialog;
import com.mishow.widget.share.ShareView;
import com.mishow.widget.share.ShareUtil.ShareListener;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：通告详情
 */
public class AnnunciateDetailActivity extends BaseActivity {

	private Button btn_back;
	private TextView title_tv;
	/** 分享 **/
	private ImageButton btn_right;
	/** 我要报名 **/
	private Button btn_to_apply;
	/** 报名人数 **/
	private RelativeLayout relative_apply_number;
	
	/** 通告名称 **/
	private TextView text_annun_name;
	
	/**  通告ID **/
	private String mAnnouncementId;
	
	public static void intoActivity(Context context,String announcementId){
		
		Intent intent = new Intent(context,AnnunciateDetailActivity.class);
		intent.putExtra(Constants.ANNOUN_ID, announcementId);
		context.startActivity(intent);
	}
	
	
	@Override
	protected void initData() {
		if (getIntent() != null) {
			mAnnouncementId = getIntent().getStringExtra(Constants.ANNOUN_ID);
		}
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		setContentView(R.layout.activity_annunciate_detail);
		title_tv = (TextView)findViewById(R.id.title_tv);
		btn_back =  (Button)findViewById(R.id.btn_back);
		btn_right = (ImageButton)findViewById(R.id.btn_right);
		
		text_annun_name = (TextView)findViewById(R.id.text_annun_name);
		
		relative_apply_number = (RelativeLayout)findViewById(R.id.relative_apply_number);
		
		btn_to_apply = (Button)findViewById(R.id.btn_to_apply);
	}

	@Override
	protected void setAttribute() {
		title_tv.setText("通告详情");
		btn_back.setOnClickListener(onClickListener);
		btn_right.setOnClickListener(onClickListener);
		btn_right.setVisibility(View.VISIBLE);
		btn_right.setImageResource(R.drawable.icon_share);
		btn_to_apply.setOnClickListener(onClickListener);
		
		relative_apply_number.setOnClickListener(onClickListener);
		
		getAnnouncementFromApi(mAnnouncementId);
	}
	
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_right: //分享
				share();
				break;
			case R.id.btn_to_apply://我要报名
				String annun_name = text_annun_name.getText().toString().trim();
				AnnunciateApplyActivity.intoActivity(mContext,mAnnouncementId,annun_name);
				break;
			case R.id.relative_apply_number://报名列表
				AnnunciateApplyListActivity.intoActivity(mContext);
				break;
			default:
				break;
			}
			
		}
	};
	
	
	/**
	 * 服务端获取通告详情信息
	 * @param announcementId 通告ID
	 */
	private void getAnnouncementFromApi(String announcementId) {

		if (!AndroidUtil.isNetworkAvailable(this)) {
			ToastUtil.show(mContext,
					getResources().getString(R.string.no_connector));
			return;
		}
		showProgress("", "", true);
		Map<String, String> params = new HashMap<String, String>();
		params.put("announcementId", announcementId);

		Map<String, String> lastParams = NetworkUtil.generaterPostRequestParams(mContext, params);

		String postUrl = HttpUrlConstant.getPostUrl(this,HttpUrlConstant.GET_QUERY_ANNOUNCEMENT_DETAILS);
		OnResponseListener<AnnouncementDetailsResponse> listener = new OnResponseListener<AnnouncementDetailsResponse>(this) {

			@Override
			public void onSuccess(AnnouncementDetailsResponse response) {

				AnnouncementDetails result = response.getBody().getResult();
				if (result != null) {
					setDisplayData(result);
				}
			}

			@Override
			public void onError(String code, String message) {

				ToastUtil.show(mContext, message);
			}

			@Override
			public void onCompleted() {
				dismissProgress();
			}
		};
		executeRequest(new FastJsonRequest<AnnouncementDetailsResponse>(
				Request.Method.POST, postUrl, AnnouncementDetailsResponse.class,
				listener, listener).setParams(lastParams));
	}
	/**
	 * 设置显示数据
	 */
	private void setDisplayData(AnnouncementDetails detail){
		
		text_annun_name.setText(detail.getAnnouncementTheme());
	}
	
	private ShareDialog shareDialog;// 分享对话框
	/**
	 * 显示分享对话框
	 */
	private void share() {
		if (shareDialog == null) {
			shareDialog = new ShareDialog(this, ShareView.TYPE_SHARE_APP);
			shareDialog.setShareContent("title", "content", "http://www.baidu.com", "http://pic74.nipic.com/file/20150806/20968868_172407221000_2.jpg");
			shareDialog.setShareListener(new ShareListener() {
				
				@Override
				public void shareCallBack(SHARE_MEDIA platform, int result, String message) {
					
					ToastUtil.show(mContext, message);
				}
			});
		}
		shareDialog.show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
	}
}
