package com.mishow.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.android.volley.Request;
import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.fragment.DatePickerFragment;
import com.mishow.net.HttpUrlConstant;
import com.mishow.net.NetworkUtil;
import com.mishow.preferenses.SharedPreferencesUtil;
import com.mishow.request.core.Response;
import com.mishow.request.listener.OnResponseListener;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.Constants;
import com.mishow.utils.ToastUtil;
import com.mishow.volley.http.request.FastJsonRequest;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：发布通告
 */
@SuppressLint("SimpleDateFormat")
public class AnnunciateReleaseActivity extends BaseActivity implements
						DatePickerFragment.DialogCallBackListener
{

	private Button btn_back;
	private TextView title_tv;
	
	/** 发布 **/
	private Button btn_release;
	
	private EditText edit_title;
	
	private LinearLayout linear_end_time;
	private TextView text_end_time;
	/** 报名截止时间 **/
	private String mInitEndTime;
	
	private ArrayList<String> skill_types;
	/** 才艺类型 **/
	private TextView text_skill_type;
	
	/** 工作地点城市 **/
	private TextView text_city_location;
	private TextView text_location_addres;
	/** 详细地址 **/
	private RelativeLayout relative_locatioin;
	/** 性别要求 **/
	private TextView text_sex;
	
	private EditText edit_person_number;
	/** 结账方式 **/
	private TextView text_pay_type;
	
	private EditText edit_reward;
	private TextView text_reward_select;
	/** 是否面试 **/
	private TextView text_interview;
	
	private EditText edit_traffic;
	
	private EditText edit_synopsis;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	private SharedPreferencesUtil mShPreferencesUtil;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,AnnunciateReleaseActivity.class);
		context.startActivity(intent);
	}
	
	@Override
	protected void initData() {
		//默认为当前时间 
		//mInitEndTime = dateFormat.format(new Date());
		mShPreferencesUtil = new SharedPreferencesUtil(mContext);
		skill_types = new ArrayList<String>();
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		// 不显示程序的标题栏
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
		// 不显示系统的标题栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
				
		setContentView(R.layout.activity_annunciaterelease);
		title_tv = (TextView)findViewById(R.id.title_tv);
		btn_back =  (Button)findViewById(R.id.btn_back);
		
		edit_title = (EditText)findViewById(R.id.edit_title);
		
		linear_end_time = (LinearLayout)findViewById(R.id.linear_end_time);
		text_end_time = (TextView) findViewById(R.id.text_end_time);
		
		text_skill_type = (TextView) findViewById(R.id.text_skill_type);
		
		text_city_location = (TextView)findViewById(R.id.text_city_location);
		
		text_location_addres = (TextView) findViewById(R.id.text_location_addres);
		relative_locatioin =  (RelativeLayout)findViewById(R.id.relative_locatioin);
		
		text_sex = (TextView)findViewById(R.id.text_sex);
		edit_person_number = (EditText)findViewById(R.id.edit_person_number);
		
		text_pay_type = (TextView)findViewById(R.id.text_pay_type);
		
		edit_reward = (EditText)findViewById(R.id.edit_reward);
		text_reward_select = (TextView)findViewById(R.id.text_reward_select);
		
		text_interview = (TextView)findViewById(R.id.text_interview);
		
		edit_traffic = (EditText)findViewById(R.id.edit_traffic);
		edit_synopsis = (EditText)findViewById(R.id.edit_synopsis);
		
		btn_release =  (Button)findViewById(R.id.btn_release);
	    
	}

	@Override
	protected void setAttribute() {
		
		title_tv.setText("发通告");
		text_end_time.setText(dateFormat.format(new Date()));
		btn_back.setOnClickListener(onClickListener);
		
		text_end_time.setOnClickListener(onClickListener);
		
		text_city_location.setOnClickListener(onClickListener);
		
		
		relative_locatioin.setOnClickListener(onClickListener);
		text_skill_type.setOnClickListener(onClickListener);
		
		text_sex.setOnClickListener(onClickListener);
		text_pay_type.setOnClickListener(onClickListener);
		text_reward_select.setOnClickListener(onClickListener);
		
		text_interview.setOnClickListener(onClickListener);
		
		btn_release.setOnClickListener(onClickListener);
		
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.text_end_time:
				DatePickerFragment.newInstance(AnnunciateReleaseActivity.this,
						text_end_time.getText().toString(),false).show(
						mContext.getSupportFragmentManager(), "datePicker");
				break;
			case R.id.text_city_location:
				// 弹出已开通城市列表
                CityListActivity.intoActivity(mContext);
				break;
			case R.id.relative_locatioin:
				AnnunciateReleaseLocationActivity.intoActivity(mContext,text_city_location.getTag().toString());
				break;
			case R.id.text_skill_type:
				//AnnunciateSkillTypeActivity.intoActivity(mContext,skill_types);
				LabelVOTypeActivity.intoActivityForResult(mContext, "skill_type",skill_types, "才艺类型",Constants.SKILLTYPE_REQUEST_CODE);
				break;
			case R.id.text_sex:
				selectSexDialog();
				break;
			case R.id.text_pay_type:
				selectPayTypeDialog();
				break;
			case R.id.text_reward_select:
				selectIsRewardSelectDialog();
				break;
			case R.id.text_interview:
				selectIsInterviewDialog();
				break;
			case R.id.btn_release:
				doSaveAnnunciate();
				break;
			default:
				break;
			}
			
		}
	};
	
	/**
	 * 发布通告
	 */
	private void doSaveAnnunciate(){
		
		if (!AndroidUtil.isNetworkAvailable(this)) {
			ToastUtil.show(mContext,
					getResources().getString(R.string.no_connector));
			return;
		}
		showProgress("", "", true);
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", mShPreferencesUtil.getUserId());
		
		params.put("announcementTheme",edit_title.getText().toString().trim());//通告主题
		params.put("announcementEndtime",text_end_time.getText().toString().trim());//通告截止时间
		params.put("announcementProvince", text_city_location.getText().toString().trim()); //通告省
		params.put("announcementProvinceCode", "");//通告省编码
		params.put("announcementCity", text_city_location.getText().toString().trim());//通告所在市
		params.put("announcementCityCode", ""); //通告所在市编码
		params.put("announcementAddress",text_location_addres.getText().toString().trim());//通告详细地址
		params.put("announcementSex",text_sex.getTag().toString());//通告性别要求(0-保密,1-男,2-女)
		params.put("announcementNumber", edit_person_number.getText().toString().trim());//通告需求人数
		
		params.put("announcementTalentType", text_skill_type.getText().toString().trim());//通告才艺类型
		
		params.put("announcementPaymentType",text_pay_type.getTag().toString());//通告结款方式(0-现结2-面议)
		
		params.put("announcementRemuneration", edit_reward.getText().toString().trim());//通告报酬
		params.put("announcementRemunerationUnit", text_reward_select.getTag().toString());//通告报酬单位(0-元/人 1-元/人/天 2-元/人/小时)
		
		params.put("announcementIsInterview",text_interview.getTag().toString());//通告是否面试(0-否 1-是)
		params.put("announcementTraffic", edit_traffic.getText().toString().trim());//通告面试交通费
		params.put("announcementSynopsis", edit_synopsis.getText().toString().trim());//通告简介
		params.put("announcementState", "1");//-1-不显示 0-未开始 1-进行中 2-已关闭
		

		Map<String, String> lastParams = NetworkUtil.generaterPostRequestParams(mContext, params);

		String postUrl = HttpUrlConstant.getPostUrl(this,HttpUrlConstant.SAVE_ANNOUNCEMENT);
		OnResponseListener<Response> listener = new OnResponseListener<Response>(this) {

			@Override
			public void onSuccess(Response response) {

				ToastUtil.show(mContext, response.getBody().getMessage());
				finish();
			}

			@Override
			public void onError(String code, String message) {

				ToastUtil.show(mContext, message);
			}

			@Override
			public void onCompleted() {

				dismissProgress();
			}
		};
		executeRequest(new FastJsonRequest<Response>(
				Request.Method.POST, postUrl, Response.class,
				listener, listener).setParams(lastParams));
		
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.ANNUNCIATE_LOCATION_REQUEST_CODE:
                    PoiItem poiItem = data.getParcelableExtra(Constants.ANNUNCIATE_LOCATION_EXTRA_NAME);
                    setLocationInfo(poiItem);
                    break;
                case Constants.SKILLTYPE_REQUEST_CODE:
                	ArrayList<String> types = data.getStringArrayListExtra(Constants.SELECTED_TAGS);
                	setSkillTypeInfo(types);
                	break;
                case Constants.CITYLIST_CODE: // 城市列表返回
                    if (data != null) {
                    	text_city_location.setText(data.getExtras().getString(Constants.CITY_NAME)); // 城市名
                    	text_city_location.setTag(data.getExtras().getString(Constants.CITY_CODE)); //城市code
                    }
                    break;
                default:
                    break;
            }
        }
		
		
	};
	
	private void setLocationInfo(PoiItem poi) {

		text_location_addres.setText(poi.getCityName() + poi.getAdName() + poi.getSnippet() + "(" + poi.getTitle() + ")");
    }
	
	
	private void setSkillTypeInfo(ArrayList<String> types) {
		if (types != null) {
			skill_types.clear();
			StringBuilder sBuilder = new StringBuilder();
			for (int i = 0; i < types.size(); i++) {
				sBuilder.append(types.get(i)+",");
				skill_types.add(types.get(i));
			}
			text_skill_type.setText(sBuilder.substring(0,sBuilder.length() - 1).toString());
		}
    }

	/**
	 * 选择性别对话框初始化
	 */
	private void selectSexDialog() {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
		alertDialog.setItems(new String[] { "男", "女", "不限" },
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							text_sex.setText("男");
							text_sex.setTag("1");
							break;
						case 1:
							text_sex.setText("女");
							text_sex.setTag("2");
							break;
						case 2:
							text_sex.setText("不限");
							text_sex.setTag("0");
							break;
						}
					}
				});
		alertDialog.create();
		alertDialog.show();
	}
	
	/**
	 * 结账方式对话框初始化
	 */
	private void selectPayTypeDialog() {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
		alertDialog.setItems(new String[] { "现结", "面议"},
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							text_pay_type.setText("现结");
							text_pay_type.setTag(0);
							break;
						case 1:
							text_pay_type.setText("面议");
							text_pay_type.setTag(1);
							break;
						}
					}
				});
		alertDialog.create();
		alertDialog.show();
	}

	/**
	 * 是否面试对话框初始化
	 */
	private void selectIsInterviewDialog() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
		alertDialog.setItems(new String[] { "否", "是"},
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							text_interview.setText("否");
							text_interview.setTag(0);
							break;
						case 1:
							text_interview.setText("是");
							text_interview.setTag(1);
							break;
						}
					}
				});
		alertDialog.create();
		alertDialog.show();
	}
	
	
	/**
	 * 报酬选择方式
	 */
	private void selectIsRewardSelectDialog() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
		alertDialog.setItems(new String[] { "元/人", "元/人/天","元/人/小时"},
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							text_reward_select.setText("元/人");
							text_reward_select.setTag(0);
							edit_reward.setHint("元/人");
							break;
						case 1:
							text_reward_select.setText("元/人/天");
							text_reward_select.setTag(1);
							edit_reward.setHint("元/人/天");
							break;
						case 2:
							text_reward_select.setText("元/人/小时");
							text_reward_select.setTag(2);
							edit_reward.setHint("元/人/小时");
							break;
						}
					}
				});
		alertDialog.create();
		alertDialog.show();
	}
	
	@Override
	public void callBack(String msg) {
		text_end_time.setText(msg);
	}

}
