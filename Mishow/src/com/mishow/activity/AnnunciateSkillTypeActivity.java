package com.mishow.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.bean.HotSearchBean;
import com.mishow.bean.ParcelableMap;
import com.mishow.utils.Constants;
import com.mishow.widget.AdjustLayout;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：才艺类型
 */
public class AnnunciateSkillTypeActivity extends BaseActivity {

	private Button btn_back;
	private TextView title_tv;
	private TextView title_right;
	
	
	/** 平面拍摄数据 **/
	private ArrayList<String> plane_Temp_Datas = new ArrayList<String>();
	/** 视频拍摄数据 **/
	private ArrayList<String> video_Temp_Datas = new ArrayList<String>();
	/** 线下活动数据 **/
	private ArrayList<String> offline_Temp_Datas = new ArrayList<String>();
	/** 推广活动数据 **/
	private ArrayList<String> generalize_Temp_Datas = new ArrayList<String>();
	
	/** 平面拍摄AdjustLayout **/
	private AdjustLayout adjust_plane_layout;
	/** 视频拍摄AdjustLayout **/
	private AdjustLayout adjust_video_layout;
	/** 线下活动AdjustLayout **/
	private AdjustLayout adjust_offline_layout;
	/** 推广活动AdjustLayout **/
	private AdjustLayout adjust_generalize_layout;
	
	
	//private ParcelableMap map_select;
	
	private HashMap<Integer, ArrayList<String>> map_select_bundle;
	
	
	/*private ArrayList<String> planeDatas;
	private ArrayList<String> videoDatas;
	private ArrayList<String> offlineDatas;
	private ArrayList<String> generalizeDatas;*/
	
	private ArrayList<String> mStrings;
	
	public static void intoActivity(Context context,ArrayList<String> mpa_select){
		
		Intent intent = new Intent(context,AnnunciateSkillTypeActivity.class);
		intent.putExtra(Constants.SELECTED_TAGS, mpa_select);
		((Activity)context).startActivityForResult(intent, Constants.SKILLTYPE_REQUEST_CODE);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void initData() {
		
		for (int i = 0; i < 5; i++) {
			plane_Temp_Datas.add("平面拍摄");
		}
		for (int i = 0; i < 4; i++) {
			video_Temp_Datas.add("视频拍摄");
		}
		for (int i = 0; i < 5; i++) {
			offline_Temp_Datas.add("线下活动");
		}
		for (int i = 0; i < 4; i++) {
			generalize_Temp_Datas.add("推广活动");
		}
		
		if (getIntent().getStringArrayListExtra(Constants.SELECTED_TAGS) != null) {
			
			//mStrings = (ParcelableMap)getIntent().getParcelableExtra(Constants.SELECTED_TAGS);
			mStrings = getIntent().getStringArrayListExtra(Constants.SELECTED_TAGS);
		} else {
			mStrings = new ArrayList<>();
		}
		
		/*if (map_select.map.get(1000) != null) {
			planeDatas = (ArrayList<String>) map_select.map.get(1000);
		}else{
			planeDatas = new ArrayList<String>();
		}
		
		if (map_select.map.get(1001) != null) {
			videoDatas = (ArrayList<String>) map_select.map.get(1001);
		}else{
			videoDatas = new ArrayList<String>();
		}
		
		if (map_select.map.get(1002) != null) {
			offlineDatas = (ArrayList<String>) map_select.map.get(1002);
		}else{
			offlineDatas = new ArrayList<String>();
		}
		
		if (map_select.map.get(1003) != null) {
			generalizeDatas = (ArrayList<String>) map_select.map.get(1003);
		}else{
			generalizeDatas = new ArrayList<String>();
		}*/
		
		
		
		map_select_bundle = new HashMap<>();
		
		map_select_bundle.put(1000, plane_Temp_Datas);
		map_select_bundle.put(1001, video_Temp_Datas);
		map_select_bundle.put(1002, offline_Temp_Datas);
		map_select_bundle.put(1003, generalize_Temp_Datas);
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_annunciateskilltype);
		btn_back =  (Button)findViewById(R.id.btn_back);
		title_tv = (TextView)findViewById(R.id.title_tv);
		title_right = (TextView)findViewById(R.id.title_right);
		
		adjust_plane_layout = (AdjustLayout)findViewById(R.id.adjust_plane_layout);
		adjust_video_layout = (AdjustLayout)findViewById(R.id.adjust_video_layout);
		adjust_offline_layout = (AdjustLayout)findViewById(R.id.adjust_offline_layout);
		adjust_generalize_layout = (AdjustLayout)findViewById(R.id.adjust_generalize_layout);
		
	}

	@Override
	protected void setAttribute() {
		
		title_tv.setText("才艺类型");
		title_right.setVisibility(View.VISIBLE);
		title_right.setText("确定");
		title_right.setTextColor(mContext.getResources().getColor(R.color.search_pop_color));
		
		title_right.setOnClickListener(onClickListener);
		btn_back.setOnClickListener(onClickListener);
		
		
		filledPlaneDatas(map_select_bundle);
		filledVideoDatas(map_select_bundle);
		filledOfflineDatas(map_select_bundle);
		filledGeneralizeDatas(map_select_bundle);
		
		//filledDatas(map_select_bundle);
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.title_right:
				if (mStrings.size() > 0) {
					
					setResult(RESULT_OK, new Intent().putExtra(Constants.SKILLTYPE_EXTRA_NAME,mStrings));
				}
				finish();
				break;
			default:
				break;
			}
			
		}
	};
	
	/**
	 * 填充显示平面拍摄
	 */
	private void filledPlaneDatas(HashMap<Integer, ArrayList<String>> map_select_bundle) {
		
		//planeDatas = map_select.get(1000);
		
		ArrayList<String> arrayList = map_select_bundle.get(1000);
		/*if (mStrings != null && mStrings.size() > 0) {
			
			Iterator ite = mStrings.iterator();
			while(ite.hasNext()){
				
			String string  = (String)ite.next();
			if(!arrayList.contains(string))
				
				ite.remove();
			}
		}*/
		
		for (final String bean : arrayList) {
			View view = LayoutInflater.from(this).inflate(R.layout.item_hot_search, null);
			CheckBox checkbox = (CheckBox) view.findViewById(R.id.hot_search_text);
			checkbox.setText(bean);
			checkbox.setChecked(mStrings.contains(bean));
			checkbox.setSingleLine(true);
			checkbox.setEllipsize(TruncateAt.MIDDLE);
			checkbox.setOnCheckedChangeListener(onCheckedChangeListener);
			/*checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    
                	if (isChecked) {
                		mStrings.add(compoundButton.getText().toString());
                    } else {
                    	mStrings.remove(compoundButton.getText().toString());
                    }
                	compoundButton.setChecked(isChecked);
                    refreshSelectedCount();
                }
            });*/
			
			//map_select.map.put(1000, planeDatas);
			if (checkbox.isChecked()) {
				checkbox.setBackgroundResource(R.drawable.shape_selectitem_press);
			}
			adjust_plane_layout.addView(view);
		}
	}
	/**
	 * 填充显示视频拍摄
	 */
	private void filledVideoDatas(HashMap<Integer, ArrayList<String>> map_select_bundle) {
		
		//videoDatas = map_select.get(1001);
		
		ArrayList<String> arrayList = map_select_bundle.get(1001);
		/*if (mStrings != null) {
			Iterator ite = mStrings.iterator();
			while(ite.hasNext()){
				
			String string  = (String)ite.next();
			if(!arrayList.contains(string))
				
				ite.remove();
			}
		}*/
		for (final String bean : arrayList) {
			View view = LayoutInflater.from(this).inflate(R.layout.item_hot_search, null);
			CheckBox checkbox = (CheckBox) view.findViewById(R.id.hot_search_text);
			checkbox.setText(bean);
			checkbox.setSingleLine(true);
			checkbox.setEllipsize(TruncateAt.MIDDLE);
			checkbox.setChecked(mStrings.contains(bean));
			checkbox.setOnCheckedChangeListener(onCheckedChangeListener);
			/*checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    
                	if (isChecked) {
                		mStrings.add(compoundButton.getText().toString());
                    } else {
                    	mStrings.remove(compoundButton.getText().toString());
                    }
                	compoundButton.setChecked(isChecked);
                    refreshSelectedCount();
                }
            });*/
			
			//map_select.map.put(1001, videoDatas);
			if (checkbox.isChecked()) {
				checkbox.setBackgroundResource(R.drawable.shape_selectitem_press);
			}
			adjust_video_layout.addView(view);
		}
	}
	/**
	 * 填充显示线下活动
	 */
	private void filledOfflineDatas(HashMap<Integer, ArrayList<String>> map_select_bundle) {
		
		//offlineDatas = map_select.get(1002);
		
		ArrayList<String> arrayList = map_select_bundle.get(1002);
		/*if (mStrings != null) {
			Iterator ite = mStrings.iterator();
			while(ite.hasNext()){
				
			String string  = (String)ite.next();
			if(!arrayList.contains(string))
				
				ite.remove();
			}
		}*/
		for (final String bean : arrayList) {
			View view = LayoutInflater.from(this).inflate(R.layout.item_hot_search, null);
			CheckBox checkbox = (CheckBox) view.findViewById(R.id.hot_search_text);
			checkbox.setText(bean);
			checkbox.setSingleLine(true);
			checkbox.setEllipsize(TruncateAt.MIDDLE);
			checkbox.setChecked(mStrings.contains(bean));
			checkbox.setOnCheckedChangeListener(onCheckedChangeListener);
			/*checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    
                	if (isChecked) {
                		mStrings.add(compoundButton.getText().toString());
                    } else {
                    	mStrings.remove(compoundButton.getText().toString());
                    }
                	compoundButton.setChecked(isChecked);
                    refreshSelectedCount();
                }
            });*/
			
			//map_select.map.put(1002, offlineDatas);
			if (checkbox.isChecked()) {
				checkbox.setBackgroundResource(R.drawable.shape_selectitem_press);
			}
			adjust_offline_layout.addView(view);
		}
	}
	/**
	 * 填充显示推广活动
	 */
	private void filledGeneralizeDatas(HashMap<Integer, ArrayList<String>> map_select_bundle) {
		
		//generalizeDatas = map_select.get(1003);
		
		ArrayList<String> arrayList = map_select_bundle.get(1003);
		/*if (mStrings != null) {
			Iterator ite = mStrings.iterator();
			while(ite.hasNext()){
				
			String string  = (String)ite.next();
			if(!arrayList.contains(string))
				
				ite.remove();
			}
		}*/
		for (final String bean : arrayList) {
			View view = LayoutInflater.from(this).inflate(R.layout.item_hot_search, null);
			CheckBox checkbox = (CheckBox) view.findViewById(R.id.hot_search_text);
			checkbox.setText(bean);
			checkbox.setSingleLine(true);
			checkbox.setEllipsize(TruncateAt.MIDDLE);
			checkbox.setChecked(mStrings.contains(bean));
			checkbox.setOnCheckedChangeListener(onCheckedChangeListener);
			/*checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    
                	if (isChecked) {
                		mStrings.add(compoundButton.getText().toString());
                    } else {
                    	mStrings.remove(compoundButton.getText().toString());
                    }
                	compoundButton.setChecked(isChecked);
                    refreshSelectedCount();
                }
            });*/
			
			//map_select.map.put(1003, generalizeDatas);
			if (checkbox.isChecked()) {
				checkbox.setBackgroundResource(R.drawable.shape_selectitem_press);
			}
			adjust_generalize_layout.addView(view);
		}
	}
	
	
	private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            
        	if (isChecked) {
                mStrings.add(compoundButton.getText().toString());
            } else {
                mStrings.remove(compoundButton.getText().toString());
            }
        	compoundButton.setChecked(isChecked);
            refreshSelectedCount();

        }
    };

    // refresh标题栏显示的选择数
    private void refreshSelectedCount() {
        int total = mStrings.size();
        title_right.setText("确定 (" + total + ")");
    }
}
