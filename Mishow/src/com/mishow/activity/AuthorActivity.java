package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;

/**
 * 作者：wei.miao <br/>
 * 描述：达人认证
 */
public class AuthorActivity extends BaseActivity{

	private Button btn_back;
	private TextView title_right;
	
	private RelativeLayout relative_author_name;
	
	private RelativeLayout relative_author_v;


	public static void intoActivity(Context context) {

		Intent intent = new Intent(context, AuthorActivity.class);
		context.startActivity(intent);

	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		// 不显示程序的标题栏
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
		// 不显示系统的标题栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

		setContentView(R.layout.activity_author);
		btn_back = (Button) findViewById(R.id.btn_back);
		title_right = (TextView) findViewById(R.id.title_right);
		
		relative_author_name = (RelativeLayout)findViewById(R.id.relative_author_name);
		relative_author_v = (RelativeLayout)findViewById(R.id.relative_author_v);

		
	}

	@Override
	protected void setAttribute() {
		setTitle("达人认证");
		title_right.setVisibility(View.GONE);
		btn_back.setOnClickListener(onClickListener);
		relative_author_name.setOnClickListener(onClickListener);
		relative_author_v.setOnClickListener(onClickListener);
	}


	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.relative_author_name:
				AuthorNameActivity.intoActivity(mContext);
				break;
			case R.id.relative_author_v:
				
				break;
			default:
				break;
			}
		}
	};


}
