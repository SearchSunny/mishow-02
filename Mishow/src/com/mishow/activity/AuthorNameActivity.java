package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.utils.ToastUtil;

/**
 * 作者：wei.miao <br/>
 * 描述：实名认证
 */
public class AuthorNameActivity extends BaseActivity{

	private Button btn_back;
	private TextView title_right;
	
	private Button btn_submit;
	/** 真实姓名 **/
	private EditText edit_author_name;


	public static void intoActivity(Context context) {

		Intent intent = new Intent(context, AuthorNameActivity.class);
		context.startActivity(intent);

	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		// 不显示程序的标题栏
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
		// 不显示系统的标题栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

		setContentView(R.layout.activity_author_name);
		btn_back = (Button) findViewById(R.id.btn_back);
		title_right = (TextView) findViewById(R.id.title_right);
		
		edit_author_name = (EditText)findViewById(R.id.edit_author_name);
		
		btn_submit = (Button)findViewById(R.id.btn_submit);

		
	}

	@Override
	protected void setAttribute() {
		setTitle("实名认证");
		title_right.setVisibility(View.GONE);
		
		btn_back.setOnClickListener(onClickListener);
		btn_submit.setOnClickListener(onClickListener);
		
	}


	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_submit:
				if (edit_author_name.getText().length() < 1) {
					edit_author_name.requestFocus();
					ToastUtil.show(mContext, "请输入真实姓名");
				}
				break;
			case R.id.relative_author_v:
				
				break;
			default:
				break;
			}
		}
	};


}
