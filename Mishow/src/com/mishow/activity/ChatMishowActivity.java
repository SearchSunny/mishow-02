package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
/**
 * 作者：wei.miao <br/>
 * 描述：聊天界面
 */
public class ChatMishowActivity extends BaseActivity {

	private String userId;
	private String userName;
	/**
	 * 启动聊天界面(当前登录用户与指定userid聊天)
	 * @param context
	 * @param userid 指定userid聊天
	 * @param userName 指定userName名称
	 */
	public static void intoActivity(Context context,String userid,String userName) {
		Intent intent = new Intent(context, ChatMishowActivity.class);
		intent.putExtra("userid", userid);
		intent.putExtra("userName", userName);
		context.startActivity(intent);
	}
	
	@Override
	protected void initData() {
		if (getIntent() != null) {
			userId = getIntent().getStringExtra("userid");
			userName = getIntent().getStringExtra("userName");
		}
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		setContentView(R.layout.activity_mishow_chat);
	}

	@Override
	protected void setAttribute() {
		
		//new出EaseChatFragment或其子类的实例
		 EaseChatFragment chatFragment = new EaseChatFragment();
		 //传入参数
		 Bundle args = new Bundle();
		 //传入 chatType（会话类型）
		 args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);//单聊
		 //args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_GROUP);//群聊
		 //userId（用户或群id）
		 args.putString(EaseConstant.EXTRA_USER_ID, userId);
		 args.putString(EaseConstant.EXTRA_USER_NAEM, userName);
		 chatFragment.setArguments(args);
		 getSupportFragmentManager().beginTransaction().add(R.id.ec_layout_container, chatFragment).commit();
	}

}
