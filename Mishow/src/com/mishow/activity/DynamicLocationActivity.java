package com.mishow.activity;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.amap.api.maps2d.model.LatLng;
import com.amap.api.services.core.PoiItem;
import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.adapter.MyKeyWordsLocationAdapter;
import com.mishow.log.MyLog;
import com.mishow.map.search.CustomMapSearch;
import com.mishow.preferenses.SharedPreferencesUtil;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.Constants;
import com.mishow.utils.ToastUtil;
import com.mishow.widget.MyListView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：所在位置
 */
public class DynamicLocationActivity extends BaseActivity {
	
	private Button btn_back;
	private TextView title_tv;
	
	/** 关键字 **/
	private EditText edit_keyword;
	/** 搜索 **/
	private Button btn_location_search;
	
	private PullPushRefreshLayout refreshLayout_mishow;
	private MyListView listview_data;
	/**
	 * 关键字
	 **/
	private String mKeyword = "";
	private LatLng latlng;
	/**
     * 纬度
     **/
    private double latitude;
    /**
     * 经度
     **/
    private double longitude;
    
    private CustomMapSearch mCustomMapSearch;

	private MyKeyWordsLocationAdapter myClinicSearchKeywordAdapter;

	/**
	 * 表示查第几页 从第0页开始
	 **/
	private int mPageNum = 0;
	/**
	 * 总页数
	 **/
	private int mPageCount;
	/**
	 * poi数据列表
	 **/
	private ArrayList<PoiItem> mPoiItemList;
	
	private SharedPreferencesUtil sharedPreferencesUtil;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,DynamicLocationActivity.class);
		((Activity)context).startActivityForResult(intent, Constants.ANNUNCIATE_LOCATION_REQUEST_CODE);
		
	}

	@Override
	protected void initData() {
		
		sharedPreferencesUtil = new SharedPreferencesUtil(mContext);
		if (sharedPreferencesUtil.getLongitude() != null && sharedPreferencesUtil.getLatitude() != null) {
			
			latitude = Double.parseDouble(sharedPreferencesUtil.getLatitude());
			longitude = Double.parseDouble(sharedPreferencesUtil.getLongitude());
			
		}else{
			
			latitude = 39.90403;
			longitude = 116.407525;
		}
		
		latlng = new LatLng(latitude, longitude);
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_dynamiclocation);
		
		title_tv = (TextView)findViewById(R.id.title_tv);
		btn_back =  (Button)findViewById(R.id.btn_back);
		
		btn_location_search = (Button)findViewById(R.id.btn_location_search);
		
		refreshLayout_mishow = (PullPushRefreshLayout) findViewById(R.id.refreshLayout_mishow);
		listview_data = (MyListView) findViewById(R.id.listview_data);
		edit_keyword = (EditText) findViewById(R.id.edit_keyword);
	}

	@Override
	protected void setAttribute() {
		
		title_tv.setText("所在位置");
		btn_back.setOnClickListener(onClickListener);
		btn_location_search.setOnClickListener(onClickListener);
		
		mCustomMapSearch = new CustomMapSearch(mContext,mOnPoiSearchResultListener);
		mPoiItemList = new ArrayList<PoiItem>();

		myClinicSearchKeywordAdapter = new MyKeyWordsLocationAdapter(mContext,mPoiItemList);

		listview_data.setOnItemClickListener(onItemClickListener);
		listview_data.setAdapter(myClinicSearchKeywordAdapter);

		// 禁用下拉刷新
		refreshLayout_mishow.setEnabled(false);
		refreshLayout_mishow.setListView(listview_data);
		refreshLayout_mishow.setOnLoadListener(onLoadListener);
		
		doSearchQuery();
	}

	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_location_search:
				if (AndroidUtil.isNetworkAvailable(mContext)) {
					mKeyword = checkEditText(edit_keyword);
					if ("".equals(mKeyword)) {
						ToastUtil.show(mContext, "请输入搜索关键字");
					} else {
						doSearchQuery();
					}
                }else{
                    ToastUtil.show(mContext,"网络连接异常,请检查网络");
                }
				break;
			
			default:
				break;
			}
			
		}
	};
	
	// 处理搜索功能-----------------------------------------------
		/**
		 * 搜索回调
		 */
		private CustomMapSearch.OnPoiSearchResultListener mOnPoiSearchResultListener = new CustomMapSearch.OnPoiSearchResultListener() {
			@Override
			public void onSearchSuccess(List<PoiItem> poiItems, int pageCount) {
				MyLog.d("MV", "搜索成功=====返回该结果的总页数=" + pageCount);
				mPageCount = pageCount;
				mPoiItemList.addAll(poiItems);
				myClinicSearchKeywordAdapter.notifyDataSetChanged();
				loadCompleted();
				dismissProgress();
			}

			@Override
			public void onSearchFail(int reCode) {
				MyLog.d("MV", "搜索失败=====返回该结果的总页数=" + reCode);
				if (reCode != 1000) {
					ToastUtil.show(mContext, "没有找到该位置信息");
				}
				loadCompleted();
				dismissProgress();
			}
		};

		/**
		 * 加载更多接口
		 */
		private PullPushRefreshLayout.OnLoadListener onLoadListener = new PullPushRefreshLayout.OnLoadListener() {
			@Override
			public void onLoad() {

				MyLog.d("MV", "mPageNum=" + mPageNum);
				if (mPageNum < mPageCount) {
					mPageNum++;
					mCustomMapSearch.doSearchBound(mKeyword, "","",latitude,longitude,mPageNum);
				} else {

					loadCompleted();
				}

			}
		};

		private void doSearchQuery() {
			mPageNum = 0;
			
			showProgress();
			if (mPoiItemList != null) {
				mPoiItemList.clear();
				myClinicSearchKeywordAdapter.notifyDataSetChanged();
			}
			mCustomMapSearch.doSearchBound(mKeyword, "","",latitude,longitude,mPageNum);
		}

		/**
		 * 加载完成
		 */
		private void loadCompleted() {

			refreshLayout_mishow.setRefreshing(false);
			refreshLayout_mishow.setLoading(false);
			// 获取到全部数据后，则隐藏加载更多
			if (mPageNum < mPageCount) {
				refreshLayout_mishow.setCanLoadMore(true);
			} else {
				refreshLayout_mishow.setCanLoadMore(false);
			}
		}

		private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long id) {

				PoiItem poiItem = mPoiItemList.get(position);
				Intent intent = new Intent();
				intent.putExtra(Constants.ANNUNCIATE_LOCATION_EXTRA_NAME, poiItem);
				setResult(RESULT_OK, intent);
				finish();
			}
		};

		/**
		 * 判断edittext是否null
		 */
		public String checkEditText(EditText editText) {
			if (editText != null && editText.getText() != null
					&& !(editText.getText().toString().trim().equals(""))) {
				return editText.getText().toString().trim();
			} else {
				return "";
			}
		}
}
