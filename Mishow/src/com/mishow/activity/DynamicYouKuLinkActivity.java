package com.mishow.activity;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.utils.FileUtil;
import com.mishow.utils.ToastUtil;
import com.mishow.widget.UploadImageDialog;
import com.mishow.widget.UploadVideoDialog;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述： 优酷链接地址
 */
public class DynamicYouKuLinkActivity extends BaseActivity {
	
	/** 取消 **/
	private TextView txt_link_cancel;
	/** 发送 **/
	private TextView txt_link_send;
	/** 链接地址 **/
	private EditText edit_link_desc;
	
	
	/** 下一步 **/
	private Button btn_link_next;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,DynamicYouKuLinkActivity.class);
		context.startActivity(intent);
		
	}

	@Override
	protected void initData() {
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_dynamicyoukulink);
		txt_link_cancel = (TextView)findViewById(R.id.txt_link_cancel);
		txt_link_send = (TextView)findViewById(R.id.txt_link_send);
		edit_link_desc = (EditText)findViewById(R.id.edit_link_desc);
		btn_link_next = (Button)findViewById(R.id.btn_link_next);
	}

	@Override
	protected void setAttribute() {
		
		txt_link_cancel.setOnClickListener(onClickListener);
		txt_link_send.setOnClickListener(onClickListener);
		edit_link_desc.setOnClickListener(onClickListener);
		
		btn_link_next.setOnClickListener(onClickListener);
		
	}

	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.txt_link_cancel:
				finish();
				break;
			case R.id.txt_link_send:
				
				break;
			case R.id.btn_link_next:
				ToastUtil.show(mContext, "下一步");
				break;
			default:
				break;
			}
			
		}
	};
}
