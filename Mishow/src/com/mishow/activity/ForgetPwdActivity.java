package com.mishow.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
/**
 * 作者：wei.miao<br/>
 * 描述：忘记密码页面
 */
public class ForgetPwdActivity extends BaseActivity {
	
	/** 返回 **/
	private Button btn_back;
	/** 标题 **/
	private TextView title_tv;
	/** 手机号 **/
	private EditText edit_forget_phone;
	/** 验证码 **/
	private EditText edit_forget_vercode;
	/** 输入密码 **/
	private EditText edit_forget_pwd;
	/** 获取验证码 **/
	private Button btn_forget_vercode;
	/** 下一步 **/
	private Button btn_forget_next;

	@Override
	protected void initData() {

	}

	@Override
	protected void initView(Bundle savedInstanceState) {

		setContentView(R.layout.activity_forget);
		
		btn_back = (Button) findViewById(R.id.btn_back);
		title_tv = (TextView) findViewById(R.id.title_tv);
		
		edit_forget_phone = (EditText) findViewById(R.id.edit_forget_phone);
		edit_forget_vercode = (EditText) findViewById(R.id.edit_forget_vercode);
		edit_forget_pwd = (EditText) findViewById(R.id.edit_forget_pwd);
		
		btn_forget_vercode = (Button) findViewById(R.id.btn_forget_vercode);
		btn_forget_next = (Button) findViewById(R.id.btn_forget_next);
	}

	@Override
	protected void setAttribute() {

		title_tv.setText("忘记密码");
		btn_back.setOnClickListener(onClickListener);
	}

	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_forget_next:
				
				Intent intent = new Intent(ForgetPwdActivity.this, LoginActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				
				break;
			}
		}
	};
}
