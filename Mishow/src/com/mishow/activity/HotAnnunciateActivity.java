package com.mishow.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.adapter.AnnunciateFragmentAdapter;
import com.mishow.bean.Announcement;
import com.mishow.bean.AnnunciateBean;
import com.mishow.bean.Area;
import com.mishow.bean.MenuItemBean;
import com.mishow.db.DBhelper;
import com.mishow.inteface.CascadingMenuViewOnSelectListener;
import com.mishow.log.MyLog;
import com.mishow.net.HttpUrlConstant;
import com.mishow.net.NetworkUtil;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.request.listener.OnResponseListener;
import com.mishow.request.response.AnnouncementResponse;
import com.mishow.request.response.AnnouncementResponse.Result;
import com.mishow.request.response.ProfessionTypeResponse;
import com.mishow.request.result.Profession;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.ToastUtil;
import com.mishow.volley.http.request.FastJsonRequest;
import com.mishow.widget.CascadingMenuPopWindow;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;
import com.mishow.widget.PopWindowHotAnnunciateAll;
import com.mishow.widget.PopWindowHotAnnunciateFiltrate;
import com.mishow.widget.PopWindowHotAnnunciateSmart;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：热通告
 */
public class HotAnnunciateActivity extends BaseActivity implements OnClickListener,OnRefreshListener{
	
	/** 全国 **/
	public static final int TAB_ONE = 1;
	/** 全部才艺 **/
	public static final int TAB_TWO = 2;
	/** 智能排序 **/
	public static final int TAB_THREE = 3;
	/** 筛选 **/
	public static final int TAB_FOUR = 4;
	
	
	/** 返回 **/
	private Button btn_back;
	/** 标题 **/
	private TextView title_tv;
	
	/** 全国排序  **/
	private LinearLayout linear_sort_nationwide;
	private TextView tv_sort_nationwide;
	private ImageView iv_sort_nationwide;
	
	/** 全部才艺  **/
	private LinearLayout linear_sort_all;
	private TextView txt_sort_all;
	private ImageView iv_sort_all;
	
	
	/** 智能排序  **/
	private LinearLayout linear_sort_smart;
	private TextView txt_sort_smart;
	private ImageView iv_sort_smart;
	
	
	/** 筛选  **/
	private LinearLayout linear_sort_filtrate;
	private TextView txt_sort_filtrate;
	private ImageView iv_sort_filtrate;
	
	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_annunciate;
	/** 数据为空 **/
	private EmptyDataLayout ll_search_annunciate_empty;
	/** 通告列表 **/
	private MyListView lv_search_annunciate;
	
	/** 搜索通告列表Adapter **/
	private AnnunciateFragmentAdapter annunciateFragmentAdapter;
	
	/** 通告列表 **/
	private ArrayList<Announcement> annunciateBeans;
	
	private CascadingMenuPopWindow cascadingMenuPopWindow = null;
	private ArrayList<Area> provinceList;
	private DBhelper dBhelper;
	
	/** 全部 PopWindow **/
	private PopWindowHotAnnunciateAll mPopWindowHotAnnunciateAll = null;
	
	/** 智能 PopWindow **/
	private PopWindowHotAnnunciateSmart mPopWindowHotAnnunciateSmart = null;
	
	/** 筛选 PopWindow **/
	private PopWindowHotAnnunciateFiltrate mPopWindowHotAnnunciateFiltrate = null;
	
	/**
	 * 总页数
	 **/
	private int mTotal;
	/**
	 * 标记是否在刷新
	 */
	private boolean isReflesh;
	/** 刷新操作 **/
	public static final int ACTION_REFRESH = 1;
	/** 加载更多操作 **/
	public static final int ACTION_LOAD_MORE = 2;
	/**
	 * 跳转到热通告页面
	 * @param context
	 */
	public static void intoActivity(Context context) {
		Intent intent = new Intent(context, HotAnnunciateActivity.class);
		context.startActivity(intent);
	}
	
	@Override
	protected void initData() {
	
		annunciateBeans = new ArrayList<Announcement>();
		
		
		annunciateFragmentAdapter = new AnnunciateFragmentAdapter(this, annunciateBeans);
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_hot_annunciate);
		btn_back = (Button) findViewById(R.id.btn_back);
		title_tv = (TextView) findViewById(R.id.title_tv);
		//全国排序 
		linear_sort_nationwide = (LinearLayout)findViewById(R.id.linear_sort_nationwide);
		tv_sort_nationwide = (TextView)findViewById(R.id.tv_sort_nationwide);
		iv_sort_nationwide = (ImageView)findViewById(R.id.iv_sort_nationwide);
		
		//全部才艺
		linear_sort_all = (LinearLayout)findViewById(R.id.linear_sort_all);
		txt_sort_all = (TextView)findViewById(R.id.txt_sort_all);
		iv_sort_all = (ImageView)findViewById(R.id.iv_sort_all);
		
		//智能排序 
		linear_sort_smart = (LinearLayout)findViewById(R.id.linear_sort_smart);
		txt_sort_smart = (TextView)findViewById(R.id.txt_sort_smart);
		iv_sort_smart = (ImageView)findViewById(R.id.iv_sort_smart);
		
		
		//筛选 
		linear_sort_filtrate = (LinearLayout)findViewById(R.id.linear_sort_filtrate);
		txt_sort_filtrate = (TextView)findViewById(R.id.txt_filtrate);
		iv_sort_filtrate = (ImageView)findViewById(R.id.iv_sort_filtrate);
		
		
		refreshLayout_annunciate = (PullPushRefreshLayout)findViewById(R.id.refreshLayout_annunciate);
    	ll_search_annunciate_empty = (EmptyDataLayout)findViewById(R.id.ll_search_annunciate_empty);
    	lv_search_annunciate = (MyListView)findViewById(R.id.lv_search_annunciate);
		
		
	}

	@Override
	protected void setAttribute() {
		
		title_tv.setText("热通告");
		btn_back.setOnClickListener(this);
		
		linear_sort_nationwide.setOnClickListener(this);
		linear_sort_all.setOnClickListener(this);
		linear_sort_smart.setOnClickListener(this);
		linear_sort_filtrate.setOnClickListener(this);
		
		lv_search_annunciate.setAdapter(annunciateFragmentAdapter);
		lv_search_annunciate.setOnItemClickListener(onItemClickListener);
		
		//向三级menu添加地区数据
		dBhelper = new DBhelper(this,false);
		provinceList = dBhelper.getProvince();
		
		refreshLayout_annunciate.setListView(lv_search_annunciate);
		refreshLayout_annunciate.setOnRefreshListener(this);
		refreshLayout_annunciate.setOnLoadListener(onLoadListener);
		
		onRefresh();
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.linear_sort_nationwide: //全国排序
			showNationPopWindow();
			break;
		case R.id.linear_sort_all://全部排序
			showAllPopWindow();
			break;
		case R.id.linear_sort_smart://智能排序
			showSmartPopWindow();
			break;
		case R.id.linear_sort_filtrate://筛选
			showFiltratePopWindow();
			break;
		default:
			break;
		}
		
	}
	/**
	 * 通行列表点击
	 */
	private OnItemClickListener onItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			int announcementId = annunciateBeans.get(position).getId();
			AnnunciateDetailActivity.intoActivity(mContext,announcementId+"");
		}
	};
	
	/**
	 * 服务端获取通告列表
	 * @param action 区分刷新或加载更多
	 */
	private void getAnnouncementsFromApi(final int action) {

		if (!AndroidUtil.isNetworkAvailable(this)) {
			ToastUtil.show(mContext,
					getResources().getString(R.string.no_connector));
			return;
		}
		if(isReflesh){
			return ;
		}
		isReflesh = true;
		showProgress("", "", true);
		Map<String, String> params = new HashMap<String, String>();
		params.put("pageNo", pageNo+"");
		params.put("pageSize", pageSize+"");

		Map<String, String> lastParams = NetworkUtil.generaterPostRequestParams(mContext, params);

		String postUrl = HttpUrlConstant.getPostUrl(this,HttpUrlConstant.GET_QUERY_ANNOUNCEMENT_PAGE);
		OnResponseListener<AnnouncementResponse> listener = new OnResponseListener<AnnouncementResponse>(this) {

			@Override
			public void onSuccess(AnnouncementResponse response) {

				Result result = response.getBody().getResult();
				ArrayList<Announcement> dataList = result.getDataList();
				
				if (dataList != null && dataList.size() > 0) {
					mTotal = response.getBody().getResult().getTotal();
					if (action == ACTION_REFRESH) {
						handlerRefresh(dataList);
					} else if (action == ACTION_LOAD_MORE) {
						handleLoadMore(dataList);
					}
				}
			}

			@Override
			public void onError(String code, String message) {
				isReflesh = false;
				ToastUtil.show(mContext, message);
				if (action == ACTION_LOAD_MORE) {
					pageNo--;
				}
			}

			@Override
			public void onCompleted() {
				isReflesh = false;
				dismissProgress();
				loadCompleted();
			}
		};
		executeRequest(new FastJsonRequest<AnnouncementResponse>(
				Request.Method.POST, postUrl, AnnouncementResponse.class,
				listener, listener).setParams(lastParams));

	}
	
	
	private void handlerRefresh(ArrayList<Announcement> results) {
		if (results == null || results.isEmpty()) {
			annunciateBeans.clear();
			annunciateFragmentAdapter.notifyDataSetChanged();
			ll_search_annunciate_empty.setVisibility(View.VISIBLE);
		} else {
			ll_search_annunciate_empty.setVisibility(View.GONE);
			annunciateBeans.clear();
			annunciateBeans.addAll(results);
			annunciateFragmentAdapter.notifyDataSetChanged();
		}
	}
	
	private void handleLoadMore(ArrayList<Announcement> results) {
		if (results != null && !results.isEmpty()) {
			annunciateBeans.addAll(results);
			annunciateFragmentAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public void onRefresh() {
		pageNo = 1;
		getAnnouncementsFromApi(ACTION_REFRESH);
	}
	
	/**
	 * 加载更多接口
	 */
	private PullPushRefreshLayout.OnLoadListener onLoadListener = new PullPushRefreshLayout.OnLoadListener() {
		@Override
		public void onLoad() {

			MyLog.d("MV", "pageNo=" + pageNo);
			if (mTotal > annunciateBeans.size()) {
				pageNo++;
				getAnnouncementsFromApi(ACTION_LOAD_MORE);
			} else {
				loadCompleted();
			}
		}
	};
	
	/**
	 * 加载完成
	 */
	private void loadCompleted() {

		refreshLayout_annunciate.setRefreshing(false);
		refreshLayout_annunciate.setLoading(false);
		// 获取到全部数据后，则隐藏加载更多
		if (mTotal > annunciateBeans.size()) {
			refreshLayout_annunciate.setCanLoadMore(true);
		} else {
			refreshLayout_annunciate.setCanLoadMore(false);
		}
	}
	
	//热通行全国-----------------------start----------------------
	/**
	 * 显示热通告全国popWindow
	 */
	private void showNationPopWindow() {
		
		if (cascadingMenuPopWindow == null) {
			cascadingMenuPopWindow = new CascadingMenuPopWindow(
					getApplicationContext(), provinceList);
			cascadingMenuPopWindow
					.setMenuViewOnSelectListener(new NMCascadingMenuViewOnSelectListener());
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			cascadingMenuPopWindow.showAsDropDown(linear_sort_nationwide,1,1);
			
			cascadingMenuPopWindow.setOnDismissListener(new DismissListener(1));
			iv_sort_nationwide.setImageResource(R.drawable.arrow_up);
			
			
		} else if (cascadingMenuPopWindow != null
				&& cascadingMenuPopWindow.isShowing()) {
			
			cascadingMenuPopWindow.dismiss();
			
		} else {
			
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			cascadingMenuPopWindow.showAsDropDown(linear_sort_nationwide,1,1);
			iv_sort_nationwide.setImageResource(R.drawable.arrow_up);
		}
	}
	
	
	private class DismissListener implements OnDismissListener{
		
		int  mPosition;
		public DismissListener(int position) {
			
			this.mPosition = position;
		}
		
		@Override
		public void onDismiss() {
			
			closePopWindow(mPosition);
			
		}
	};
	/**
	 * 关闭显示热通告全国popWindow
	 */
	private void closePopWindow(int position){
		
		if (position == 1) {
			
			iv_sort_nationwide.setImageResource(R.drawable.arrow_down);
			if (cascadingMenuPopWindow != null
					&& cascadingMenuPopWindow.isShowing()) {
				
				cascadingMenuPopWindow.dismiss();
			}
		}else if(position == 2){
			
			iv_sort_all.setImageResource(R.drawable.arrow_down);
			if (mPopWindowHotAnnunciateAll != null
					&& mPopWindowHotAnnunciateAll.isShowing()) {
				
				mPopWindowHotAnnunciateAll.closePopWin();
			}
		}else if(position == 3){
			
			iv_sort_smart.setImageResource(R.drawable.arrow_down);
			if (mPopWindowHotAnnunciateSmart != null
					&& mPopWindowHotAnnunciateSmart.isShowing()) {
				
				mPopWindowHotAnnunciateSmart.closePopWin();
			}
			
		}else if(position == 4){
			
			iv_sort_filtrate.setImageResource(R.drawable.arrow_down);
			if (mPopWindowHotAnnunciateFiltrate != null
					&& mPopWindowHotAnnunciateFiltrate.isShowing()) {
				
				mPopWindowHotAnnunciateFiltrate.closePopWin();
			}
		}
		
	}
	
	/**
	 * 
	 * 作者：wei.miao <br/>
	 * 描述：级联菜单选择回调接口
	 */
	class NMCascadingMenuViewOnSelectListener implements
			CascadingMenuViewOnSelectListener {

		@Override
		public void getValue(Area area) {
			
			Toast.makeText(getApplicationContext(), "" + area.getName(),
					Toast.LENGTH_SHORT).show();
		}

	}

	//热通告全国----------------------end-----------------------
	
	//热通告全部----------------------start------------------------
	private void showAllPopWindow(){
		
		ArrayList<MenuItemBean> menuContents = new ArrayList<MenuItemBean>();
		menuContents.add(new MenuItemBean("全部"));
		menuContents.add(new MenuItemBean("模特"));
		menuContents.add(new MenuItemBean("影视表演"));
		menuContents.add(new MenuItemBean("音乐表演"));
		
		menuContents.add(new MenuItemBean("全部"));
		menuContents.add(new MenuItemBean("模特"));
		menuContents.add(new MenuItemBean("影视表演"));
		menuContents.add(new MenuItemBean("音乐表演"));
		
		
		if (mPopWindowHotAnnunciateAll == null) {
			mPopWindowHotAnnunciateAll = new PopWindowHotAnnunciateAll(
					getApplicationContext(), menuContents);
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			mPopWindowHotAnnunciateAll.showAsDropDown(linear_sort_all,1,1);
			
			mPopWindowHotAnnunciateAll.setOnDismissListener(new DismissListener(2));
			iv_sort_all.setImageResource(R.drawable.arrow_up);
			
			
		} else if (mPopWindowHotAnnunciateAll != null
				&& mPopWindowHotAnnunciateAll.isShowing()) {
			
			mPopWindowHotAnnunciateAll.dismiss();
			
		} else {
			
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			mPopWindowHotAnnunciateAll.showAsDropDown(linear_sort_all,1,1);
			iv_sort_all.setImageResource(R.drawable.arrow_up);
		}
	}
	//热通告全部----------------------end------------------------
	
	//热通告智能排序----------------------start------------------------
	
	private void showSmartPopWindow(){
		
		ArrayList<MenuItemBean> menuContents = new ArrayList<MenuItemBean>();
		menuContents.add(new MenuItemBean("全部"));
		menuContents.add(new MenuItemBean("模特"));
		menuContents.add(new MenuItemBean("影视表演"));
		menuContents.add(new MenuItemBean("音乐表演"));
		
		menuContents.add(new MenuItemBean("全部"));
		menuContents.add(new MenuItemBean("模特"));
		menuContents.add(new MenuItemBean("影视表演"));
		menuContents.add(new MenuItemBean("音乐表演"));
		
		if (mPopWindowHotAnnunciateSmart == null) {
			mPopWindowHotAnnunciateSmart = new PopWindowHotAnnunciateSmart(
					getApplicationContext(), menuContents);
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			mPopWindowHotAnnunciateSmart.showAsDropDown(linear_sort_smart,1,1);
			
			mPopWindowHotAnnunciateSmart.setOnDismissListener(new DismissListener(3));
			iv_sort_smart.setImageResource(R.drawable.arrow_up);
			
			
		} else if (mPopWindowHotAnnunciateSmart != null
				&& mPopWindowHotAnnunciateSmart.isShowing()) {
			
			mPopWindowHotAnnunciateSmart.dismiss();
			
		} else {
			
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			mPopWindowHotAnnunciateSmart.showAsDropDown(linear_sort_smart,1,1);
			iv_sort_smart.setImageResource(R.drawable.arrow_up);
		}
	}
	//热通告智能排序----------------------end------------------------
	
	//热通告筛选排序----------------------start------------------------
	private void showFiltratePopWindow(){
		
		ArrayList<MenuItemBean> menuContents = new ArrayList<MenuItemBean>();
		menuContents.add(new MenuItemBean("全部"));
		menuContents.add(new MenuItemBean("模特"));
		menuContents.add(new MenuItemBean("影视表演"));
		menuContents.add(new MenuItemBean("音乐表演"));
		
		menuContents.add(new MenuItemBean("全部"));
		menuContents.add(new MenuItemBean("模特"));
		menuContents.add(new MenuItemBean("影视表演"));
		menuContents.add(new MenuItemBean("音乐表演"));
		
		if (mPopWindowHotAnnunciateFiltrate == null) {
			mPopWindowHotAnnunciateFiltrate = new PopWindowHotAnnunciateFiltrate(
					getApplicationContext(), menuContents);
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			mPopWindowHotAnnunciateFiltrate.showAsDropDown(linear_sort_filtrate,1,1);
			
			mPopWindowHotAnnunciateFiltrate.setOnDismissListener(new DismissListener(4));
			iv_sort_filtrate.setImageResource(R.drawable.arrow_up);
			
			
		} else if (mPopWindowHotAnnunciateFiltrate != null
				&& mPopWindowHotAnnunciateFiltrate.isShowing()) {
			
			mPopWindowHotAnnunciateFiltrate.dismiss();
			
		} else {
			
			//相对某个控件的位置，有偏移X、Y方向各偏移1
			mPopWindowHotAnnunciateFiltrate.showAsDropDown(linear_sort_filtrate,1,1);
			iv_sort_filtrate.setImageResource(R.drawable.arrow_up);
		}
	}

	
	//热通告智能排序----------------------end------------------------
}
