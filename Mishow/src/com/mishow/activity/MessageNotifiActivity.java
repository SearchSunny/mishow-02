package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：点赞和评论消息
 */
public class MessageNotifiActivity extends BaseActivity{

	
	private Button btn_back;
	private TextView title_tv;
	private ImageButton btn_right;
	private TextView title_right;
	
	
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,MessageNotifiActivity.class);
		context.startActivity(intent);
		
	}
	
	@Override
	protected void initData() {
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_message_notifi);
		btn_back =  (Button)findViewById(R.id.btn_back);
		title_tv = (TextView)findViewById(R.id.title_tv);
		
		btn_right = (ImageButton)findViewById(R.id.btn_right);
		title_right = (TextView)findViewById(R.id.title_right);
		
	}

	@Override
	protected void setAttribute() {
		title_tv.setText("通知");
		title_right.setVisibility(View.VISIBLE);
		title_right.setText("编辑");
		title_right.setTextColor(getResources().getColor(R.color.pale_red));
		
		btn_back.setOnClickListener(onClickListener);
		
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			default:
				break;
			}
			
		}
	};

}
