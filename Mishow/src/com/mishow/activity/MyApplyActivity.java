package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.fragment.MyApplyFragment;
import com.mishow.fragment.MyReleaseFragment;
import com.mishow.utils.Constants;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：我报名的
 */
public class MyApplyActivity extends BaseActivity implements OnClickListener,OnCheckedChangeListener{
	
	private Button btn_back;
	private TextView title_tv;
	
	private FrameLayout content;
	
	private RadioGroup rgroup_title; 
	/** 报名中 **/
	private RadioButton radio_recruit;
	/** 已接受 **/
	private RadioButton radio_past;
	/** 未接受 **/
	private RadioButton radio_close;
	/** 全部 **/
	private RadioButton radio_all;
	
	/** 显示fragment **/
	private MyApplyFragment currentfragment; 
	/** 管理显示fragment **/
	private FragmentManager fragmentmanager;
	/** 报名标示(0=全部，1报名中，2=已接受，3=未接受 ) **/
	private int status; 
	
	public static void intoActivity(Context context){
		Intent intent = new Intent(context,MyApplyActivity.class);
		context.startActivity(intent);
	}

	@Override
	protected void initData() {
		fragmentmanager = getSupportFragmentManager();
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		setContentView(R.layout.activity_my_apply);
		
		btn_back =  (Button)findViewById(R.id.btn_back);
		title_tv = (TextView)findViewById(R.id.title_tv);
		
		rgroup_title = (RadioGroup) findViewById(R.id.rgroup_title);
		radio_recruit = (RadioButton) findViewById(R.id.radio_recruit);
		radio_past = (RadioButton) findViewById(R.id.radio_past);
		radio_close = (RadioButton) findViewById(R.id.radio_close);
		radio_all = (RadioButton) findViewById(R.id.radio_all);
		
		content = (FrameLayout)findViewById(R.id.content);
	}

	@Override
	protected void setAttribute() {
		title_tv.setText("我报名的");
		
		btn_back.setOnClickListener(this);
		//加此监听会请求多次接口，注意更改
		rgroup_title.setOnCheckedChangeListener(this);
		
		showOrderByStatus(status);
	}
	
	/**
	 * 根据不同订单显示界面
	 * 
	 * @param status
	 *            订单类型
	 */
	private void showOrderByStatus(int status) {
		int status_release = MyReleaseFragment.STATUS_ALL; // 默认为全部
		switch (status) {   //0=全部，1招募中，2=已过期，3=已关闭
		case Constants.FROM_STATUS_ALL: // 全部
			status_release = MyReleaseFragment.STATUS_ALL;
			rgroup_title.check(R.id.radio_all);
			break;
		case Constants.FROM_STATUS_RECRUIT:
			status_release = MyReleaseFragment.STATUS_RECRUIT;
			rgroup_title.check(R.id.radio_recruit);
			break;
		case Constants.FROM_STATUS_PAST:
			status_release = MyReleaseFragment.STATUS_PAST;
			rgroup_title.check(R.id.radio_past);
			break;
		case Constants.FROM_STATUS_CLOSE:
			status_release = MyReleaseFragment.STATUS_CLOSE;
			rgroup_title.check(R.id.radio_close);
			break;
		default:
			break;
		}
		showTabColors();
		currentfragment = MyApplyFragment.newInstance(status_release);
		showFragment(currentfragment);
	}
	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		
		int status_release = MyReleaseFragment.STATUS_ALL; // 默认为全部
		switch (checkedId) {
		case R.id.radio_all:
			status_release = MyReleaseFragment.STATUS_ALL;
			break;
		case R.id.radio_recruit:
			status_release = MyReleaseFragment.STATUS_RECRUIT;
			break;
		case R.id.radio_past:
			status_release = MyReleaseFragment.STATUS_PAST;
			break;
		case R.id.radio_close:
			status_release = MyReleaseFragment.STATUS_CLOSE;
			break;
		default:
			break;
		}
		showTabColors();
		currentfragment = MyApplyFragment.newInstance(status_release);
		showFragment(currentfragment);
		
	}
	
	/**
	 * 显示fragment列表
	 */
	private void showFragment(MyApplyFragment fragment) {
		FragmentTransaction transaction = fragmentmanager.beginTransaction();
		transaction.replace(R.id.content, fragment);
		transaction.commit();
	}
	
	/**
	 *  tab字体颜色显示
	 */
	private void showTabColors(){
		if(radio_recruit.isChecked() == true){
			radio_recruit.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			radio_recruit.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(radio_past.isChecked() == true){
			radio_past.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			radio_past.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(radio_close.isChecked() == true){
			radio_close.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			radio_close.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(radio_all.isChecked() == true){
			radio_all.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			radio_all.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
	}

}
