package com.mishow.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.adapter.OrganizationFragmentAdapter;
import com.mishow.adapter.OrganizationPeoplesrrRecyclerAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.widget.MyListView;
import com.mishow.widget.MyRecycleView;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：附近表演详情
 */
public class NearbyShowDetailActivity extends BaseActivity{

	private Button btn_back;
	
	MyRecycleView id_recyclerview;
	
	OrganizationPeoplesrrRecyclerAdapter mRecyclerAdapter;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,NearbyShowDetailActivity.class);
		context.startActivity(intent);
	}
	
	@Override
	protected void initData() {
		
		mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 10; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mRecyclerAdapter = new OrganizationPeoplesrrRecyclerAdapter(mContext, mRedsBeans);
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		setContentView(R.layout.activity_nearby_showdetail);
		btn_back =  (Button)findViewById(R.id.btn_back);
		id_recyclerview = (MyRecycleView)findViewById(R.id.id_recyclerview);
	}

	@Override
	protected void setAttribute() {
		setTitle("北京相声大会");
		btn_back.setOnClickListener(onClickListener);
		LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
		layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);//支付横向、纵向
		//设置布局管理器 
		id_recyclerview.setLayoutManager(layoutManager);
		id_recyclerview.setAdapter(mRecyclerAdapter);
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;

			default:
				break;
			}
		}
	};

}
