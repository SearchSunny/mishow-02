package com.mishow.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.bean.HotSearchBean;
import com.mishow.widget.AdjustLayout;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：职业类型
 */
public class ProfessionTypeActivity extends BaseActivity {

	private Button btn_back;
	private TextView title_tv;
	private TextView title_right;
	
	
	/** 平面拍摄数据 **/
	private List<HotSearchBean> planeDatas = new ArrayList<HotSearchBean>();

	
	/** 平面拍摄AdjustLayout **/
	private AdjustLayout adjust_plane_layout;

	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,ProfessionTypeActivity.class);
		context.startActivity(intent);
		
	}
	@Override
	protected void initData() {
		
		for (int i = 0; i < 5; i++) {
			
			HotSearchBean hot = new HotSearchBean();
			hot.setGhwName("平面拍摄");
			
			planeDatas.add(hot);
		}
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_profession_type);
		btn_back =  (Button)findViewById(R.id.btn_back);
		title_tv = (TextView)findViewById(R.id.title_tv);
		title_right = (TextView)findViewById(R.id.title_right);
		
		adjust_plane_layout = (AdjustLayout)findViewById(R.id.adjust_plane_layout);
	}

	@Override
	protected void setAttribute() {
		
		title_tv.setText("职业类型");
		title_right.setVisibility(View.VISIBLE);
		title_right.setText("保存");
		title_right.setTextColor(mContext.getResources().getColor(R.color.search_pop_color));
		
		btn_back.setOnClickListener(onClickListener);
		
		filledPlaneDatas();
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			default:
				break;
			}
			
		}
	};
	
	/**
	 * 填充显示平面拍摄
	 */
	private void filledPlaneDatas() {
		
		for (final HotSearchBean bean : planeDatas) {
			View view = LayoutInflater.from(this).inflate(R.layout.item_hot_search, null);
			CheckBox textView = (CheckBox) view.findViewById(R.id.hot_search_text);
			textView.setText(bean.getGhwName());
			textView.setSingleLine(true);
			textView.setEllipsize(TruncateAt.MIDDLE);
			textView.setOnCheckedChangeListener(onCheckedChangeListener);
			if (textView.isChecked()) {
				textView.setBackgroundResource(R.drawable.shape_selectitem_press);
			}
			adjust_plane_layout.addView(view);
		}
	}
	
	private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            
        	/*if (isChecked) {
                mStrings.add(s);
            } else {
                mStrings.remove(s);
            }*/
        	compoundButton.setChecked(isChecked);
            refreshSelectedCount();

        }
    };

    // refresh标题栏显示的选择数
    private void refreshSelectedCount() {
        //int total = mStrings.size();
        //mTitleRight.setText("已选 (" + total + ")");
    }
}
