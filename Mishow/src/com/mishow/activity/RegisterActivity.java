package com.mishow.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：注册页面
 */
public class RegisterActivity extends BaseActivity {
	
	/** 返回 **/
	private Button btn_back;
	/** 标题 **/
	private TextView title_tv;
	/** 手机号 **/
	private EditText edit_register_phone;
	/** 验证码 **/
	private EditText edit_register_vercode;
	/** 输入密码 **/
	private EditText edit_register_pwd;
	/** 获取验证码 **/
	private Button btn_register_vercode;
	/** 下一步 **/
	private Button btn_register_next;
	@Override
	protected void initData() {

	}

	@Override
	protected void initView(Bundle savedInstanceState) {

		setContentView(R.layout.activity_register);
		btn_back = (Button) findViewById(R.id.btn_back);
		title_tv = (TextView) findViewById(R.id.title_tv);
		
		edit_register_phone = (EditText) findViewById(R.id.edit_register_phone);
		edit_register_vercode = (EditText) findViewById(R.id.edit_register_vercode);
		edit_register_pwd = (EditText) findViewById(R.id.edit_register_pwd);
		
		btn_register_vercode = (Button) findViewById(R.id.btn_register_vercode);
		btn_register_next = (Button) findViewById(R.id.btn_register_next);
		
		
	}

	@Override
	protected void setAttribute() {

		title_tv.setText("注册");
		btn_back.setOnClickListener(onClickListener);
	}

	
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
				RegisterActivity.this.finish();
				break;
			}
		}
	};
}
