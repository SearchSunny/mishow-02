package com.mishow.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.fragment.SearchNearMapFragment;
import com.mishow.fragment.SearchNearbyRedsFragment;
import com.mishow.fragment.SearchNearbyShowFragment;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.log.MyLog;
import com.mishow.widget.CustomViewFragmentPager;
import com.mishow.widget.CustomViewFragmentPager.OnGetCurrentItem;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜附近
 */
public class SearchNearbyActivity extends BaseActivity implements OnClickListener{

	private List<BaseFragment> mFragmentList;
	/**
	 * 附近 
	 */
    private SearchNearMapFragment mNearMapFragment;  
    /**
     * 红人
     */
    private SearchNearbyRedsFragment mNearbyRedsFragment; 
    /**
     * 表演
     */
    private SearchNearbyShowFragment mNearbyShowFragment; 
	 
	/** 返回 **/
	private Button btn_back;
	/** 标题 **/
	private TextView title_tv;
	
	/** 附近 **/
	private TextView tv_search_neary;
	/** 红人 **/
	private TextView tv_search_neary_reds;
	/** 表演 **/
	private TextView tv_search_neary_show;
	
	private CustomViewFragmentPager custom_viewpager;
	
	/** fm对象 **/
	private FragmentManager fragmentManager;
	
	
	/**
	 * 跳转进入搜附近界面
	 * @param context
	 * @param keyword
	 * @param from
	 */
	public static void intoActivity(Context context) {
		Intent intent = new Intent(context, SearchNearbyActivity.class);
		context.startActivity(intent);
	}
	
	public static void intoActivity(Context context, String keyword, int from) {
		Intent intent = new Intent(context, SearchNearbyActivity.class);
		intent.putExtra("keyword", keyword);
		intent.putExtra("fromPage", from);
		context.startActivity(intent);
	}
	
	@Override
	protected void initData() {
		
		fragmentManager = getSupportFragmentManager();
		
		mFragmentList = new ArrayList<BaseFragment>(); 
		
		if (mNearMapFragment == null) {
			
			mNearMapFragment = SearchNearMapFragment.newInstance();
		}
		if (mNearbyRedsFragment == null) {
			
			mNearbyRedsFragment = SearchNearbyRedsFragment.newInstance();
		}
		if (mNearbyShowFragment == null) {
			
			mNearbyShowFragment = SearchNearbyShowFragment.newInstance();
		}
		
		mFragmentList.add(mNearMapFragment);
		mFragmentList.add(mNearbyRedsFragment);
		mFragmentList.add(mNearbyShowFragment);
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_search_nearby);
		
		btn_back = (Button) findViewById(R.id.btn_back);
		title_tv = (TextView) findViewById(R.id.title_tv);
		
		tv_search_neary = (TextView)findViewById(R.id.tv_search_neary);
		tv_search_neary_reds = (TextView)findViewById(R.id.tv_search_neary_reds);
		tv_search_neary_show = (TextView)findViewById(R.id.tv_search_neary_show);
		
		custom_viewpager = (CustomViewFragmentPager)findViewById(R.id.custom_viewpager_search);
	}

	@Override
	protected void setAttribute() {
		
		btn_back.setOnClickListener(this);
		tv_search_neary.setOnClickListener(new MyOnClickListener(0));
		tv_search_neary_reds.setOnClickListener(new MyOnClickListener(1));
		tv_search_neary_show.setOnClickListener(new MyOnClickListener(2));
		custom_viewpager.setScanScroll(false);
		custom_viewpager.setAttribute(fragmentManager,mFragmentList, R.drawable.icon_switch_red_line,3);
		//custom_viewpager.setCurrentItemInterface(this);
		
		showTabColors(0);
	
	}
	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;

		default:
			break;
		}
	}
	
	private class MyOnClickListener implements OnClickListener{

		private int index = 0;
		public MyOnClickListener(int i) {
			
			index = i;
		}
		@Override
		public void onClick(View v) {
			MyLog.d("lock", "MyOnClickListener============="+index);
			custom_viewpager.setCurrentItem(index);
			showTabColors(index);
		}
		
	}
	
	/*@Override
	public void getCurrentItem(int currentItem) {
		custom_viewpager.setCurrentItem(currentItem);
		showTabColors(currentItem);
	}*/
	
	/**
	 *  tab字体颜色显示
	 */
	private void showTabColors(int index){
		
		if(index == 0){
			tv_search_neary.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			tv_search_neary.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(index == 1){
			tv_search_neary_reds.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			tv_search_neary_reds.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(index == 2){
			tv_search_neary_show.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			tv_search_neary_show.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		
	}

}
