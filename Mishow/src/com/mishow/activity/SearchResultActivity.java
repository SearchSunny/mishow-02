package com.mishow.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.fragment.SearchAnnunciateFragment;
import com.mishow.fragment.SearchOrganizationFragment;
import com.mishow.fragment.SearchRedsFragment;
import com.mishow.fragment.SearchSpecialFragment;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.widget.CustomViewFragmentPager;
import com.mishow.widget.CustomViewFragmentPager.OnGetCurrentItem;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：搜索结果页面
 */
public class SearchResultActivity extends BaseActivity implements OnClickListener,OnGetCurrentItem{
	
	 private List<BaseFragment> mFragmentList;
	/**
	 * 通告  
	 */
    private SearchAnnunciateFragment mAnnunciateFragment;  
    /**
     * 红人
     */
    private SearchRedsFragment mRedsFragment; 
    /**
     * 专题
     */
    private SearchSpecialFragment mSpecialFragment; 
    /**
     * 机构
     */
    private SearchOrganizationFragment mOrganizationFragment;
	 
	/** 返回 **/
	private Button btn_back;
	/** 标题 **/
	private TextView title_tv;
	
	/** 通告 **/
	private TextView tv_search_annunciate;
	/** 红人 **/
	private TextView tv_search_reds;
	/** 专题 **/
	private TextView tv_search_special;
	/** 机构 **/
	private TextView tv_search_organization;
	
	private CustomViewFragmentPager custom_viewpager;
	
	/** fm对象 **/
	private FragmentManager fragmentManager;

	public static void intoActivity(Context context) {
		Intent intent = new Intent(context, SearchResultActivity.class);
		context.startActivity(intent);
	}
	
	public static void intoActivity(Context context, String keyword, int from) {
		Intent intent = new Intent(context, SearchResultActivity.class);
		intent.putExtra("keyword", keyword);
		intent.putExtra("fromPage", from);
		context.startActivity(intent);
	}
	
	@Override
	protected void initData() {
		
		fragmentManager = getSupportFragmentManager();
		
		mFragmentList = new ArrayList<BaseFragment>(); 
		
		if (mAnnunciateFragment == null) {
			
			mAnnunciateFragment = SearchAnnunciateFragment.newInstance();
		}
		if (mRedsFragment == null) {
			
			mRedsFragment = SearchRedsFragment.newInstance();
		}
		if (mSpecialFragment == null) {
			
			mSpecialFragment = SearchSpecialFragment.newInstance();
		}
		if (mOrganizationFragment == null) {
			
			mOrganizationFragment = SearchOrganizationFragment.newInstance();
		}
		
		mFragmentList.add(mAnnunciateFragment);
		mFragmentList.add(mRedsFragment);
		mFragmentList.add(mSpecialFragment);
		mFragmentList.add(mOrganizationFragment);
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_search_result);
		
		btn_back = (Button) findViewById(R.id.btn_back);
		title_tv = (TextView) findViewById(R.id.title_tv);
		
		tv_search_annunciate = (TextView)findViewById(R.id.tv_search_annunciate);
		tv_search_reds = (TextView)findViewById(R.id.tv_search_reds);
		tv_search_special = (TextView)findViewById(R.id.tv_search_special);
		tv_search_organization = (TextView)findViewById(R.id.tv_search_organization);
		
		custom_viewpager = (CustomViewFragmentPager)findViewById(R.id.custom_viewpager_search);
		
	}

	@Override
	protected void setAttribute() {
		//title_tv.setText("搜索");
		
		btn_back.setOnClickListener(this);
		tv_search_annunciate.setOnClickListener(new MyOnClickListener(0));
		tv_search_reds.setOnClickListener(new MyOnClickListener(1));
		tv_search_special.setOnClickListener(new MyOnClickListener(2));
		tv_search_organization.setOnClickListener(new MyOnClickListener(3));
		custom_viewpager.setScanScroll(true);
		custom_viewpager.setAttribute(fragmentManager,mFragmentList, R.drawable.icon_switch_red_line,4);
		custom_viewpager.setCurrentItemInterface(this);
		
		showTabColors(0);
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;

		default:
			break;
		}
	}

	private class MyOnClickListener implements OnClickListener{

		private int index = 0;
		public MyOnClickListener(int i) {
			
			index = i;
		}
		@Override
		public void onClick(View v) {
			Log.i("lock", "MyOnClickListener============="+index);
			custom_viewpager.setCurrentItem(index);
		}
		
	}
	@Override
	public void getCurrentItem(int currentItem) {
		
		showTabColors(currentItem);
	}
	
	/**
	 *  tab字体颜色显示
	 */
	private void showTabColors(int index){
		
		if(index == 0){
			tv_search_annunciate.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			tv_search_annunciate.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(index == 1){
			tv_search_reds.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			tv_search_reds.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(index == 2){
			tv_search_special.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			tv_search_special.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if(index == 3){
			tv_search_organization.setTextColor(getResources().getColor(R.color.color_tab_select));
		}else{
			tv_search_organization.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
	}

}
