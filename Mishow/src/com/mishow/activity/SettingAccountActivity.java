package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.preferenses.SharedPreferencesUtil;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：账号设置
 */
public class SettingAccountActivity extends BaseActivity {

	private Button btn_back;
	private Button btn_account_modify_pwd;
	private Button btn_account_modify_phone;
	
	/** 已绑定手机号 **/
	private TextView text_account_phone;
	private SharedPreferencesUtil preferencesUtil;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,SettingAccountActivity.class);
		context.startActivity(intent);
		
	}
	@Override
	protected void initData() {
	
		preferencesUtil = new SharedPreferencesUtil(mContext);
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_setting_account);
		btn_back =  (Button)findViewById(R.id.btn_back);
		btn_account_modify_pwd =  (Button)findViewById(R.id.btn_account_modify_pwd);
		btn_account_modify_phone =  (Button)findViewById(R.id.btn_account_modify_phone);
		
		text_account_phone = (TextView)findViewById(R.id.text_account_phone);
	}

	@Override
	protected void setAttribute() {
		setTitle("账号设置");
		text_account_phone.setText("已绑定手机号："+preferencesUtil.getLastAccount());
		btn_back.setOnClickListener(onClickListener);
		btn_account_modify_pwd.setOnClickListener(onClickListener);
		btn_account_modify_phone.setOnClickListener(onClickListener);
	}
	
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_account_modify_pwd:
				SettingModifyPwdActivity.intoActivity(mContext);
				break;
			case R.id.btn_account_modify_phone:
				SettingModifyPhoneActivity.intoActivity(mContext);
				break;
			default:
				break;
			}
		}
	};

}
