package com.mishow.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.em.EMCustom;
import com.mishow.preferenses.SharedPreferencesUtil;
import com.mishow.widget.CustomDialog;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：设置
 */
public class SettingActivity extends BaseActivity {

	private Button btn_back;
	private Button btn_exit;
	private TextView title_right;
	private RelativeLayout relative_setting_account;
	private RelativeLayout relative_setting_feedback;
	
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,SettingActivity.class);
		context.startActivity(intent);
		
	}
	@Override
	protected void initData() {
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_setting);
		btn_back =  (Button)findViewById(R.id.btn_back);
		btn_exit = (Button)findViewById(R.id.btn_exit);
		title_right = (TextView)findViewById(R.id.title_right);
		relative_setting_account = (RelativeLayout)findViewById(R.id.relative_setting_account);
		relative_setting_feedback = (RelativeLayout)findViewById(R.id.relative_setting_feedback);
		
	}

	@Override
	protected void setAttribute() {
		setTitle("设置");
		
		btn_back.setOnClickListener(onClickListener);
		btn_exit.setOnClickListener(onClickListener);
		title_right.setVisibility(View.VISIBLE);
		title_right.setText("保存");
		title_right.setTextColor(getResources().getColor(R.color.pale_red));
		relative_setting_account.setOnClickListener(onClickListener);
		relative_setting_feedback.setOnClickListener(onClickListener);
		
		initLoginOutDialog();
	}
	
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.relative_setting_account://账号设置
				SettingAccountActivity.intoActivity(mContext);
				break;
			case R.id.relative_setting_feedback:
				SettingFeedbackActivity.intoActivity(mContext);
				break;
			case R.id.btn_exit:
				if (dialog != null && !dialog.isShowing()) {
                    dialog.show();
                }
				break;
			default:
				break;
			}
		}
	};

	private CustomDialog dialog;// 注销登录对话框
	// 初始化退出登录对话框
    private void initLoginOutDialog() {
        dialog = new CustomDialog(SettingActivity.this, true);
        dialog.setTitleContent("退出当前账号","退出后将收不到任何消息，确定要退出？");
        dialog.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //doLogout();
                        SharedPreferencesUtil sp = new SharedPreferencesUtil(SettingActivity.this);
                        sp.clearUserInfo();
                        
                        EMCustom.getInstance().EMLogout(true);
                      
                        Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        mContext.finish();

                    }
                });

        dialog.setNagetiveButton("取消",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
	
	
}
