package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：设置意见反馈
 */
public class SettingFeedbackActivity extends BaseActivity {

	private Button btn_back;
	private TextView title_right;
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,SettingFeedbackActivity.class);
		context.startActivity(intent);
		
	}
	@Override
	protected void initData() {
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_setting_feedback);
		btn_back =  (Button)findViewById(R.id.btn_back);
		title_right = (TextView)findViewById(R.id.title_right);
	}

	@Override
	protected void setAttribute() {
		setTitle("意见反馈");
		btn_back.setOnClickListener(onClickListener);
		title_right.setVisibility(View.VISIBLE);
		title_right.setText("发送");
		title_right.setTextColor(getResources().getColor(R.color.pale_red));
	}
	
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;

			default:
				break;
			}
		}
	};

}
