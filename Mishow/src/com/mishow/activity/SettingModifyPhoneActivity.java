package com.mishow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：设置更换修改手机号
 */
public class SettingModifyPhoneActivity extends BaseActivity {

	private Button btn_back;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,SettingModifyPhoneActivity.class);
		context.startActivity(intent);
		
	}
	@Override
	protected void initData() {
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_setting_modifyphone);
		btn_back =  (Button)findViewById(R.id.btn_back);
	}

	@Override
	protected void setAttribute() {
		setTitle("登录");
		btn_back.setOnClickListener(onClickListener);
	}
	
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;

			default:
				break;
			}
		}
	};

}
