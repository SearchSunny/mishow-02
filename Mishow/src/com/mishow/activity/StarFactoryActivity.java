package com.mishow.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.base.BaseActivity;
import com.mishow.adapter.StarFactoryAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：星工厂
 */
public class StarFactoryActivity extends BaseActivity {
	
	private Button btn_back;
	private TextView title_tv;

	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_starFactory;
	/** 数据为空 **/
	private EmptyDataLayout ll_starFactory_empty;
	/** 机构列表 **/
	private MyListView lv_starFactory;
	/** 搜索机构列表Adapter **/
	private StarFactoryAdapter mStarFactoryAdapter ;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static void intoActivity(Context context){
		
		Intent intent = new Intent(context,StarFactoryActivity.class);
		context.startActivity(intent);
		
	}
	@Override
	protected void initData() {
		
		mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 10; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mStarFactoryAdapter = new StarFactoryAdapter(mContext, mRedsBeans);
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		setContentView(R.layout.activity_starfactory);
		title_tv = (TextView)findViewById(R.id.title_tv);
		btn_back =  (Button)findViewById(R.id.btn_back);
		
		refreshLayout_starFactory = (PullPushRefreshLayout)findViewById(R.id.refreshLayout_organization);
		ll_starFactory_empty = (EmptyDataLayout)findViewById(R.id.ll_search_organization_empty);
		lv_starFactory = (MyListView)findViewById(R.id.lv_search_organization);
	}

	@Override
	protected void setAttribute() {
		title_tv.setText("星工厂");
		btn_back.setOnClickListener(onClickListener);
		lv_starFactory.setAdapter(mStarFactoryAdapter);
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.btn_back:
					finish();
					break;
				default:
					break;
				}
				
			}
		};
		
	

}
