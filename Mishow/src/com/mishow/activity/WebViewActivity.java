package com.mishow.activity;

import android.os.Bundle;

import com.mishow.activity.base.WebViewBaseActivity;
/**
 * 作者：wei.miao <br/>
 * 描述：WebView H5跳转*/
public class WebViewActivity extends WebViewBaseActivity{
	

	/**  专题活动(从通知栏进入) **/
	public static final int TYPE_ACTIVITY_FROMNOTICE = 3;

	@Override
	protected void initData() {
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
	}

	@Override
	protected void setAttribute() {
		
	}
}
