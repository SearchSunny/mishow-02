package com.mishow.activity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.mishow.activity.base.BaseActivity;
/**
 * 作者：wei.miao <br/>
 * 描述：欢迎页面
 */
public class WelcomeActivity extends BaseActivity implements AnimationListener{

	@Override
	protected void initData() {
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
	}

	@Override
	protected void setAttribute() {
		
	}
	
	
	
	
	

	@Override
	public void onAnimationStart(Animation animation) {
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		
	}

	
}
