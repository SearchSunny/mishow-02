package com.mishow.activity.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.log.MyLog;
import com.mishow.net.NetworkUtil;
import com.mishow.preferenses.SharedPreferencesUtil;
import com.mishow.service.LocationServices;
import com.mishow.volley.http.request.RequestExtension;
import com.mishow.widget.LoadingDialog;
import com.umeng.analytics.MobclickAgent;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：所有Activity基类
 */
@SuppressLint("NewApi")
public abstract class BaseActivity extends FragmentActivity{
	
	protected BaseActivity mContext;
	/** 加载进度 对话框 **/
	//private ProgressDialog mProgressDialog;
	
	/** 当前页码 **/
	public int pageNo = 1;
	/** 每页显示的记录数 **/
	public int pageSize = 10;
	
	private LoadingDialog mProgressDialog;// 加载进度 对话框
	
	private SharedPreferencesUtil mShPreferencesUtil;
	
	private static final String TAG = BaseActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
        MyLog.i("onCreate", getLocalClassName());
		getSaveState(savedInstanceState);
		mShPreferencesUtil = new SharedPreferencesUtil(mContext);
		initData();
		initView(savedInstanceState);
		setAttribute();
		startService(new Intent(this, LocationServices.class));
		
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		MyLog.i("onResume", getLocalClassName());
		// 友盟统计页面跳转，“排除所有包含Fragment的Activity”
		if (!getClass().getSimpleName().equals("MainActivity")
				|| !getClass().getSimpleName().equals("MedicineDetailActivity")
				|| !getClass().getSimpleName().equals("WeShopActivity")) {
			
			MobclickAgent.onPageStart(getClass().getSimpleName());
		}
		// 友盟统计应用时长
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MyLog.i("onPause", getLocalClassName());
		// 统计页面跳转(保证MobclickAgent.onPageEnd在MobclickAgent.onPause前调用，因为MobclickAgent.onPause会保存信息)
		if (!getClass().getSimpleName().equals("MainActivity")
				|| !getClass().getSimpleName().equals("MedicineDetailActivity")
				|| !getClass().getSimpleName().equals("WeShopActivity")) {
			
			MobclickAgent.onPageEnd(getClass().getSimpleName());
		}
		MobclickAgent.onPause(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	/**
	 * 初始化数据
	 */
	protected abstract void initData();

	/**
	 * 初始化UI控件
	 */
	protected abstract void initView(Bundle savedInstanceState);

	/**
	 * 设置控件属性
	 */
	protected abstract void setAttribute();

	/**
	 * 获取保存的设置
	 * @param savedInstanceState
	 */
	protected void getSaveState(Bundle savedInstanceState) {};
	
	public void showProgress() {
        showProgress("", "", true);
    }
	
	/**
	 * 显示等待框
	 * 
	 * @param title
	 * @param message
	 */
	public void showProgress(String title, String message, boolean isCancel) {
		try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                return;
            }
            mProgressDialog = new LoadingDialog(this,R.style.CustomDialog);
            mProgressDialog.show();
            mProgressDialog.setCancelable(isCancel);
            TextView textView = (TextView) mProgressDialog.findViewById(R.id.progress_msg);
            if (TextUtils.isEmpty(message)) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	/**
	 * 取消等待框
	 */
	public void dismissProgress() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
	}

	
	/**
	 * 执行网络请求
	 * 
	 * @param request
	 */
	public void executeRequest(RequestExtension<?> request) {
		executeRequest(mContext,request, this);
	}
	
	public void executeRequest(Context context, RequestExtension<?> request) {
		executeRequest(context, request, TAG);
	}

	/**
	 * 执行带标志的网络请求
	 * 
	 * @param request
	 * @param tag
	 */
	public void executeRequest(Context context,RequestExtension<?> request, Object tag) {
		NetworkUtil.executeRequest(this, request, tag);
	}
	/**
	 * 设置title
	 * @param title
	 */
	protected void setTitle(String title) {
        if (findViewById(R.id.title_tv) != null && findViewById(R.id.title_tv) instanceof TextView) {
            ((TextView) findViewById(R.id.title_tv)).setText(title);
        }
    }
	/**
	 * 处理返回键
	 * @param v
	 */
	public void btnBackClose(View v){
        finish();
    }
	
}
