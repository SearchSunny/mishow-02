package com.mishow.activity.photo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.mishow.activity.DynamicReleaseActivity;
import com.mishow.activity.SelectReleaseActivity;
import com.mishow.activity.base.BaseActivity;
import com.mishow.adapter.photo.FolderAdapter;
import com.mishow.utils.Constants;
import com.mishow.utils.photo.Bimp;
import com.mishow.utils.photo.PublicWay;
import com.mishow.utils.photo.Res;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：这个类主要是用来进行显示包含图片的文件夹
 */
public class ImageFile extends BaseActivity {

	private FolderAdapter folderAdapter;
	private Button bt_cancel;
	private Context mContext;
	
	private GridView gridView;
	private TextView textView;
	
	private int release_type;
	
	@Override
	protected void initData() {
		
		Bundle bundle = getIntent().getExtras();
		release_type = bundle.getInt(Constants.IMAGE_RELEASE_TYPE);
		folderAdapter = new FolderAdapter(this,release_type);
		mContext = this;
		
	}

	@Override
	protected void initView(Bundle savedInstanceState) {
		
		setContentView(Res.getLayoutID("plugin_camera_image_file"));
		PublicWay.activityList.add(this);
		
		bt_cancel = (Button) findViewById(Res.getWidgetID("cancel"));
		
		gridView = (GridView) findViewById(Res.getWidgetID("fileGridView"));
		textView = (TextView) findViewById(Res.getWidgetID("headerTitle"));
		
		
		
	}

	@Override
	protected void setAttribute() {
		
		textView.setText(Res.getString("photo"));
		bt_cancel.setOnClickListener(new CancelListener());
		gridView.setAdapter(folderAdapter);
	}
	
	private class CancelListener implements OnClickListener {// 取消按钮的监听
		public void onClick(View v) {
			//清空选择的图片
			//Bimp.tempSelectBitmap.clear();
			Intent intent = new Intent();
			if (release_type == Constants.IMAGE_RELEASE_TYPE_DYNAMIC) {
				intent.setClass(mContext, DynamicReleaseActivity.class);
				startActivity(intent);
			}else if (release_type == Constants.IMAGE_RELEASE_TYPE_SELECT){
				intent.setClass(mContext, SelectReleaseActivity.class);
				startActivity(intent);
			}else{
				finish();
			}
			
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent();
			if (release_type == Constants.IMAGE_RELEASE_TYPE_DYNAMIC) {
				intent.setClass(mContext, DynamicReleaseActivity.class);
				startActivity(intent);
			}else if (release_type == Constants.IMAGE_RELEASE_TYPE_SELECT){
				intent.setClass(mContext, SelectReleaseActivity.class);
				startActivity(intent);
			}
		}
		
		return true;
	}

	
}
