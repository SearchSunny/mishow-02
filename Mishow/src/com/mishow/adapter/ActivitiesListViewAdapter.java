package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.bean.HomeBanner;
import com.mishow.bean.ProcureHomeAd;
import com.mishow.utils.DeviceUtil;
import com.mishow.utils.ImageLoader.ImageLoaderProxy;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：首页活动列表 Adapter
 */
public class ActivitiesListViewAdapter extends BaseAdapter{

	private Context mContext;
	private ArrayList<HomeBanner> adFocusImgList;
	
	/** 活动视图的高度 **/
	//private int adActivityHeight;

	public ActivitiesListViewAdapter(Context mContext, ArrayList<HomeBanner> adFocusImgList) {
		this.mContext = mContext;
		this.adFocusImgList = adFocusImgList;
		
		// 控制活动banner大小
		//adActivityHeight = (int) (DeviceUtil.getDeviceWidth(mContext) * 750 / 2184f);
	}

	@Override
	public int getCount() {
		return adFocusImgList == null ? 0 : adFocusImgList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_activities_list, null);
			holder.iconImg = (ImageView) convertView.findViewById(R.id.img_activities_item_icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		/*LayoutParams bannerLp = holder.iconImg.getLayoutParams();
		bannerLp.height = adActivityHeight;*/
		
		ImageLoaderProxy.getInstance(mContext).displayImage(adFocusImgList.get(position).getBannerUrl(), holder.iconImg,R.drawable.mph_drug_default);
		return convertView;
	}

	private static class ViewHolder {
		ImageView iconImg;
	}

}
