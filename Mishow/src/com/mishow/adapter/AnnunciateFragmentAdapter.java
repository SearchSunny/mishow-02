package com.mishow.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mishow.R;
import com.mishow.bean.Announcement;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.FormatNumberUtil;
import com.mishow.utils.TimeUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：搜索通告列表Adapter
 */
public class AnnunciateFragmentAdapter extends BaseAdapter{

	private ArrayList<Announcement> mAnnunciateBeans;

	private Context mContext;
	/** 小数格式化 **/
	private FormatNumberUtil numberUtil;
	
	private static SimpleDateFormat currentFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public AnnunciateFragmentAdapter(Context mContext, ArrayList<Announcement> annunciateBeans) {
		this.mContext = mContext;
		this.mAnnunciateBeans = annunciateBeans;
		numberUtil = FormatNumberUtil.getInstance(FormatNumberUtil.TYPE_CURRENCY);
	}
	@Override
	public int getCount() {
		
		return mAnnunciateBeans == null ? 0 : mAnnunciateBeans.size();
	}

	@Override
	public Object getItem(int position) {
		
		return mAnnunciateBeans.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.fragment_search_annunciate_item, null);
			holder.image_round_icon = (ImageView)convertView.findViewById(R.id.image_round_icon);
			holder.text_manager_company = (TextView)convertView.findViewById(R.id.text_manager_company);
			holder.image_item_icon = (ImageView)convertView.findViewById(R.id.image_item_icon);
			
			holder.text_update_time = (TextView)convertView.findViewById(R.id.text_update_time);
			holder.image_annunciate = (ImageView)convertView.findViewById(R.id.image_annunciate);
			holder.text_annunciate_name = (TextView)convertView.findViewById(R.id.text_annunciate_name);
			holder.texdt_start_date = (TextView)convertView.findViewById(R.id.texdt_start_date);
			
			holder.text_person_number = (TextView)convertView.findViewById(R.id.text_person_number);
			holder.text_sex = (TextView)convertView.findViewById(R.id.text_sex);
			holder.text_height = (TextView)convertView.findViewById(R.id.text_height);
			
			holder.text_city = (TextView)convertView.findViewById(R.id.text_city);
			holder.text_price = (TextView)convertView.findViewById(R.id.text_price);
			holder.image_status = (ImageView)convertView.findViewById(R.id.image_status);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Announcement announcement = mAnnunciateBeans.get(position);
		if (announcement != null) {
			
			if (announcement.getUpdateTime() != null && !announcement.getUpdateTime().equals("")) {
				
				//String currentTime = currentFormat.format(announcement.getUpdateTime().toString());
				//String str_time = TimeUtil.update_time(announcement.getUpdateTime().toString());
				//holder.text_update_time.setText(str_time);
			}
			holder.text_annunciate_name.setText(announcement.getAnnouncementTheme());
			holder.text_city.setText(announcement.getAnnouncementCity());
			if (announcement.getAnnouncementState() != null) {
				
				if (announcement.getAnnouncementState() == 2) {
					holder.image_status.setVisibility(View.VISIBLE);
				}else{
					holder.image_status.setVisibility(View.GONE);
				}
			}
			
			if (announcement.getCreateTime() != null && announcement.getAnnouncementEndtime() != null) {
				
				String endTime = AndroidUtil.convertToTime(announcement.getCreateTime());
				
				String createTime = AndroidUtil.convertToTime(announcement.getUpdateTime());
				
				
				holder.texdt_start_date.setText("时间："+createTime+"至"+endTime);
			}
			if (announcement.getAnnouncementNumber() != null) {
				
				holder.text_person_number.setText(announcement.getAnnouncementNumber()+"");
			}
			
			//0-保密,1-男,2-女
			if (announcement.getAnnouncementSex() != null &&
					!announcement.getAnnouncementSex().equals("")){
				
				if(announcement.getAnnouncementSex().equals("0")){
					holder.text_sex.setText("保密");
				}else if(announcement.getAnnouncementSex().equals("1")){
					holder.text_sex.setText("男");
				}else if(announcement.getAnnouncementSex().equals("2")){
					holder.text_sex.setText("女");
				}
			}
			
			if (announcement.getAnnouncementRemunerationUnit() != null) {
				
				String price = announcement.getAnnouncementRemuneration().toString();
				if (announcement.getAnnouncementRemunerationUnit() == 0) {
					holder.text_price.setText(price+"/人");
				}else if (announcement.getAnnouncementRemunerationUnit() == 1){
					holder.text_price.setText(price+"/人/天");
				}else if (announcement.getAnnouncementRemunerationUnit() == 2){
					holder.text_price.setText(price+"/人/小时");
				}
			}
		}
		
		
		return convertView;
	}

	
	private static class ViewHolder {
		/** 经纪人头像 **/
		ImageView image_round_icon;
		/** 经纪公司名称 **/
		TextView text_manager_company;
		/** 是否实名 **/
		ImageView image_item_icon;
		/** 发布更新时间 **/
		TextView text_update_time;
		/** 通告图片 **/
		ImageView image_annunciate;
		/** 通告主题 **/
		TextView text_annunciate_name;
		/** 通告开始-结束时间  **/
		TextView texdt_start_date;
		/** 通告人数  **/
		TextView text_person_number;
		/** 性别  **/
		TextView text_sex;
		/** 身高  **/
		TextView text_height;
		
		/** 通告城市 **/
		TextView text_city;
		/** 通告价格  **/
		TextView text_price;
		/** 通告状态(-1-不显示 0-未开始 1-进行中 2-已关闭) **/
		ImageView image_status;
		
	}
}
