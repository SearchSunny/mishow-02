package com.mishow.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：首页Banner适配器
 */
public class BannerPageAdapter extends PagerAdapter{

	private ArrayList<ImageView> views;
	private Context context;

	public BannerPageAdapter(Context context, ArrayList<ImageView> views) {
		this.context = context;
		this.views = views;
	}

	@Override
	public int getCount() {
		if (views == null) {
			return 0;
		}
		return views.size();

	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		if (views.size() > position) {
			container.removeView(views.get(position));
		}
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		container.addView(views.get(position));
		return views.get(position);
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

}
