package com.mishow.adapter;


import com.mishow.R;
import com.mishow.utils.DeviceUtil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：首页Banner上显示的圆点适配器
 */
public class BannerPointAdapter extends BaseAdapter{


	public static final int STORE_BANNER = 2;
	
	private Integer[] store_thumbIds={R.drawable.banner_point_default,
			R.drawable.banner_point_hov};
	private Integer[] old_thumbIds={R.drawable.banner_point_default,
			R.drawable.banner_point_hov};
	
	private Integer[] thumbIds ;
	private Context context;
	private int size;
	private int currentIndex = 0;
	private int type=1;
	private int height;//图片高度

	public BannerPointAdapter(Context context, int size) {
		this.context = context;
		this.size = size;
		height = DeviceUtil.dip2px(context, 8);
	}

	//设置当前显示的position
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	@Override
	public int getCount() {
		return size;
	}

	@Override
	public Object getItem(int position) {
		int index = 0;
		if (position == currentIndex) {
			index = 0;
		} else {
			index = 1;
		}
		return thumbIds[index];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView img = new ImageView(context);
		img.setLayoutParams(new GridView.LayoutParams(
				LayoutParams.WRAP_CONTENT,
				height));
		if (type==STORE_BANNER) {
			thumbIds=store_thumbIds;
		}else {
			thumbIds=old_thumbIds;
		}
		// 红球表示选中状态
		if (position == currentIndex) {
			img.setImageResource(thumbIds[1]);
		} else {
			img.setImageResource(thumbIds[0]);
		}
		img.setScaleType(ScaleType.FIT_CENTER);
		img.setAdjustViewBounds(true);
		return img;
	}
	
	public void setPointType(int type){
		this.type=type;
	}
	

}
