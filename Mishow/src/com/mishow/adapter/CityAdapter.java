package com.mishow.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.utils.DeviceUtil;

/**
 * 城市适配器
 *
 */
public class CityAdapter extends BaseAdapter {

    private Context context;

    private ArrayList<String> strList;

    private int itemWidth;

    private int itemHeight;

    public CityAdapter(Context context, ArrayList<String> strList) {
        this.context = context;
        this.strList = strList;
        itemWidth = (int) ((DeviceUtil.getDeviceWidth(context) - DeviceUtil
                .dip2px(context, 90)) / 3);
        itemHeight = itemWidth / 2;
    }

    @Override
    public int getCount() {
        return strList.size();
    }

    @Override
    public Object getItem(int position) {
        return strList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.city_item, null);
            convertView
                    .setLayoutParams(new LayoutParams(itemWidth, itemHeight));
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.btn_city.setText(strList.get(position));
        return convertView;
    }

    class ViewHolder {
        TextView btn_city;

        public ViewHolder(View convertView) {
            btn_city = (TextView) convertView.findViewById(R.id.btn_city);
        }
    }

}
