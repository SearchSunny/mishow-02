package com.mishow.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.bean.Area;
import com.mishow.bean.MenuItemBean;

/**
 * 菜单列表List适配器
 */
public class HotAnnunciationFiltrateAdapter extends BaseAdapter {
    //上下文
	private Context mContext;
	//菜单列表
	private List<MenuItemBean> mListData;
	
	private OnClickListener onClickListener;
	private OnItemClickListener mOnItemClickListener;

	/**
	 * 
	 * @param context
	 * @param listData 要显示的数据列表
	 * @param sId 数据选中背景颜色
	 * @param nId 数据未选中背景颜色
	 */
	public HotAnnunciationFiltrateAdapter(Context context, List<MenuItemBean> listData) {
		mContext = context;
		mListData = listData;
	}

	@Override
	public int getCount() {
		return mListData.size();
	}

	@Override
	public Object getItem(int position) {
		return mListData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_hot_annunciate_all,null);
			holder.txt_hot_annunciation_all = (TextView) convertView.findViewById(R.id.txt_hot_annunciation_all);
			convertView.setTag(holder);
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.txt_hot_annunciation_all.setText(mListData.get(position).getName());
		
		return convertView;
	}

	private class ViewHolder{
		
		TextView txt_hot_annunciation_all;
	}

}
