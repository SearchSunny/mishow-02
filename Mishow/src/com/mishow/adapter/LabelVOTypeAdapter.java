package com.mishow.adapter;

import java.util.ArrayList;
import java.util.Iterator;

import com.mishow.R;
import com.mishow.inteface.LabelSelectRefreshInteface;
import com.mishow.request.result.LabelVO;
import com.mishow.widget.AdjustLayout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：标签列表Adapter
 */
public class LabelVOTypeAdapter extends BaseAdapter {

	private ArrayList<LabelVO> mLableVos;
	/** 是否支持单选 **/
	private boolean mIsRadio;
	/** 用户已选择的标签 **/
	private ArrayList<String> mStrings;
	private Context mContext;

	private ArrayList<String> mLabels;

	private LabelSelectRefreshInteface mLabelSelectRefresh;
	
	
	public LabelVOTypeAdapter(Context mContext, ArrayList<String> mTags,
			ArrayList<LabelVO> lableVos, boolean isRadio,LabelSelectRefreshInteface labelSelectRefresh) {
		this.mContext = mContext;
		this.mLableVos = lableVos;
		this.mIsRadio = isRadio;
		this.mStrings = mTags;
		this.mLabelSelectRefresh = labelSelectRefresh;
		mLabels = new ArrayList<String>();
	}

	@Override
	public int getCount() {

		return mLableVos == null ? 0 : mLableVos.size();
	}

	@Override
	public Object getItem(int position) {

		return null;
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.activity_label_type_item, null);

			holder.text_lable_title = (TextView) convertView
					.findViewById(R.id.text_lable_title);
			holder.adjust_plane_layout = (AdjustLayout) convertView
					.findViewById(R.id.adjust_plane_layout);
			holder.view_line = convertView.findViewById(R.id.view_tag_line);
			convertView.setTag(holder);

		} else {

			holder = (ViewHolder) convertView.getTag();
		}
		
		if (position == mLableVos.size() - 1) {
			
			holder.view_line.setVisibility(View.GONE);
			
		}else{
			
			holder.view_line.setVisibility(View.VISIBLE);
		}
		
		LabelVO labelVO = mLableVos.get(position);
		holder.text_lable_title.setText(labelVO.getLabelClass());

		mLabels.clear();
		for (int i = 0; i < labelVO.getLabelLowers().size(); i++) {

			String labelNmae = labelVO.getLabelLowers().get(i).getLabelname();
			mLabels.add(labelNmae);

		}
		setUpAllTags(mLabels, holder.adjust_plane_layout);

		return convertView;
	}

	/**
	 * 获取标签数据成功
	 * 
	 * @param tags
	 *            服务端返回的标签
	 */
	private void setUpAllTags(ArrayList<String> tags,
			final AdjustLayout mAjustLayout) {

		/*Iterator<String> ite = mStrings.iterator();
		while (ite.hasNext()) {

			String string = (String) ite.next();
			if (!tags.contains(string))

				ite.remove();
		}*/

		for (final String s : tags) {
			final CheckBox checkbox = (CheckBox) LayoutInflater.from(mContext)
					.inflate(R.layout.view_tag_checkbox, null);
			checkbox.setText(s);
			checkbox.setChecked(mStrings.contains(s));
			checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton compoundButton,
						boolean b) {
					if (mIsRadio) {
						for (int j = 0; j < mAjustLayout.getChildCount(); j++) {
							mStrings = new ArrayList<String>();
							((CheckBox) mAjustLayout.getChildAt(j))
									.setChecked(false);
						}
					}
					if (b) {
						mStrings.add(s);
					} else {
						mStrings.remove(s);
					}
					checkbox.setChecked(b);

					mLabelSelectRefresh.refreshSelectedCount(mStrings);
				}
			});
			mAjustLayout.addView(checkbox);
			mLabelSelectRefresh.refreshSelectedCount(mStrings);
		}
	}

	private static class ViewHolder {
		TextView text_lable_title;
		AdjustLayout adjust_plane_layout;
		View view_line;
	}
}
