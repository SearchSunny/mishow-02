package com.mishow.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.bean.Area;

/**
 * 菜单列表List适配器
 */
public class MenuItemAdapter extends BaseAdapter {
    //上下文
	private Context mContext;
	//菜单列表
	private List<Area> mListData;
	private int selectedPos = -1;
	private String selectedText = "";
	/** 未选中背景图 **/
	private int normalDrawbleId;
	/** 选中的背景图 **/
	private int selectedDrawble;
	private float textSize;
	private OnClickListener onClickListener;
	private OnItemClickListener mOnItemClickListener;

	/**
	 * 
	 * @param context
	 * @param listData 要显示的数据列表
	 * @param sId 数据选中背景颜色
	 * @param nId 数据未选中背景颜色
	 */
	public MenuItemAdapter(Context context, List<Area> listData, int sId, int nId) {
		mContext = context;
		mListData = listData;
		selectedDrawble = sId;
		normalDrawbleId = nId;
		init();
	}

	private void init() {
		onClickListener = new OnClickListener() {

			@Override
			public void onClick(View view) {
				selectedPos = (Integer) view.getTag();
				setSelectedPosition(selectedPos);
				if (mOnItemClickListener != null) {
					mOnItemClickListener.onItemClick(view, selectedPos);
				}
			}
		};
	}
	/**
	 * 设置选中的position,并通知刷新其它列表
	 */
	public void setSelectedPosition(int pos) {
		if (mListData != null && pos < mListData.size()) {
			selectedPos = pos;
			selectedText = mListData.get(pos).getName();
			notifyDataSetChanged();
		}
	}

	/**
	 * 设置选中的position,但不通知刷新
	 */
	public void setSelectedPositionNoNotify(int pos,ArrayList<Area> list) {
		selectedPos = pos;
		mListData = list;
		if (mListData != null && pos < mListData.size()) {
			selectedText = mListData.get(pos).getName();
		} 
	}

	/**
	 * 获取选中的position
	 */
	public int getSelectedPosition() {
		
		if (mListData != null && selectedPos < mListData.size()) {
			return selectedPos;
		}

		return -1;
	}

	/**
	 * 设置列表字体大小
	 */
	public void setTextSize(float tSize) {
		textSize = tSize;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView view;
		if (convertView == null) {
			view = (TextView) LayoutInflater.from(mContext).inflate(R.layout.choose_item, parent, false);
		} else {
			view = (TextView) convertView;
		}
		view.setTag(position);
		String mString = "";
		if (mListData != null) {
			if (position < mListData.size()) {
				mString = mListData.get(position).getName();
			}
		}
		if (mString.contains("不限"))
			view.setText("不限");
		else
			view.setText(mString);
		view.setTextSize(TypedValue.COMPLEX_UNIT_SP,textSize);

		if (selectedText != null && selectedText.equals(mString)) {
			
			//设置选中背景图
			
			view.setBackgroundDrawable(mContext.getResources().getDrawable(selectedDrawble));
			view.setTextColor(mContext.getResources().getColor(R.color.color_profession_name));
		} else {
			//设置未选中的背景图
			view.setBackgroundDrawable(mContext.getResources().getDrawable(normalDrawbleId));
			view.setTextColor(mContext.getResources().getColor(R.color.color_annunciate_name));
		}
		view.setOnClickListener(onClickListener);
		return view;
	}
	

	public void setOnItemClickListener(OnItemClickListener l) {
		mOnItemClickListener = l;
	}

	/**
	 * 重新定义菜单选项单击接口
	 */
	public interface OnItemClickListener {
		public void onItemClick(View view, int position);
	}

	@Override
	public int getCount() {
		return mListData.size();
	}

	@Override
	public Object getItem(int position) {
		return mListData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}
