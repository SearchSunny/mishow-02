package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.bean.MenuItemBean;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：菜单弹出窗口Adapter
 */
public class MenuPopAdatper extends BaseAdapter {

	private Context mContext;//Context 对象
	private ArrayList<MenuItemBean> menuContents;//内容
	
	public MenuPopAdatper(Context mContext, ArrayList<MenuItemBean> menuContents) {
		this.mContext = mContext;
		this.menuContents = menuContents;
	}
	
	@Override
	public int getCount() {
		return menuContents == null ? 0 : menuContents.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_menu_pop, null);
			holder.nameTxt = (TextView) convertView.findViewById(R.id.txt_name);
			holder.view_line = convertView.findViewById(R.id.view_line);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (position != 0 && menuContents.size()-1 == position) {
			
			holder.view_line.setVisibility(View.GONE);
			
		}else{
			
			holder.view_line.setVisibility(View.VISIBLE);
		}
		
		holder.nameTxt.setText(menuContents.get(position).getName());
		return convertView;
	}

	private static class ViewHolder {
		private TextView nameTxt;
		private View view_line;
	}
}
