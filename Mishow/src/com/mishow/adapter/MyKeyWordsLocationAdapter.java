package com.mishow.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.mishow.R;

import java.util.ArrayList;

/**
 *
 */

public class MyKeyWordsLocationAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<PoiItem> mPoiItems;
    public  MyKeyWordsLocationAdapter(Context context, ArrayList<PoiItem> poiItemList){

        this.mContext = context;
        this.mPoiItems = poiItemList;

    }
    @Override
    public int getCount() {
        return mPoiItems != null ? mPoiItems.size(): 0;
    }

    @Override
    public Object getItem(int i) {
        return mPoiItems != null ? mPoiItems.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext,R.layout.item_clinic_location_layout, null);
            holder.txt_poititle = (TextView) convertView.findViewById(R.id.txt_poititle);
            holder.txt_poisnippet = (TextView) convertView.findViewById(R.id.txt_poisnippet);
            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        PoiItem item = mPoiItems.get(position);
        holder.txt_poititle.setText(item.getTitle());
        holder.txt_poisnippet.setText(item.getCityName()+item.getAdName()+item.getSnippet());
        return convertView;
    }

    private class ViewHolder {
        /** POI的名称 **/
        TextView txt_poititle;
        /** POI的地址 **/
        TextView txt_poisnippet;
    }
}
