package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.bean.Announcement;
import com.mishow.utils.AndroidUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyReleaseAdapter extends BaseAdapter{
	
	private Context mContext;
	private ArrayList<Announcement> mReleases;
	private boolean myType;
	
	/**
	 * 
	 * @param context
	 * @param releases
	 * @param type 我的类型(true=我发布的false=我报名的)
	 */
	public MyReleaseAdapter(Context context,ArrayList<Announcement> releases,boolean type) {
		
		this.mContext = context;
		this.mReleases = releases;
		this.myType = type;
	}

	@Override
	public int getCount() {
		return mReleases == null ? 0 : mReleases.size();
	}

	@Override
	public Object getItem(int position) {
		return mReleases.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.fragment_myrelease_item, null);
			
			holder.text_update_time = (TextView)convertView.findViewById(R.id.text_update_time);
			holder.image_annunciate = (ImageView)convertView.findViewById(R.id.image_annunciate);
			holder.text_annunciate_name = (TextView)convertView.findViewById(R.id.text_annunciate_name);
			holder.texdt_start_date = (TextView)convertView.findViewById(R.id.texdt_start_date);
			
			holder.text_person_number = (TextView)convertView.findViewById(R.id.text_person_number);
			holder.text_sex = (TextView)convertView.findViewById(R.id.text_sex);
			holder.text_height = (TextView)convertView.findViewById(R.id.text_height);
			
			holder.text_city = (TextView)convertView.findViewById(R.id.text_city);
			holder.text_price = (TextView)convertView.findViewById(R.id.text_price);
			holder.text_type = (TextView)convertView.findViewById(R.id.text_type);
			holder.relative_update_time = (RelativeLayout)convertView.findViewById(R.id.relative_update_time);
			holder.text_share = (TextView)convertView.findViewById(R.id.text_share);
			holder.view_succ = (View)convertView.findViewById(R.id.view_succ);
			convertView.setTag(holder);
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (myType) {
			holder.view_succ.setVisibility(View.VISIBLE);
			holder.relative_update_time.setVisibility(View.VISIBLE);
		}else{
			holder.view_succ.setVisibility(View.GONE);
			holder.relative_update_time.setVisibility(View.GONE);
		}
		Announcement announcement = mReleases.get(position);
		if (announcement != null) {
			
			holder.text_annunciate_name.setText(announcement.getAnnouncementTheme());
			holder.text_city.setText(announcement.getAnnouncementCity());
			
			if (announcement.getCreateTime() != null && announcement.getAnnouncementEndtime() != null) {
				
				String endTime = AndroidUtil.convertToTime(announcement.getCreateTime());
				
				String createTime = AndroidUtil.convertToTime(announcement.getUpdateTime());
				
				
				holder.texdt_start_date.setText("时间："+createTime+"至"+endTime);
			}
			if (announcement.getAnnouncementNumber() != null) {
				
				holder.text_person_number.setText(announcement.getAnnouncementNumber()+"");
			}
			
			//0-保密,1-男,2-女
			if (announcement.getAnnouncementSex() != null &&
					!announcement.getAnnouncementSex().equals("")){
				
				if(announcement.getAnnouncementSex().equals("0")){
					holder.text_sex.setText("保密");
				}else if(announcement.getAnnouncementSex().equals("1")){
					holder.text_sex.setText("男");
				}else if(announcement.getAnnouncementSex().equals("2")){
					holder.text_sex.setText("女");
				}
			}
			
			if (announcement.getAnnouncementRemunerationUnit() != null) {
				
				String price = announcement.getAnnouncementRemuneration().toString();
				if (announcement.getAnnouncementRemunerationUnit() == 0) {
					holder.text_price.setText(price+"/人");
				}else if (announcement.getAnnouncementRemunerationUnit() == 1){
					holder.text_price.setText(price+"/人/天");
				}else if (announcement.getAnnouncementRemunerationUnit() == 2){
					holder.text_price.setText(price+"/人/小时");
				}
			}
		}
		
		return convertView;
	}
	
	private static class ViewHolder {
		
		/** 发布更新时间 **/
		TextView text_update_time;
		/** 通告图片 **/
		ImageView image_annunciate;
		/** 通告主题 **/
		TextView text_annunciate_name;
		/** 通告开始-结束时间  **/
		TextView texdt_start_date;
		/** 通告人数  **/
		TextView text_person_number;
		/** 性别  **/
		TextView text_sex;
		/** 身高  **/
		TextView text_height;
		
		/** 通告城市 **/
		TextView text_city;
		/** 通告价格  **/
		TextView text_price;
		/** 职业类型 **/
		TextView text_type;
		/** 分享 **/
		TextView text_share;
		
		RelativeLayout relative_update_time;
		
		View view_succ;
		
	}
	
	

}
