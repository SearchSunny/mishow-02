package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.adapter.MySelectRecyclerAdapter.OnItemClickListener;
import com.mishow.bean.PhotoVO;
import com.mishow.bean.RedsBean;
import com.mishow.log.MyLog;
import com.mishow.widget.MyRecycleView;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：精选列表Adapter
 */
public class MySelectAdapter extends BaseAdapter{

	private ArrayList<PhotoVO> mPhotoVOs;

	private Context mContext;
	
	//private MySelectRecyclerAdapter mRecyclerAdapter;
	
	public MySelectAdapter(Context mContext, ArrayList<PhotoVO> photovo) {
		
		this.mContext = mContext;
		this.mPhotoVOs = photovo;
	}
	@Override
	public int getCount() {
		
		return mPhotoVOs == null ? 0 : mPhotoVOs.size();
	}

	@Override
	public Object getItem(int position) {
		
		return mPhotoVOs.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.activity_myselect_item, null);
			holder = new ViewHolder();
			holder.id_recyclerview = (MyRecycleView)convertView.findViewById(R.id.id_recyclerview);
			holder.text_select_name = (TextView)convertView.findViewById(R.id.text_select_name);
			convertView.setTag(holder);
			
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		PhotoVO photoVO = mPhotoVOs.get(position);
		holder.text_select_name.setText(photoVO.getPhotoName());
		MySelectRecyclerAdapter mRecyclerAdapter = new MySelectRecyclerAdapter(mContext, photoVO.getImageList());
		LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
		layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);//支付横向、纵向
		//设置布局管理器 
		holder.id_recyclerview.setLayoutManager(layoutManager);
		holder.id_recyclerview.setAdapter(mRecyclerAdapter);
		mRecyclerAdapter.notifyDataSetChanged();
		
		mRecyclerAdapter.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onLongClick(int position) {
				
				MyLog.d("recycleronLongClick===="+position);
			}
			
			@Override
			public void onClick(int position) {
				
				MyLog.d("recyclerOnClick===="+position);
			}
		});
		
		
		return convertView;
	}

	
	private static class ViewHolder {
		TextView text_select_name;
		RecyclerView id_recyclerview;
	}
}
