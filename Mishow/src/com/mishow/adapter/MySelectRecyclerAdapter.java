package com.mishow.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mishow.R;
import com.mishow.adapter.MySelectRecyclerAdapter.MyViewHolder;
import com.mishow.utils.ImageLoader.ImageLoaderProxy;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：精选列表Adapter
 */
public class MySelectRecyclerAdapter extends RecyclerView.Adapter<MyViewHolder>{

	private ArrayList<String> mSelectImages;

	private Context mContext;
	
	public MySelectRecyclerAdapter(Context mContext, ArrayList<String> images) {
		this.mContext = mContext;
		this.mSelectImages = images;
	}
	

	@Override
	public int getItemCount() {
		
		return mSelectImages == null ? 0 : mSelectImages.size();
	}

	//填充onCreateViewHolder方法返回的holder中的控件
	@Override
	public void onBindViewHolder(MyViewHolder holder, final int position) {
		
		//holder.tv_nikename.setText("ww");
		ImageLoaderProxy.getInstance(mContext).displayImage(mSelectImages.get(position), holder.iv_drug_pic, R.drawable.mph_drug_default);
		if( mOnItemClickListener!= null){  
            holder.iv_drug_pic.setOnClickListener( new OnClickListener() {  
                   
                  @Override  
                  public void onClick(View v) {  
                       mOnItemClickListener.onClick(position);  
                 }  
            });  
              
            holder.iv_drug_pic.setOnLongClickListener( new OnLongClickListener() {  
                  @Override  
                  public boolean onLongClick(View v) {  
                       mOnItemClickListener.onLongClick(position);  
                        return false;  
                 }  
            });
		}
		
	}

	//重写onCreateViewHolder方法，返回一个自定义的ViewHolder 
	//创建新View，被LayoutManager所调用
	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		
		MyViewHolder holder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_myselect_recycler_list, viewGroup,false));
		
        return holder;
	}
	
	public  class MyViewHolder extends ViewHolder{
		
		ImageView iv_drug_pic;
		
		public MyViewHolder(View view) {
			super(view);
			
			iv_drug_pic = (ImageView)view.findViewById(R.id.iv_drug_pic);
		}
		
	}
	
	private OnItemClickListener mOnItemClickListener;
	
	//给RecyclerView的Item添加点击事件
	public interface OnItemClickListener{
        void onClick( int position);
        void onLongClick( int position);
     }
	public void setOnItemClickListener(OnItemClickListener onItemClickListener ){
		
        this.mOnItemClickListener = onItemClickListener;
     }

}
