package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.bean.RedsBean;
import com.mishow.widget.HorizontalListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：机构艺人列表Adapter
 */
public class OrganizationPeoplesAdapter extends BaseAdapter{

	private ArrayList<RedsBean> mRedsBeans;

	private Context mContext;
	
	public OrganizationPeoplesAdapter(Context mContext, ArrayList<RedsBean> redsBeans) {
		this.mContext = mContext;
		this.mRedsBeans = redsBeans;
	}
	@Override
	public int getCount() {
		
		return mRedsBeans == null ? 0 : mRedsBeans.size();
	}

	@Override
	public Object getItem(int position) {
		
		return null;
	}

	@Override
	public long getItemId(int position) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_organization_peoples_list, null);
			holder = new ViewHolder();
			
			convertView.setTag(holder);
			
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		return convertView;
	}

	
	private static class ViewHolder {
		TextView contentTxt;
		ImageView iconImg;
		LinearLayout linear_person_mark;
	}
}
