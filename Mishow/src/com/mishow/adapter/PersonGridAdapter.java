package com.mishow.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mishow.R;
import com.mishow.bean.HomeStarShow;
import com.mishow.utils.DeviceUtil;
import com.mishow.utils.ImageLoader.ImageLoaderProxy;
import com.mishow.widget.AdjustLayout;
import com.mishow.widget.RoundCornerImageView;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：首页个人星秀Adapter
 */
public class PersonGridAdapter extends BaseAdapter{

	private Context mContext;
	private ArrayList<HomeStarShow> adFocusImgList;

	public PersonGridAdapter(Context mContext, ArrayList<HomeStarShow> adFocusImgList) {
		this.mContext = mContext;
		this.adFocusImgList = adFocusImgList;
	}

	@Override
	public int getCount() {
		return adFocusImgList == null ? 0 : adFocusImgList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_person_grid, null);
			holder.iconImg = (ImageView) convertView.findViewById(R.id.img_item_icon);
			holder.text_nikename = (TextView)convertView.findViewById(R.id.text_nikename);
			holder.img_round_icon = (RoundCornerImageView)convertView.findViewById(R.id.img_round_icon);
			holder.adjust_label_layout = (AdjustLayout)convertView.findViewById(R.id.adjust_label_layout);
			
			convertView.setTag(holder);
			
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.text_nikename.setText(adFocusImgList.get(position).getUserCname());
		String userProfession = adFocusImgList.get(position).getUserProfession();
		if (userProfession != null && !userProfession.equals("")) {
			holder.adjust_label_layout.setVisibility(View.VISIBLE);
			if (userProfession.contains(",")) {
				
				String [] profession = userProfession.split(",");
				
				setUpAllTags(Arrays.asList(profession), holder.adjust_label_layout);
			}else{
				holder.adjust_label_layout.removeAllViews();
				final CheckBox checkbox = (CheckBox) LayoutInflater.from(mContext)
						.inflate(R.layout.view_tag_index_checkbox, null);
				checkbox.setText(userProfession);
				checkbox.setTextColor(mContext.getResources().getColor(R.color.white));
				holder.adjust_label_layout.addView(checkbox);
			}
		}else{
			
			holder.adjust_label_layout.setVisibility(View.INVISIBLE);
		}
		
		String userHead = adFocusImgList.get(position).getUserHead();
		if (userHead != null && !userHead.equals("")) {
			ImageLoaderProxy.getInstance(mContext).displayImage(adFocusImgList.get(position).getUserHead(), holder.img_round_icon, R.drawable.mph_drug_default);
		}
		ImageLoaderProxy.getInstance(mContext).displayImage(adFocusImgList.get(position).getUserTopimg(), holder.iconImg, R.drawable.mph_drug_default);
		return convertView;
	}

	private static class ViewHolder {
		/** 昵称 **/
		private TextView text_nikename;
		/** 图片 **/
		private ImageView iconImg;
		/** 圆形图片 **/
		private RoundCornerImageView img_round_icon;
		/** 人个标签  **/
		private AdjustLayout adjust_label_layout;
	}
	
	/**
	 * 获取标签数据成功
	 * 
	 * @param tags
	 *            服务端返回的标签
	 */
	private void setUpAllTags(List<String> tags,
			final AdjustLayout mAjustLayout) {
		mAjustLayout.removeAllViews();
		for (final String s : tags) {
			CheckBox checkbox = (CheckBox) LayoutInflater.from(mContext)
					.inflate(R.layout.view_tag_index_checkbox, null);
			checkbox.setText(s);
			checkbox.setTextColor(mContext.getResources().getColor(R.color.white));
			mAjustLayout.addView(checkbox);
		}
	}

}
