package com.mishow.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.mishow.R;
import com.mishow.adapter.PersonalDynamicRecyclerAdapter.MyViewHolder;
import com.mishow.bean.RedsBean;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：个人主页动态Adapter
 */
public class PersonalDynamicRecyclerAdapter extends RecyclerView.Adapter<MyViewHolder>{

	private ArrayList<String> mChats;

	private Context mContext;
	
	public PersonalDynamicRecyclerAdapter(Context mContext, ArrayList<String> chats) {
		this.mContext = mContext;
		this.mChats = chats;
	}
	

	@Override
	public int getItemCount() {
		
		return mChats == null ? 0 : mChats.size();
	}

	//填充onCreateViewHolder方法返回的holder中的控件
	@Override
	public void onBindViewHolder(MyViewHolder holder, final int position) {
		
		//holder.tv_nikename.setText("ww");
		
		if( mOnItemClickListener!= null){  
            holder.iv_select_pic.setOnClickListener( new OnClickListener() {  
                   
                  @Override  
                  public void onClick(View v) {  
                       mOnItemClickListener.onClick(position);  
                 }  
            });  
              
            holder.iv_select_pic.setOnLongClickListener( new OnLongClickListener() {  
                  @Override  
                  public boolean onLongClick(View v) {  
                       mOnItemClickListener.onLongClick(position);  
                        return false;  
                 }  
            });
		}
		
	}

	//重写onCreateViewHolder方法，返回一个自定义的ViewHolder 
	//创建新View，被LayoutManager所调用
	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
		
		MyViewHolder holder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_personal_dynamic, viewGroup,false));
		
        return holder;
	}
	
	public  class MyViewHolder extends ViewHolder{
		
		ImageView iv_select_pic;
		
		public MyViewHolder(View view) {
			super(view);
			
			iv_select_pic = (ImageView)view.findViewById(R.id.iv_select_pic);
		}
		
	}
	
	private OnItemClickListener mOnItemClickListener;
	
	//给RecyclerView的Item添加点击事件
	public interface OnItemClickListener{
        void onClick( int position);
        void onLongClick( int position);
     }
	public void setOnItemClickListener(OnItemClickListener onItemClickListener ){
		
        this.mOnItemClickListener = onItemClickListener;
     }

}
