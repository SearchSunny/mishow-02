package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.bean.HomeShortcutMenu;
import com.mishow.utils.ImageLoader.ImageLoaderProxy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜附近-超星榜-热通告-星工厂
 */
@SuppressLint("NewApi")
public class ProcureGridAdapter extends BaseAdapter{

	private Context mContext;
	private ArrayList<HomeShortcutMenu> adFocusImgList;

	public ProcureGridAdapter(Context mContext, ArrayList<HomeShortcutMenu> adFocusImgList) {
		this.mContext = mContext;
		this.adFocusImgList = adFocusImgList;
	}

	@Override
	public int getCount() {
		return adFocusImgList == null ? 0 : adFocusImgList.size();
	}

	@Override
	public Object getItem(int position) {
		
		return  adFocusImgList == null ? 0 : adFocusImgList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_procure_grid, null);
			holder.contentTxt = (TextView) convertView.findViewById(R.id.txt_item_content);
			holder.iconImg = (ImageView) convertView.findViewById(R.id.img_item_icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.contentTxt.setTextColor(Color.parseColor("#777777"));
		// 内容
		holder.contentTxt.setText(adFocusImgList.get(position).getTitle());
		// 图片
		holder.iconImg.setBackgroundResource(adFocusImgList.get(position).getPic());
		//ImageLoaderProxy.getInstance(mContext).displayImage(adFocusImgList.get(position).getPic(), holder.iconImg,R.drawable.mph_drug_default);
		return convertView;
	}

	private static class ViewHolder {
		TextView contentTxt;
		ImageView iconImg;
	}
	
	

}
