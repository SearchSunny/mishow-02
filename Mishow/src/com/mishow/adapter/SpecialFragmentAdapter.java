package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.bean.RedsBean;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：搜索专题列表Adapter
 */
public class SpecialFragmentAdapter extends BaseAdapter{

	private ArrayList<RedsBean> mRedsBeans;

	private Context mContext;
	
	public SpecialFragmentAdapter(Context mContext, ArrayList<RedsBean> redsBeans) {
		this.mContext = mContext;
		this.mRedsBeans = redsBeans;
	}
	@Override
	public int getCount() {
		
		return mRedsBeans == null ? 0 : mRedsBeans.size();
	}

	@Override
	public Object getItem(int position) {
		
		return mRedsBeans.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.fragment_search_special_item, null);
			holder = new ViewHolder();
			holder.view_line = (View)convertView.findViewById(R.id.view_line);
			convertView.setTag(holder);
			
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		
		//最后一个
		if (mRedsBeans != null) {
			
			if (position == mRedsBeans.size() -1) {
				
				holder.view_line.setVisibility(View.GONE);
			}else{
				
				holder.view_line.setVisibility(View.VISIBLE);
			}
		}
		
		
		return convertView;
	}

	
	private static class ViewHolder {
		TextView contentTxt;
		ImageView iconImg;
		View view_line;
		LinearLayout linear_person_mark;
	}
}
