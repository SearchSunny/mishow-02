package com.mishow.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.adapter.StarResultFragmentAdapter.OnCommnetInfoClick;
import com.mishow.bean.CommentChatDetail;
import com.mishow.utils.TextColorUtil;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：星秀详情评论adapter
 */
public class StarDetailsCommnetAdapter extends BaseAdapter{

	private Context mContext;
	private ArrayList<CommentChatDetail> commentVOs;
	
	private OnCommnetInfoClick onCInfoClick;
	
	
	public void setOnCommentInfoClick(OnCommnetInfoClick onClick){
		
		this.onCInfoClick = onClick;
		
	}
	
	public StarDetailsCommnetAdapter(Context context,ArrayList<CommentChatDetail> commentVOs ) {
		this.mContext = context;
		this.commentVOs = commentVOs;
		
	}
	@Override
	public int getCount() {
		return commentVOs == null ? 0 : commentVOs.size();
	}

	@Override
	public Object getItem(int position) {
		return commentVOs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder= null;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_star_details_comment, null);
			viewHolder = new ViewHolder();
			viewHolder.text_comment_content = (TextView)convertView.findViewById(R.id.text_comment_content);
			
			convertView.setTag(viewHolder);
		}else{
			
			viewHolder = (ViewHolder)convertView.getTag();
		}
		
		CommentChatDetail commentVO = commentVOs.get(position);
		String commentText = commentVO.getCommentText();
		Integer commentType = commentVO.getCommentType();
		//被回复者昵称
		final String commentRespondentCname = commentVO.getCommentRespondentCname();
		//评论者昵称
		String commentReviewersCname = commentVO.getCommentReviewersCname();
		if(commentType == 1){
			
			viewHolder.text_comment_content.setText(TextColorUtil.getMarkStr(mContext.getResources().getColor(R.color.color_location_txt_hint),"王杰 回复 吴万龙：挺好","王杰","吴万龙"));
			
		}else{
			
			viewHolder.text_comment_content.setText(TextColorUtil.getMarkStr(mContext.getResources().getColor(R.color.color_location_txt_hint), commentReviewersCname+"："+commentText, commentReviewersCname+"："));
		}
		viewHolder.text_comment_content.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onCInfoClick.setEditText(commentRespondentCname);
			}
		});
		return convertView;
	}
	
	
	
	private class ViewHolder {
		
		TextView text_comment_content;
	}

}
