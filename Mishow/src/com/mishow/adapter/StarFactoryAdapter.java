package com.mishow.adapter;

import java.util.ArrayList;

import com.mishow.R;
import com.mishow.adapter.StarFactoryRecyclerAdapter.OnItemClickListener;
import com.mishow.bean.RedsBean;
import com.mishow.log.MyLog;
import com.mishow.widget.HorizontalListView;
import com.mishow.widget.MyRecycleView;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：搜索机构列表Adapter
 */
public class StarFactoryAdapter extends BaseAdapter{

	private ArrayList<RedsBean> mRedsBeans;

	private Context mContext;
	
	StarFactoryRecyclerAdapter mRecyclerAdapter;
	
	public StarFactoryAdapter(Context mContext, ArrayList<RedsBean> redsBeans) {
		
		this.mContext = mContext;
		this.mRedsBeans = redsBeans;
	}
	@Override
	public int getCount() {
		
		return mRedsBeans == null ? 0 : mRedsBeans.size();
	}

	@Override
	public Object getItem(int position) {
		
		return mRedsBeans.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.activity_starfactory_item, null);
			holder = new ViewHolder();
			holder.id_recyclerview = (MyRecycleView)convertView.findViewById(R.id.id_recyclerview);
			convertView.setTag(holder);
			
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		
		mRecyclerAdapter = new StarFactoryRecyclerAdapter(mContext, mRedsBeans);
		LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
		layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);//支付横向、纵向
		//设置布局管理器 
		holder.id_recyclerview.setLayoutManager(layoutManager);
		holder.id_recyclerview.setAdapter(mRecyclerAdapter);
		
		mRecyclerAdapter.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onLongClick(int position) {
				
				MyLog.d("recycleronLongClick===="+position);
			}
			
			@Override
			public void onClick(int position) {
				
				MyLog.d("recyclerOnClick===="+position);
			}
		});
		
		
		return convertView;
	}

	
	private static class ViewHolder {
		TextView contentTxt;
		ImageView iconImg;
		LinearLayout linear_person_mark;
		RecyclerView id_recyclerview;
	}
}
