package com.mishow.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.adapter.StarFactoryRecyclerAdapter.MyViewHolder;
import com.mishow.bean.RedsBean;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：机构艺人列表Adapter
 */
public class StarFactoryRecyclerAdapter extends RecyclerView.Adapter<MyViewHolder>{

	private ArrayList<RedsBean> mRedsBeans;

	private Context mContext;
	
	public StarFactoryRecyclerAdapter(Context mContext, ArrayList<RedsBean> redsBeans) {
		this.mContext = mContext;
		this.mRedsBeans = redsBeans;
	}
	

	@Override
	public int getItemCount() {
		
		return mRedsBeans == null ? 0 : mRedsBeans.size();
	}

	//填充onCreateViewHolder方法返回的holder中的控件
	@Override
	public void onBindViewHolder(MyViewHolder holder, final int position) {
		
		//holder.tv_nikename.setText("ww");
		
		if( mOnItemClickListener!= null){  
            holder.iv_drug_pic.setOnClickListener( new OnClickListener() {  
                   
                  @Override  
                  public void onClick(View v) {  
                       mOnItemClickListener.onClick(position);  
                 }  
            });  
              
            holder.iv_drug_pic.setOnLongClickListener( new OnLongClickListener() {  
                  @Override  
                  public boolean onLongClick(View v) {  
                       mOnItemClickListener.onLongClick(position);  
                        return false;  
                 }  
            });
		}
		
	}

	//重写onCreateViewHolder方法，返回一个自定义的ViewHolder 
	//创建新View，被LayoutManager所调用
	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
		
		MyViewHolder holder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_organization_peoples_list, viewGroup,false));
		
        return holder;
	}
	
	public  class MyViewHolder extends ViewHolder{
		
		TextView tv_nikename;
		ImageView iv_drug_pic;
		LinearLayout linear_person_mark;
		
		public MyViewHolder(View view) {
			super(view);
			
			tv_nikename = (TextView) view.findViewById(R.id.tv_nikename);
			iv_drug_pic = (ImageView)view.findViewById(R.id.iv_drug_pic);
		}
		
	}
	
	private OnItemClickListener mOnItemClickListener;
	
	//给RecyclerView的Item添加点击事件
	public interface OnItemClickListener{
        void onClick( int position);
        void onLongClick( int position);
     }
	public void setOnItemClickListener(OnItemClickListener onItemClickListener ){
		
        this.mOnItemClickListener = onItemClickListener;
     }

}
