package com.mishow.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.adapter.StarResultFragmentNineRecyclerAdapter.MyViewHolder;
import com.mishow.bean.RedsBean;
import com.mishow.widget.nine.CustomImageView;
import com.mishow.widget.nine.Image;
import com.mishow.widget.nine.NineGridlayout;
import com.mishow.widget.nine.ScreenTools;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：九宫格列表Adapter
 */
public class StarResultFragmentNineRecyclerAdapter extends BaseAdapter{

	private ArrayList<Image> mDatalist;

	private Context mContext;
	
	public StarResultFragmentNineRecyclerAdapter(Context mContext, ArrayList<Image> dataList) {
		
		this.mContext = mContext;
		this.mDatalist = dataList;
	}
	
	@Override
	public int getCount() {
		
		return mDatalist == null ? 0: mDatalist.size();
	}


	@Override
	public Object getItem(int position) {
		
		return null;
	}


	@Override
	public long getItemId(int position) {
		
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MyViewHolder viewHolder = new MyViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_ninegridlayout, null);
			viewHolder.iv_oneimage = (CustomImageView)convertView.findViewById(R.id.iv_oneimage);
			viewHolder.iv_ngrid_layout = (NineGridlayout)convertView.findViewById(R.id.iv_ngrid_layout);
			convertView.setTag(viewHolder);
			
		} else {
			
			viewHolder = (MyViewHolder) convertView.getTag();
		}
		
		if (mDatalist.isEmpty()) {
			
            viewHolder.iv_oneimage.setVisibility(View.GONE);
            viewHolder.iv_ngrid_layout.setVisibility(View.GONE);
            
        } else if (mDatalist.size() == 1) {
        	
            viewHolder.iv_ngrid_layout.setVisibility(View.GONE);
            viewHolder.iv_oneimage.setVisibility(View.VISIBLE);
 
            handlerOneImage(viewHolder, mDatalist.get(0));
            
        } else { 
        	
            viewHolder.iv_ngrid_layout.setVisibility(View.VISIBLE);
            viewHolder.iv_oneimage.setVisibility(View.GONE);
 
            viewHolder.iv_ngrid_layout.setImagesData(mDatalist);
        } 
		
		return convertView;
	} 

	public class MyViewHolder{
		
		CustomImageView iv_oneimage;
		NineGridlayout iv_ngrid_layout;
		
	}
	
	private void handlerOneImage(MyViewHolder viewHolder, Image image) {
        int totalWidth;
        int imageWidth;
        int imageHeight;
        ScreenTools screentools = ScreenTools.instance(mContext);
        totalWidth = screentools.getScreenWidth() - screentools.dip2px(80);
        imageWidth = screentools.dip2px(image.getWidth());
        imageHeight = screentools.dip2px(image.getHeight());
        if (image.getWidth() <= image.getHeight()) {
            if (imageHeight > totalWidth) {
                imageHeight = totalWidth;
                imageWidth = (imageHeight * image.getWidth()) / image.getHeight();
            } 
        } else { 
            if (imageWidth > totalWidth) {
                imageWidth = totalWidth;
                imageHeight = (imageWidth * image.getHeight()) / image.getWidth();
            } 
        } 
        ViewGroup.LayoutParams layoutparams = viewHolder.iv_oneimage.getLayoutParams();
        layoutparams.height = imageHeight;
        layoutparams.width = imageWidth;
        viewHolder.iv_oneimage.setLayoutParams(layoutparams);
        viewHolder.iv_oneimage.setClickable(true);
        viewHolder.iv_oneimage.setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
        viewHolder.iv_oneimage.setImageUrl(image.getUrl());
 
    }
}
