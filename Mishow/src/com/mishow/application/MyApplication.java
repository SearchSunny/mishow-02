package com.mishow.application;


import android.app.Application;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

import cn.jpush.android.api.JPushInterface;

import com.mishow.BuildConfig;
import com.mishow.R;
import com.mishow.em.EMCustom;
import com.mishow.log.MyLog;
import com.mishow.utils.Constants;
import com.mishow.volley.http.RequestManager;
import com.umeng.analytics.MobclickAgent;
import com.umeng.analytics.MobclickAgent.EScenarioType;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareConfig;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：全局Application
 */
public class MyApplication extends Application {
	
	{
        /**
         * 配置第三方平台的appkey
         */
		PlatformConfig.setWeixin("wx967daebe835fbeac","5bb696d9ccd75a38c8a0bfe0675559b3");
		PlatformConfig.setSinaWeibo(Constants.WB_APPID,Constants.WB_APPSECRET,Constants.WB_URL);
		PlatformConfig.setQQZone("100424468","c7394704798a158208a74ab60104f0ba");
    }
	/** 返回首页 **/
	public static boolean toHomepage = false;
	/** 收到通知后播放的通知声音 **/
	private static MediaPlayer player = null;

	@Override
	public void onCreate() {
		super.onCreate();
		
		//三方登录时是否每次都要进行授权
		UMShareConfig config = new UMShareConfig();
		config.isNeedAuthOnGetUserInfo(true);
		//设置新浪网页授权还是SSO授权-调用此接口后，无论用户设备是否安装微博客户端，都只会拉起网页授权
		//config.setSinaAuthType(UMShareConfig.AUTH_TYPE_WEBVIEW);
		//当安装的时候进行SSO授权：
		config.setSinaAuthType(UMShareConfig.AUTH_TYPE_SSO);
		// 初始化友盟SDK
		UMShareAPI.get(this).setShareConfig(config);
		

		/** 崩溃日志 **/
		CrashHandler crashHandler = CrashHandler.getInstance();
		crashHandler.init(this);

		initNetwork();
		/**
		 * 根据apk环境判断是否显示日志信息
		 */
		if (BuildConfig.DEBUG == true) {
			MyLog.init(true);
		} else {
			MyLog.init(false);
		}
		// 友盟数据分析 dubug开关,测试时使用,正式时注释
		MobclickAgent.setDebugMode(true);

		// SDK在统计Fragment时，需要关闭Activity自带的页面统计，
		// 然后在每个页面中重新集成页面统计的代码(包括调用了 onResume 和 onPause 的Activity)。
		// 禁止页面自动统计方式
		MobclickAgent.openActivityDurationTrack(false);

		// 将统计数据发回友盟,6.0以后版本已没有此方法
		// MobclickAgent.updateOnlineConfig(this);

		// EScenarioType. E_UM_NORMAL　　普通统计场景类型
		// EScenarioType. E_UM_GAME 　　游戏场景类型
		// EScenarioType. E_UM_ANALYTICS_OEM 统计盒子场景类型
		// EScenarioType. E_UM_GAME_OEM 　 游戏盒子场景类型
		MobclickAgent.setScenarioType(this, EScenarioType.E_UM_NORMAL);


		/**----------- 极光推送配置 --------- **/
		//debug 为true则会打印debug级别的日志，false则只会打印warning级别以上的日志初始化推送服务 API
		JPushInterface.setDebugMode(true);//设置调试模式
		JPushInterface.init(this);
		
		/** 环信配置 **/
		EMCustom.getInstance().initEM(getApplicationContext());
		//在做打包混淆时，关闭debug模式，避免消耗不必要的资源
		EMCustom.getInstance().setEMDebugMode(true);
	}

	/**
	 * 初始化网络设置
	 */
	private void initNetwork() {
		RequestManager rm = RequestManager.getInstance();
		rm.setTipTimeout("连接超时");
		rm.setTipNoHostAddress("连接服务器失败");
		rm.setTipNoConnector(getString(R.string.no_connector));
		rm.setTipNetworkError("网络异常，请稍后再试");
		rm.setTipConnectServerException("连接服务器失败");
		rm.setTipParseError("数据解析失败");
		rm.init(this);
	}
	/**
	 * 设置接收消息
	 * @param context
	 */
	public static void ring(Context context) {
        // 添加震动效果
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // 震动时间
        vibrator.vibrate(new long[]{10, 200, 20, 300}, -1);

        // 判断当前系统当前的声音是否为静音
        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) == 0) {
            return;
        }
        // 获取通知声音文件的地址
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (player == null) {
            player = new MediaPlayer();
        }
        try {
            player.reset();
            player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            player.setDataSource(context, uri);
            player.prepareAsync();
            player.setOnPreparedListener(new OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            player.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                    player = null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	//解决64K方法数问题，APP继承了application，需要重写attachBaseContext(Context base)方法和继承MultiDexApplication
    /*@Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }*/
}
