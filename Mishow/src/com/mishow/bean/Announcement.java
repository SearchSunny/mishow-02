package com.mishow.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：通告列表显示数据
 */
public class Announcement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8750430728659307848L;
	
	/** 主键ID */
	private Integer id;

	/** 用户ID */
	private Integer userId;

	/** 通告主题 */
	private String announcementTheme;

	/** 通告截止时间 */
	private Date announcementEndtime;

	/** 通告省 */
	private String announcementProvince;

	/** 通告省编码 */
	private String announcementProvinceCode;

	/** 通告所在市 */
	private String announcementCity;

	/** 通告所在市编码 */
	private String announcementCityCode;

	/** 通告详细地址 */
	private String announcementAddress;

	/** 通告性别要求(0-保密,1-男,2-女) */
	private String announcementSex;

	/** 通告需求人数 */
	private Integer announcementNumber;

	/** 通告才艺类型 */
	private String announcementTalentType;

	/** 通告结款方式(0-现结2-面议) */
	private Integer announcementPaymentType;

	/** 通告报酬 */
	private BigDecimal announcementRemuneration;

	/** 通告报酬单位(0-元/人 1-元/人/天 2-元/人/小时) */
	private Integer announcementRemunerationUnit;

	/** 通告是否面试(0-否 1-是) */
	private Integer announcementIsInterview;

	/** 通告面试交通费 */
	private BigDecimal announcementTraffic;

	/** 通告简介 */
	private String announcementSynopsis;

	/** 通告状态(-1-不显示 0-未开始 1-进行中 2-已关闭) */
	private Integer announcementState;

	/** 创建时间 */
	private Date createTime;

	/** 创建者 */
	private String createCreator;

	/** 更新时间 */
	private Date updateTime;

	/** 更新人 */
	private String updateCreator;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAnnouncementTheme() {
		return announcementTheme;
	}

	public void setAnnouncementTheme(String announcementTheme) {
		this.announcementTheme = announcementTheme;
	}

	public Date getAnnouncementEndtime() {
		return announcementEndtime;
	}

	public void setAnnouncementEndtime(Date announcementEndtime) {
		this.announcementEndtime = announcementEndtime;
	}

	public String getAnnouncementProvince() {
		return announcementProvince;
	}

	public void setAnnouncementProvince(String announcementProvince) {
		this.announcementProvince = announcementProvince;
	}

	public String getAnnouncementProvinceCode() {
		return announcementProvinceCode;
	}

	public void setAnnouncementProvinceCode(String announcementProvinceCode) {
		this.announcementProvinceCode = announcementProvinceCode;
	}

	public String getAnnouncementCity() {
		return announcementCity;
	}

	public void setAnnouncementCity(String announcementCity) {
		this.announcementCity = announcementCity;
	}

	public String getAnnouncementCityCode() {
		return announcementCityCode;
	}

	public void setAnnouncementCityCode(String announcementCityCode) {
		this.announcementCityCode = announcementCityCode;
	}

	public String getAnnouncementAddress() {
		return announcementAddress;
	}

	public void setAnnouncementAddress(String announcementAddress) {
		this.announcementAddress = announcementAddress;
	}

	public String getAnnouncementSex() {
		return announcementSex;
	}

	public void setAnnouncementSex(String announcementSex) {
		this.announcementSex = announcementSex;
	}

	public Integer getAnnouncementNumber() {
		return announcementNumber;
	}

	public void setAnnouncementNumber(Integer announcementNumber) {
		this.announcementNumber = announcementNumber;
	}

	public String getAnnouncementTalentType() {
		return announcementTalentType;
	}

	public void setAnnouncementTalentType(String announcementTalentType) {
		this.announcementTalentType = announcementTalentType;
	}

	public Integer getAnnouncementPaymentType() {
		return announcementPaymentType;
	}

	public void setAnnouncementPaymentType(Integer announcementPaymentType) {
		this.announcementPaymentType = announcementPaymentType;
	}

	public BigDecimal getAnnouncementRemuneration() {
		return announcementRemuneration;
	}

	public void setAnnouncementRemuneration(BigDecimal announcementRemuneration) {
		this.announcementRemuneration = announcementRemuneration;
	}

	public Integer getAnnouncementRemunerationUnit() {
		return announcementRemunerationUnit;
	}

	public void setAnnouncementRemunerationUnit(Integer announcementRemunerationUnit) {
		this.announcementRemunerationUnit = announcementRemunerationUnit;
	}

	public Integer getAnnouncementIsInterview() {
		return announcementIsInterview;
	}

	public void setAnnouncementIsInterview(Integer announcementIsInterview) {
		this.announcementIsInterview = announcementIsInterview;
	}

	public BigDecimal getAnnouncementTraffic() {
		return announcementTraffic;
	}

	public void setAnnouncementTraffic(BigDecimal announcementTraffic) {
		this.announcementTraffic = announcementTraffic;
	}

	public String getAnnouncementSynopsis() {
		return announcementSynopsis;
	}

	public void setAnnouncementSynopsis(String announcementSynopsis) {
		this.announcementSynopsis = announcementSynopsis;
	}

	public Integer getAnnouncementState() {
		return announcementState;
	}

	public void setAnnouncementState(Integer announcementState) {
		this.announcementState = announcementState;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateCreator() {
		return createCreator;
	}

	public void setCreateCreator(String createCreator) {
		this.createCreator = createCreator;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateCreator() {
		return updateCreator;
	}

	public void setUpdateCreator(String updateCreator) {
		this.updateCreator = updateCreator;
	}
	
	
	
}
