package com.mishow.bean;

import java.io.Serializable;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：通告bean
 */
public class AnnunciateBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1872648786470694311L;
	
	/** 经纪人头像 **/
	private String managerImage;
	/** 是否实名 **/
	private boolean isReaname;
	/** 通告创建时间 **/
	private String createTime;
	
	/** 通告图片 **/
	private String image;
	/** 通告标题 **/
	private String title;
	/** 开始时间 **/
	private String starTime;
	/** 结束时间 **/
	private String endTime;
	/** 人数 **/
	private int person;
	/** 性别 **/
	private int sex;
	/** 身高 **/
	private int height;
	/** 地址 **/
	private String address;
	/** 价格 **/
	private double price;
	public String getManagerImage() {
		return managerImage;
	}
	public void setManagerImage(String managerImage) {
		this.managerImage = managerImage;
	}
	public boolean isReaname() {
		return isReaname;
	}
	public void setReaname(boolean isReaname) {
		this.isReaname = isReaname;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStarTime() {
		return starTime;
	}
	public void setStarTime(String starTime) {
		this.starTime = starTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getPerson() {
		return person;
	}
	public void setPerson(int person) {
		this.person = person;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
	
	
}
