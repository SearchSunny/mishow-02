package com.mishow.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：星秀实体类
 */
public class ChatVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2896879367723794430L;
	/** 当前用户ID**/
	private Integer currUserId;
	/** 用户ID**/
	private Integer id;
	/** 发布动态的用户ID**/
	private Integer userId;
	/** 用户昵称**/
	private String userHead;
	/** 用户昵称**/
	private String userCname;
	/** 用户职业类型**/
	private String userProfession;
	/** 是否关注**/
	private boolean concern;
	
	/** 圈子文本**/
	private String chatText;
	/** 圈子图片a */
	private String chatImgA;

	/** 圈子图片b */
	private String chatImgB;

	/** 圈子图片c */
	private String chatImgC;

	/** 圈子图片d */
	private String chatImgD;

	/** 圈子图片e */
	private String chatImgE;

	/** 圈子图片f */
	private String chatImgF;

	/** 圈子图片g */
	private String chatImgG;

	/** 圈子图片h */
	private String chatImgH;

	/** 圈子图片i */
	private String chatImgI;

	/** 圈子位置 */
	private String chatPosition;

	/** 圈子经度 */
	private String chatLongitude;

	/** 圈子纬度 */
	private String chatLatitude;

	/** 圈子状态(0-有效,1-已删除) */
	private Integer chatState;

	/** 圈子类型(0-图文,1-视频) */
	private Integer chatType;

	/** 圈子发布时间 */
	private String chatCreatetime;
	
	/** 是否点赞(0-没有，1-已经点过)**/
	private Integer isLike;
	/** 点赞数**/
	private Integer likedNum;
	/** 点赞头像**/
	private String likedHead;
	/** 评论数量**/
	private Integer commentNum;
	/** 评论内容**/
	private List<CommentVO> comments;
	/** 是否实名(0-未实名,1-已实名) **/
	private Integer realName;
	/**
	 * 九宫格图片
	 */
	private ArrayList<String> imageList;
	
	public Integer getCurrUserId() {
		return currUserId;
	}
	public void setCurrUserId(Integer currUserId) {
		this.currUserId = currUserId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserHead() {
		return userHead;
	}
	public void setUserHead(String userHead) {
		this.userHead = userHead;
	}
	public String getUserCname() {
		return userCname;
	}
	public void setUserCname(String userCname) {
		this.userCname = userCname;
	}
	public String getUserProfession() {
		return userProfession;
	}
	public void setUserProfession(String userProfession) {
		this.userProfession = userProfession;
	}
	public boolean isConcern() {
		return concern;
	}
	public void setConcern(boolean concern) {
		this.concern = concern;
	}
	public String getChatText() {
		return chatText;
	}
	public void setChatText(String chatText) {
		this.chatText = chatText;
	}
	public String getChatImgA() {
		return chatImgA;
	}
	public void setChatImgA(String chatImgA) {
		this.chatImgA = chatImgA;
	}
	public String getChatImgB() {
		return chatImgB;
	}
	public void setChatImgB(String chatImgB) {
		this.chatImgB = chatImgB;
	}
	public String getChatImgC() {
		return chatImgC;
	}
	public void setChatImgC(String chatImgC) {
		this.chatImgC = chatImgC;
	}
	public String getChatImgD() {
		return chatImgD;
	}
	public void setChatImgD(String chatImgD) {
		this.chatImgD = chatImgD;
	}
	public String getChatImgE() {
		return chatImgE;
	}
	public void setChatImgE(String chatImgE) {
		this.chatImgE = chatImgE;
	}
	public String getChatImgF() {
		return chatImgF;
	}
	public void setChatImgF(String chatImgF) {
		this.chatImgF = chatImgF;
	}
	public String getChatImgG() {
		return chatImgG;
	}
	public void setChatImgG(String chatImgG) {
		this.chatImgG = chatImgG;
	}
	public String getChatImgH() {
		return chatImgH;
	}
	public void setChatImgH(String chatImgH) {
		this.chatImgH = chatImgH;
	}
	public String getChatImgI() {
		return chatImgI;
	}
	public void setChatImgI(String chatImgI) {
		this.chatImgI = chatImgI;
	}
	public String getChatPosition() {
		return chatPosition;
	}
	public void setChatPosition(String chatPosition) {
		this.chatPosition = chatPosition;
	}
	public String getChatLongitude() {
		return chatLongitude;
	}
	public void setChatLongitude(String chatLongitude) {
		this.chatLongitude = chatLongitude;
	}
	public String getChatLatitude() {
		return chatLatitude;
	}
	public void setChatLatitude(String chatLatitude) {
		this.chatLatitude = chatLatitude;
	}
	public Integer getChatState() {
		return chatState;
	}
	public void setChatState(Integer chatState) {
		this.chatState = chatState;
	}
	public Integer getChatType() {
		return chatType;
	}
	public void setChatType(Integer chatType) {
		this.chatType = chatType;
	}
	public String getChatCreatetime() {
		return chatCreatetime;
	}
	public void setChatCreatetime(String chatCreatetime) {
		this.chatCreatetime = chatCreatetime;
	}
	public Integer getIsLike() {
		return isLike;
	}
	public void setIsLike(Integer isLike) {
		this.isLike = isLike;
	}
	public Integer getLikedNum() {
		return likedNum;
	}
	public void setLikedNum(Integer likedNum) {
		this.likedNum = likedNum;
	}
	public String getLikedHead() {
		return likedHead;
	}
	public void setLikedHead(String likedHead) {
		this.likedHead = likedHead;
	}
	public Integer getCommentNum() {
		return commentNum;
	}
	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}
	public List<CommentVO> getComments() {
		return comments;
	}
	public void setComments(List<CommentVO> comments) {
		this.comments = comments;
	}
	public Integer getRealName() {
		return realName;
	}
	public void setRealName(Integer realName) {
		this.realName = realName;
	}
	public ArrayList<String> getImageList() {
		return imageList;
	}
	public void setImageList(ArrayList<String> imageList) {
		this.imageList = imageList;
	}
	
	
}
