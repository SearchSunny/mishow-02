package com.mishow.bean;

import java.io.Serializable;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：星秀详情评论分页实体类
 */
public class CommentChatDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5045484014613378344L;
	/** 评论内容 **/
	private String commentText;
	/** 评论者ID **/
	private String commentReviewersId;
	/** 评论者昵称 **/
	private String commentReviewersCname;
	/** 评论类型(0-主题评论,1-回复) **/
	private Integer commentType;
	/** 评论状态(0-有效,1-已删除) **/
	private Integer commentState;
	/** 被回复者ID */
	private Integer commentRespondentId;
	/** 被回复者昵称 */
	private String commentRespondentCname;
	
	/** 是否实名(0-未实名,1-已实名) **/
	private Integer realName;
	/**评论者头像**/
	private String commentReviewersHead;
	/**评论时间**/
	private String commentCreatetime;
	/** 评论者职业类型**/
	private String commentReviewersProfession;
	
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getCommentReviewersId() {
		return commentReviewersId;
	}
	public void setCommentReviewersId(String commentReviewersId) {
		this.commentReviewersId = commentReviewersId;
	}
	public String getCommentReviewersCname() {
		return commentReviewersCname;
	}
	public void setCommentReviewersCname(String commentReviewersCname) {
		this.commentReviewersCname = commentReviewersCname;
	}
	public Integer getCommentType() {
		return commentType;
	}
	public void setCommentType(Integer commentType) {
		this.commentType = commentType;
	}
	public Integer getCommentState() {
		return commentState;
	}
	public void setCommentState(Integer commentState) {
		this.commentState = commentState;
	}
	public Integer getCommentRespondentId() {
		return commentRespondentId;
	}
	public void setCommentRespondentId(Integer commentRespondentId) {
		this.commentRespondentId = commentRespondentId;
	}
	public String getCommentRespondentCname() {
		return commentRespondentCname;
	}
	public void setCommentRespondentCname(String commentRespondentCname) {
		this.commentRespondentCname = commentRespondentCname;
	}
	public Integer getRealName() {
		return realName;
	}
	public void setRealName(Integer realName) {
		this.realName = realName;
	}
	public String getCommentReviewersHead() {
		return commentReviewersHead;
	}
	public void setCommentReviewersHead(String commentReviewersHead) {
		this.commentReviewersHead = commentReviewersHead;
	}
	public String getCommentCreatetime() {
		return commentCreatetime;
	}
	public void setCommentCreatetime(String commentCreatetime) {
		this.commentCreatetime = commentCreatetime;
	}
	public String getCommentReviewersProfession() {
		return commentReviewersProfession;
	}
	public void setCommentReviewersProfession(String commentReviewersProfession) {
		this.commentReviewersProfession = commentReviewersProfession;
	}
}
