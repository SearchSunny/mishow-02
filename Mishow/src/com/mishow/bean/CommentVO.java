package com.mishow.bean;

import java.io.Serializable;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：星秀评论内容
 */
public class CommentVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6567165336142639382L;
	
	/** 评论内容 **/
	private String commentText;
	/** 评论者ID **/
	private String commentReviewersId;
	/** 评论者昵称 **/
	private String commentReviewersCname;
	/** 评论类型(0-主题评论,1-回复) **/
	private Integer commentType;
	/** 评论状态(0-有效,1-已删除) **/
	private Integer commentState;
	/** 被回复者ID */
	private Integer commentRespondentId;
	/** 被回复者昵称 */
	private String commentRespondentCname;
	
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getCommentReviewersId() {
		return commentReviewersId;
	}
	public void setCommentReviewersId(String commentReviewersId) {
		this.commentReviewersId = commentReviewersId;
	}
	public String getCommentReviewersCname() {
		return commentReviewersCname;
	}
	public void setCommentReviewersCname(String commentReviewersCname) {
		this.commentReviewersCname = commentReviewersCname;
	}
	public Integer getCommentType() {
		return commentType;
	}
	public void setCommentType(Integer commentType) {
		this.commentType = commentType;
	}
	public Integer getCommentState() {
		return commentState;
	}
	public void setCommentState(Integer commentState) {
		this.commentState = commentState;
	}
	public Integer getCommentRespondentId() {
		return commentRespondentId;
	}
	public void setCommentRespondentId(Integer commentRespondentId) {
		this.commentRespondentId = commentRespondentId;
	}
	public String getCommentRespondentCname() {
		return commentRespondentCname;
	}
	public void setCommentRespondentCname(String commentRespondentCname) {
		this.commentRespondentCname = commentRespondentCname;
	}
	
	
}
