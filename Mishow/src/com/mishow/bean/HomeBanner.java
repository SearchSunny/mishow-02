package com.mishow.bean;

import java.io.Serializable;

/**
 * 作者：wei.miao <br/>
 * 描述：首页banner图片
 */
public class HomeBanner extends BaseBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8855659516454947010L;
	/**
	 * 活动id
	 */
	private Integer id;
	/**
	 * 标题
	 */
	private String bannerTitle;
	/**
	 * 图片位置
	 */
	private String bannerPosition;
	/**
	 * 是否显示原生标题
	 */
	private String bannerTitleDisplay;
	/**
	 * 图片url链接
	 */
	private String bannerUrl;
	/**
	 * 活动跳转路径
	 */
	private String bannerLink;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBannerTitle() {
		return bannerTitle;
	}
	public void setBannerTitle(String bannerTitle) {
		this.bannerTitle = bannerTitle;
	}
	public String getBannerPosition() {
		return bannerPosition;
	}
	public void setBannerPosition(String bannerPosition) {
		this.bannerPosition = bannerPosition;
	}
	public String getBannerTitleDisplay() {
		return bannerTitleDisplay;
	}
	public void setBannerTitleDisplay(String bannerTitleDisplay) {
		this.bannerTitleDisplay = bannerTitleDisplay;
	}
	public String getBannerUrl() {
		return bannerUrl;
	}
	public void setBannerUrl(String bannerUrl) {
		this.bannerUrl = bannerUrl;
	}
	public String getBannerLink() {
		return bannerLink;
	}
	public void setBannerLink(String bannerLink) {
		this.bannerLink = bannerLink;
	}
	
    
}
