package com.mishow.bean;

import android.graphics.drawable.Drawable;

/**
 * 作者：wei.miao <br/>
 * 描述：首页快捷功能(暂时不用)
 */
public class HomeShortcutMenu extends BaseBean{
	/**
	 * 图片地址
	 */
	private int pic;
	/**
	 * 标题
	 */
	private String title;
	
	
	public int getPic() {
		return pic;
	}
	public void setPic(int pic) {
		this.pic = pic;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
