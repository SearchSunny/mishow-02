package com.mishow.bean;

import java.io.Serializable;
/**
 * 作者：wei.miao <br/>
 * 描述：首页星秀展示
 */
public class HomeStarShow extends BaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 713849015300872690L;
	/**用户ID**/
	private Integer userId;
	/**用户昵称**/
	private String userCname;
	/**用户头像(圆形)**/
	private String userHead;
	/**职业类型**/
	private String userProfession;
	/**个人中心顶部图片**/
	private String userTopimg;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserCname() {
		return userCname;
	}
	public void setUserCname(String userCname) {
		this.userCname = userCname;
	}
	public String getUserHead() {
		return userHead;
	}
	public void setUserHead(String userHead) {
		this.userHead = userHead;
	}
	public String getUserProfession() {
		return userProfession;
	}
	public void setUserProfession(String userProfession) {
		this.userProfession = userProfession;
	}
	public String getUserTopimg() {
		return userTopimg;
	}
	public void setUserTopimg(String userTopimg) {
		this.userTopimg = userTopimg;
	}
	
}
