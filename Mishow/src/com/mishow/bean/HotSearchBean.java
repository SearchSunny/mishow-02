package com.mishow.bean;

import java.io.Serializable;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：热门搜索及历史搜索
 */
public class HotSearchBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7847836579290360507L;
	/** 热词标识码 **/
	private int adPgCode;
	/** 参数 **/
	private String adPageParam;
	/** 搜索热词 **/
	private String ghwName;

	public String getGhwName() {
		return ghwName;
	}

	public void setGhwName(String ghwName) {
		this.ghwName = ghwName;
	}

	public int getAdPgCode() {
		return adPgCode;
	}

	public void setAdPgCode(int adPgCode) {
		this.adPgCode = adPgCode;
	}

	public String getAdPageParam() {
		return adPageParam;
	}

	public void setAdPageParam(String adPageParam) {
		this.adPageParam = adPageParam;
	}


}
