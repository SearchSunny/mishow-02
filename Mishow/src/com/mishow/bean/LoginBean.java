package com.mishow.bean;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：登录接口JavaBean
 */
public class LoginBean extends BaseBean {
	
    private Body body;

    public static class Body extends BaseBean.Body {

        private Result result;

        public static class Result {
            private String userTel;// 用户手机号码
            private String userId;// 用户ID
            private String userName;// 用户名称
            private String userPic;
            // private String userPwd;
            private int isBindWechat;// 是否绑定微信
            private String easemobId;

            public String getUserTel() {
                return userTel;
            }

            public void setUserTel(String userTel) {
                this.userTel = userTel;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getUserPic() {
                return userPic;
            }

            public void setUserPic(String userPic) {
                this.userPic = userPic;
            }

            // public String getUserPwd() {
            // return userPwd;
            // }
            // public void setUserPwd(String userPwd) {
            // this.userPwd = userPwd;
            // }

            public String getEasemobId() {
                return easemobId;
            }

            public void setEasemobId(String easemobId) {
                this.easemobId = easemobId;
            }

            public int getIsBindWechat() {
                return isBindWechat;
            }

            public void setIsBindWechat(int isBindWechat) {
                this.isBindWechat = isBindWechat;
            }
        }

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }

    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }


}
