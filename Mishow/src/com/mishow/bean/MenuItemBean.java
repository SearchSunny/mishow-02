package com.mishow.bean;

import java.io.Serializable;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：菜单列表内容
 */
public class MenuItemBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8494093694777343137L;
	private int icon;
	private String name;
	
	public MenuItemBean(String name) {
		super();
		this.name = name;
	}
	public MenuItemBean(int icon, String name) {
		super();
		this.icon = icon;
		this.name = name;
	}
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
