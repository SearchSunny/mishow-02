package com.mishow.bean;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableMap implements Parcelable {

	public HashMap<Integer, ArrayList<String>> map = new HashMap<>();
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {

		out.writeMap(map);
	}

	public static final Parcelable.Creator<ParcelableMap> CREATOR = new Creator<ParcelableMap>() {

		@SuppressWarnings("unchecked")
		@Override
		public ParcelableMap createFromParcel(Parcel source) {
			ParcelableMap p =  new ParcelableMap();  
            p.map = source.readHashMap(HashMap.class.getClassLoader()); 
            return  p; 
		}

		@Override
		public ParcelableMap[] newArray(int size) {
			return new ParcelableMap[size];
		}
	};


}
