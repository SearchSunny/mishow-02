package com.mishow.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PhotoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8919556170798729377L;

	private Integer id;

	/** 用户ID */
	private Integer userId;
	/** 相册UUID **/
	private String photouuid;
	/** 相册名称 */
	private String photoName;

	/** 类型(0-公开,1-私人) */
	private Integer photoType;

	/** 照片状态(0-显示,1-删除) */
	private Integer photoState;

	/** 相册创建时间 */
	private Date photoCreatetime;
	/** 上传图片数组字符 **/
	private String imageStr;
	/** 上传图片数据 **/
	private ArrayList<String> imageList;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getPhotouuid() {
		return photouuid;
	}
	public void setPhotouuid(String photouuid) {
		this.photouuid = photouuid;
	}
	public String getPhotoName() {
		return photoName;
	}
	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}
	public Integer getPhotoType() {
		return photoType;
	}
	public void setPhotoType(Integer photoType) {
		this.photoType = photoType;
	}
	public Integer getPhotoState() {
		return photoState;
	}
	public void setPhotoState(Integer photoState) {
		this.photoState = photoState;
	}
	public Date getPhotoCreatetime() {
		return photoCreatetime;
	}
	public void setPhotoCreatetime(Date photoCreatetime) {
		this.photoCreatetime = photoCreatetime;
	}
	public String getImageStr() {
		return imageStr;
	}
	public void setImageStr(String imageStr) {
		this.imageStr = imageStr;
	}
	public ArrayList<String> getImageList() {
		return imageList;
	}
	public void setImageList(ArrayList<String> imageList) {
		this.imageList = imageList;
	}
	
}
