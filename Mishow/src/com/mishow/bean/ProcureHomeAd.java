package com.mishow.bean;

import java.io.Serializable;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：
 */
public class ProcureHomeAd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9206212436243179760L;
	/** banner图片地址 **/
	private String aImgPath;
	/** H5页面标题 **/
	private String aTitle;
	
	
	public String getaImgPath() {
		return aImgPath;
	}
	public void setaImgPath(String aImgPath) {
		this.aImgPath = aImgPath;
	}
	public String getaTitle() {
		return aTitle;
	}
	public void setaTitle(String aTitle) {
		this.aTitle = aTitle;
	}
	
	
}
