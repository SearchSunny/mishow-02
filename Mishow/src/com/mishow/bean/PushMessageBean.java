package com.mishow.bean;
/**
 * 作者：wei.miao <br/>
 * 描述：消息推送JavaBean
 */
public class PushMessageBean {
	/** **/
    private CustomContent custom_content;
    /**  **/
    private String description;
    /**  **/
    private String notification_basic_style;
    /** **/
    private String notification_builder_id;
    /** **/
    private String open_type;
    /**  **/
    private String title;
    /** **/
    private String user_confirm;

    public CustomContent getCustom_content() {
        return custom_content;
    }

    public void setCustom_content(CustomContent custom_content) {
        this.custom_content = custom_content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotification_basic_style() {
        return notification_basic_style;
    }

    public void setNotification_basic_style(String notification_basic_style) {
        this.notification_basic_style = notification_basic_style;
    }

    public String getNotification_builder_id() {
        return notification_builder_id;
    }

    public void setNotification_builder_id(String notification_builder_id) {
        this.notification_builder_id = notification_builder_id;
    }

    public String getOpen_type() {
        return open_type;
    }

    public void setOpen_type(String open_type) {
        this.open_type = open_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser_confirm() {
        return user_confirm;
    }

    public void setUser_confirm(String user_confirm) {
        this.user_confirm = user_confirm;
    }

    /**
     * 自定义消息内容
     */
    public static class CustomContent {
        /**
         * 推送内容
         **/
        private String pushContent;
        /** 推送类型 **/
        private int pushType = -1;
        /** 推送到指定用户userId **/
        private String pushToUserId;

        public String getPushContent() {
            return pushContent;
        }

        public void setPushContent(String pushContent) {
            this.pushContent = pushContent;
        }

        public String getPushToUserId() {
            return pushToUserId;
        }

        public void setPushToUserId(String pushToUserId) {
            this.pushToUserId = pushToUserId;
        }

        public int getPushType() {
            return pushType;
        }

        public void setPushType(int pushType) {
            this.pushType = pushType;
        }

    }
}
