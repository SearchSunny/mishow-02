package com.mishow.bean;

import java.io.Serializable;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：红人bean
 */
public class RedsBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5209326556937730868L;
	
	/** 红人 **/
	private int redId;
	/** 红人昵称 **/
	private String nikeName;
	/** 红人头像图片 **/
	private String image;
	/** 红人是否实名 **/
	private boolean isReaname;
	/** 红人职业类型 **/
	private String professionType;
	/** 红人粉丝数 **/
	private int hotNum;
	/** 红人是否有精选 **/
	private boolean isSelection;
	
	
	public int getRedId() {
		return redId;
	}
	public void setRedId(int redId) {
		this.redId = redId;
	}
	public String getNikeName() {
		return nikeName;
	}
	public void setNikeName(String nikeName) {
		this.nikeName = nikeName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public boolean isReaname() {
		return isReaname;
	}
	public void setReaname(boolean isReaname) {
		this.isReaname = isReaname;
	}
	public String getProfessionType() {
		return professionType;
	}
	public void setProfessionType(String professionType) {
		this.professionType = professionType;
	}
	public int getHotNum() {
		return hotNum;
	}
	public void setHotNum(int hotNum) {
		this.hotNum = hotNum;
	}
	public boolean isSelection() {
		return isSelection;
	}
	public void setSelection(boolean isSelection) {
		this.isSelection = isSelection;
	}
	

}
