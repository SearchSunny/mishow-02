package com.mishow.bean;
/**
 * 作者：wei.miao<br/>
 * 描述：二维码类型
 */
public class ScanCodeBean {

	private int codeType;// 二维码类型：用户邀请码 - 101;店员邀请码 - 102;药店二维码 - 103;订单提货码 -
    // 104
    private String code;// 二维码

    public ScanCodeBean(int codeType, String code) {
        super();
        this.codeType = codeType;
        this.code = code;
    }

    public int getCodeType() {
        return codeType;
    }

    public void setCodeType(int codeType) {
        this.codeType = codeType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
