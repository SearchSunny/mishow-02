package com.mishow.bean;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：验证码返回结果
 */
public class SmsBean extends BaseBean {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends BaseBean.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}

		public static class Result {
			private String isNewUser;// 是否是新用户

		}
	}

}
