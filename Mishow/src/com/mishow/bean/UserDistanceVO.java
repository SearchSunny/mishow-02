package com.mishow.bean;

import java.io.Serializable;
/**
 * 作者：wei.miao <br/>
 * 描述：搜索附近的人
 */
public class UserDistanceVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8268818929753229872L;
	
	/** 用户ID **/
	private Integer userId;
	/** 用户昵称 **/
	private String userCname;
	/** 用户头像 **/
	private String userHead;
	/** 经度 **/
	private String userLongitude;
	/** 纬度 **/
	private String userLatitude;
	/** 最后记录坐标时间 **/
	private String userLocaltime;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserCname() {
		return userCname;
	}
	public void setUserCname(String userCname) {
		this.userCname = userCname;
	}
	public String getUserHead() {
		return userHead;
	}
	public void setUserHead(String userHead) {
		this.userHead = userHead;
	}
	public String getUserLongitude() {
		return userLongitude;
	}
	public void setUserLongitude(String userLongitude) {
		this.userLongitude = userLongitude;
	}
	public String getUserLatitude() {
		return userLatitude;
	}
	public void setUserLatitude(String userLatitude) {
		this.userLatitude = userLatitude;
	}
	public String getUserLocaltime() {
		return userLocaltime;
	}
	public void setUserLocaltime(String userLocaltime) {
		this.userLocaltime = userLocaltime;
	}
	

}
