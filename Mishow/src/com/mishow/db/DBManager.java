package com.mishow.db;
 
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream; 

import com.mishow.R;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：读取本地省份数据
 */
public class DBManager {
    private final int BUFFER_SIZE = 1024;
    //private String DB_NAME = "city_cn.s3db";
    private String DB_NAME = "addr.db";
    public static final String PACKAGE_NAME = "com.mishow";
    
    private String DB_PATH;
    //public static final String DB_PATH = "/data"+ Environment.getDataDirectory().getAbsolutePath() + "/"+ PACKAGE_NAME;
    
    private SQLiteDatabase database;
    private Context context;
    private File file=null;
    
    private boolean openOtherDataBase;
    
    DBManager(Context context,boolean otherDbName) {
        this.context = context;
        this.openOtherDataBase = otherDbName;
        if (openOtherDataBase) {
			
        	this.DB_NAME = "addr.db";
        	
		}else{
			
			this.DB_NAME = "city_cn.s3db";
		}
        DB_PATH = "/data" + Environment.getDataDirectory().getAbsolutePath() + "/" + context.getPackageName();
    }
 
    public void openDatabase() {
    	
    	if (openOtherDataBase) {
			
    		this.database = this.openAddrDatabase(DB_PATH + "/" + DB_NAME);
		}else{
			
			this.database = this.openDatabase(DB_PATH + "/" + DB_NAME);
		}
    	
        
    }
    public SQLiteDatabase getDatabase(){
    	
    	return this.database;
    }
 
    private SQLiteDatabase openDatabase(String dbfile) {
        try {
        	file = new File(dbfile);
            if (!file.exists()) {
            	InputStream is = context.getResources().openRawResource(R.raw.city);
            	if(is!=null){
            	}else{
            	}
            	FileOutputStream fos = new FileOutputStream(dbfile);
            	if(is!=null){
            	}else{
            	}
                byte[] buffer = new byte[BUFFER_SIZE];
                int count = 0;
                while ((count =is.read(buffer)) > 0) {
                    fos.write(buffer, 0, count);
                	fos.flush();
                }
                fos.close();
                is.close();
            }
            database = SQLiteDatabase.openOrCreateDatabase(dbfile,null);
            return database;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
        }
        return null;
    }
    
    
    private SQLiteDatabase openAddrDatabase(String dbfile) {
        try {
        	file = new File(dbfile);
            if (!file.exists()) {
            	InputStream is = context.getResources().openRawResource(R.raw.addr);
            	if(is!=null){
            	}else{
            	}
            	FileOutputStream fos = new FileOutputStream(dbfile);
            	if(is!=null){
            	}else{
            	}
                byte[] buffer = new byte[BUFFER_SIZE];
                int count = 0;
                while ((count =is.read(buffer)) > 0) {
                    fos.write(buffer, 0, count);
                	fos.flush();
                }
                fos.close();
                is.close();
            }
            database = SQLiteDatabase.openOrCreateDatabase(dbfile,null);
            return database;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
        }
        return null;
    }
    
    public void closeDatabase() {
    	if(this.database!=null)
    		this.database.close();
    }
}