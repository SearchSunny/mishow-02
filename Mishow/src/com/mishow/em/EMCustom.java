package com.mishow.em;

import android.content.Context;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.easeui.controller.EaseUI;
import com.hyphenate.exceptions.HyphenateException;
import com.hyphenate.util.NetUtils;
import com.mishow.log.MyLog;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：环信IM相关的处理
 */
public class EMCustom {
	/**
	 * 单例持有器
	 */
	private static final class InstanceHolder {
		private static final EMCustom INSTANCE = new EMCustom();
	}

	/**
	 * 禁止构造
	 */
	private EMCustom() {

	}

	/**
	 * 获得单例
	 * 
	 * @return
	 */
	public static EMCustom getInstance() {
		return InstanceHolder.INSTANCE;
	}

	/**
	 * 初始化EM环信配置
	 * 
	 * @param context
	 */
	public void initEM(Context context) {
		// 详情参见http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1chat_1_1_e_m_options.html
		EMOptions options = new EMOptions();
		// 默认添加好友时，是不需要验证的，改成需要验证(默认true)
		options.setAcceptInvitationAlways(false);
		// 设置自动登录,SDK中自动登录属性默认是 true打开的
		// 首次登录成功后，不需要再次调用登录方法，在下次 APP 启动时，SDK 会自动为您登录
		options.setAutoLogin(true);
		// 设置(主动或被动)退出群组时，是否删除群聊聊天记录,true为删除群组相关消息
		options.setDeleteMessagesAsExitGroup(false);
		// 设置是否自动接受加群邀请
		options.setAutoAcceptGroupInvitation(true);

		EaseUI.getInstance().init(context, null);
	}

	/**
	 * 在做打包混淆时，关闭debug模式，避免消耗不必要的资源
	 * 
	 * @param debug
	 */
	public void setEMDebugMode(boolean debug) {
		EMClient.getInstance().setDebugMode(debug);
	}

	/**
	 * 注册环信
	 * 注册模式分两种，开放注册和授权注册。只有开放注册时，才可以客户端注册
	 * 开放注册是为了测试使用，正式环境中不推荐使用该方式注册环信账号
	 * 授权注册的流程应该是您服务器通过环信提供的 REST API 注册，之后保存到您的服务器或返回给客户端
	 * @param username 用户名
	 * @param pwd 密码
	 * @throws 注册失败会抛出HyphenateException
	 */
	public void EMCreateAccount(final String username, final String pwd){
		//注册方法，同步，需要自己异步执行，根据执行情况判断是否注册成功
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					// 同步方法
					EMClient.getInstance().createAccount(username, pwd);
					MyLog.d("注册环信成功");
				} catch (HyphenateException e) {
					e.printStackTrace();
					MyLog.d("注册环信失败="+e.getErrorCode()+"=="+e.getMessage());
				}
			}
		}).start();
		
	}

	/**
	 * 登录环信
	 * 
	 * @param userName 用户名
	 * @param password 密码
	 */
	public void EMLogin(String userName, String password) {
		//登录方法，异步，可以回调中监听登录状态	
		EMClient.getInstance().login(userName, password, new EMCallBack() {// 回调
					@Override
					public void onSuccess() {
						// 注意：
						// 登录成功后需要调用EMClient.getInstance().chatManager().loadAllConversations();
						// 和EMClient.getInstance().groupManager().loadAllGroups();。
						// 这两个方法是为了保证进入主页面后本地会话和群组都 load 完毕。
						EMClient.getInstance().groupManager().loadAllGroups();
						EMClient.getInstance().chatManager()
								.loadAllConversations();
						MyLog.d("登录环信服务器成功！");
					}

					@Override
					public void onProgress(int progress, String status) {

					}

					@Override
					public void onError(int code, String message) {
						MyLog.d("登录环信服务器失败！");
					}
				});
	}

	/**
	 * 退出环信登录
	 * 
	 * @param isLogout
	 *            app处理时可以弹出提示框让用户选择，是否继续退出
	 */
	public void EMLogout(boolean isLogout) {
		//退出登录方法，第一个参数表示是否解绑推送的token,没有使用推送或者被踢都要传false
		// 同步方法
		// EMClient.getInstance().logout(isLogout);
		// 异步方法
		EMClient.getInstance().logout(isLogout, new EMCallBack() {

			@Override
			public void onSuccess() {
				MyLog.d("退出环信登录成功！");
			}

			@Override
			public void onProgress(int progress, String status) {

			}

			@Override
			public void onError(int code, String message) {
				MyLog.d("退出环信登录失败！" + code);
			}
		});
	}

	/**
	 * 注册连接状态监听
	 * 当掉线时，Android SDK 会自动重连，无需进行任何操作，通过注册连接监听来知道连接状态。
	 * 在聊天过程中难免会遇到网络问题，在此 SDK 为您提供了网络监听接口，实时监听可以根据 disconnect 返回的 error 判断原因。
	 * 若服务器返回的参数值为EMError.USER_LOGIN_ANOTHER_DEVICE，则认为是有同一个账号异地登录；
	 * 若服务器返回的参数值为EMError.USER_REMOVED，则是账号在后台被删除。
	 */
	public void EMAddConnection(Context context) {

		// 注册一个监听连接状态的listener
		EMClient.getInstance().addConnectionListener(
				new MyConnectionListener(context));

	}

	/**
	 * 描述：实现ConnectionListener接口
	 */
	private class MyConnectionListener implements EMConnectionListener {

		private Context mContext;

		public MyConnectionListener(Context context) {
			this.mContext = context;
		}

		@Override
		public void onConnected() {

		}

		@Override
		public void onDisconnected(final int error) {
			new Runnable() {
				@Override
				public void run() {
					if (error == EMError.USER_REMOVED) {
						// 显示帐号已经被移除
					} else if (error == EMError.USER_LOGIN_ANOTHER_DEVICE) {
						// 显示帐号在其他设备登录
					} else {
						if (NetUtils.hasNetwork(mContext)) {
							// 连接不到聊天服务器
						} else {
							// 当前网络不可用，请检查网络设置
						}
					}
				}
			};
		}
	}
}
