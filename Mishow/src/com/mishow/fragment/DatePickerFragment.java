package com.mishow.fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.mishow.widget.MyDatePickerDialog;

/**
 * 作者：wei.miao <br/>
 * 描述：日期控件
 */

public class DatePickerFragment extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {

    public static final String EXTRA_DATE = "BirthdayStr";
    
    public static final String EXTRA_IS_ASTRICE = "IsAstrict";
    private static DialogCallBackListener mDialogCallBackListener;
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    private MyDatePickerDialog myDatePickerDialog;
    private Date mDate;
    private String mdateStr;
    
    private boolean isAstrict;

    /**
     * 
     * @param dialogCallBackListener
     * @param date
     * @param isAstrict 日期是否有限制
     * @return
     */
    public static DatePickerFragment newInstance(DialogCallBackListener dialogCallBackListener, String date,boolean isAstrict) {

        mDialogCallBackListener = dialogCallBackListener;
        Bundle args = new Bundle();
        try {

            args.putSerializable(EXTRA_DATE, df.parse(date));
            args.putBoolean(EXTRA_IS_ASTRICE, isAstrict);
        } catch (ParseException e) {

            e.printStackTrace();
        }

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("NewApi")
	@NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mDate = (Date) getArguments().getSerializable(EXTRA_DATE);
        isAstrict = getArguments().getBoolean(EXTRA_IS_ASTRICE);
        
        final Calendar c = Calendar.getInstance();
        c.setTime(mDate);

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        myDatePickerDialog = new MyDatePickerDialog(getActivity(), this, year, month, day);
        DatePicker datePicker = myDatePickerDialog.getDatePicker();
        if (isAstrict) {
			
        	try {
                datePicker.setMinDate(df.parse("1916-01-01").getTime());
            } catch (ParseException e) {

                e.printStackTrace();
            }
            datePicker.setMaxDate(Calendar.getInstance().getTime().getTime());
		}
        
        myDatePickerDialog.setInverseBackgroundForced(true);
        return myDatePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        //返回的moth需要加1
        int selectMonth = month + 1;
        mdateStr = year + "-" + selectMonth + "-" + dayOfMonth;
        if (isAstrict) {
			
        	//获取当前日期
            Calendar calendarReality = Calendar.getInstance();
            int year_reality = calendarReality.get(Calendar.YEAR);
            int month_reality = calendarReality.get(Calendar.MONTH);
            int day_reality = calendarReality.get(Calendar.DAY_OF_MONTH);
            //选择的日期与当前日期相比 相等
            if (year == year_reality) {
                //小于当前月份
                if (selectMonth < month_reality + 1) {

                    mDialogCallBackListener.callBack(mdateStr);

                }//月份相等
                else if (selectMonth == month_reality + 1) {

                    if (dayOfMonth > day_reality) {

                        return;

                    } else {

                        mDialogCallBackListener.callBack(mdateStr);
                    }
                } else {

                    return;
                }

            }//小于当前年份或者等于1916
            else if (year < year_reality && year >= 1916) {

                mDialogCallBackListener.callBack(mdateStr);

            } else {

                return;
            }
		}else{
			
			mDialogCallBackListener.callBack(mdateStr);
		}
    }

    //通过该接口回调Dialog需要传递的值
    public interface DialogCallBackListener {
        //具体方法
        void callBack(String msg);
    }
}
