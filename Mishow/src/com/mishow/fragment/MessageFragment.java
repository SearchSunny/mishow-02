package com.mishow.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.activity.MessageAttentionActivity;
import com.mishow.activity.MessageLikeActivity;
import com.mishow.activity.MessageNotifiActivity;
import com.mishow.fragment.base.BaseFragment;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：消息Fragment
 */
public class MessageFragment extends BaseFragment {
	/** Fragment布局 **/
	private View view;
	/** activity对象 **/
	private Activity activity;
	
	
	
	private Button btn_back;
	private TextView title_tv;
	/** 通知 **/
	private RelativeLayout relative_message_notifi;
	/** 点赞 **/
	private RelativeLayout relative_message_like;
	/** 关注 **/
	private RelativeLayout relative_message_attention;
	
	public static MessageFragment newInstance() {
		
		MessageFragment fragment = new MessageFragment();
		return fragment;
	}
	
	public static MessageFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		MessageFragment fragment = new MessageFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onAttach(Activity activity) {
		this.activity = activity;
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		//cancelRequest("GET_CART_NUM");
	}
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
		if (view == null) {
			view = inflater.inflate(R.layout.fragment_message, container, false);
			initView(view);
		} else {
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null) {
				parent.removeView(view);
			}

			// 防止切换时，刷新动画不消失的BUG
			/*if (spLayout != null && spLayout.isRefreshing()) {
				spLayout.setRefreshing(false);
			}*/
		}
		setAttribute();
		return view;
	}
	
	/**
	 * 初始化View
	 * @param view
	 */
	private void initView(View view){
		
		btn_back =  (Button)view.findViewById(R.id.btn_back);
		title_tv = (TextView)view.findViewById(R.id.title_tv);
		
		relative_message_notifi = (RelativeLayout)view.findViewById(R.id.relative_message_notifi);
		relative_message_like = (RelativeLayout)view.findViewById(R.id.relative_message_like);
		relative_message_attention = (RelativeLayout)view.findViewById(R.id.relative_message_attention);
		
	}
	
	/**
	 * 设置控件属性
	 */
	private void setAttribute(){
		btn_back.setVisibility(View.INVISIBLE);
		title_tv.setText("消息");
		relative_message_notifi.setOnClickListener(onClickListener);
		relative_message_like.setOnClickListener(onClickListener);
		relative_message_attention.setOnClickListener(onClickListener);
		
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.relative_message_notifi:
				MessageNotifiActivity.intoActivity(activity);
				break;
			case R.id.relative_message_like:
				MessageLikeActivity.intoActivity(activity);
				break;
			case R.id.relative_message_attention:
				MessageAttentionActivity.intoActivity(activity);
				break;

			default:
				break;
			}
			
		}
	};
	
	/**
     * 刷新消息界面
     */
    public void refresh() {
    	//请求服务端
    }
}
