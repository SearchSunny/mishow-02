package com.mishow.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.mishow.R;
import com.mishow.activity.AuthorActivity;
import com.mishow.activity.LabelVOTypeActivity;
import com.mishow.activity.MyApplyActivity;
import com.mishow.activity.MyReleaseActivity;
import com.mishow.activity.PersonDataActivity;
import com.mishow.activity.PersonalMainActivity;
import com.mishow.activity.SettingActivity;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.net.HttpUrlConstant;
import com.mishow.net.NetworkUtil;
import com.mishow.preferenses.SharedPreferencesUtil;
import com.mishow.request.listener.OnResponseListener;
import com.mishow.request.response.StatureResponse;
import com.mishow.request.result.Stature;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.ToastUtil;
import com.mishow.volley.http.request.FastJsonRequest;
import com.mishow.widget.dialog.MyCardCodeDialog;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：我的Fragment
 */
public class MyFragment extends BaseFragment {
	/** Fragment布局 **/
	private View view;
	/** activity对象 **/
	private Activity activity;
	
	private Button btn_back;
	private TextView title_tv;
	
	/**
	 * 个人主页
	 */
	private RelativeLayout relative_person_main;
	/**
	 * 实名认证
	 */
	private RelativeLayout relative_authorize;
	
	/** 我的名片 **/
	private RelativeLayout relative_my_cardcode;
	/** 设置 **/
	private RelativeLayout relative_setting;
	/** 个人资料 **/
	private RelativeLayout relative_person_data;
	/** 丽质标签 **/
	private RelativeLayout relative_verylabel_data;
	
	/** 我发布的 **/
	private RelativeLayout relative_my_release;
	
	/** 我报名的 **/
	private RelativeLayout relative_my_apply;
	
	private SharedPreferencesUtil mShPreferencesUtil;
	
	/** 丽质资料显示标签 **/
	ArrayList<String> mVeryLabelTags = new ArrayList<String>();
	
	public static MyFragment newInstance() {
		
		MyFragment fragment = new MyFragment();
		return fragment;
	}
	
	public static MyFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		MyFragment fragment = new MyFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onAttach(Activity activity) {
		this.activity = activity;
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mShPreferencesUtil = new SharedPreferencesUtil(activity);
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
		if (view == null) {
			view = inflater.inflate(R.layout.fragment_my, container, false);
			initView(view);
		} else {
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null) {
				parent.removeView(view);
			}

			// 防止切换时，刷新动画不消失的BUG
			/*if (spLayout != null && spLayout.isRefreshing()) {
				spLayout.setRefreshing(false);
			}*/
		}
		setAttribute();
		return view;
	}
	
	/**
	 * 初始化View
	 * @param view
	 */
	private void initView(View view){
		btn_back =  (Button)view.findViewById(R.id.btn_back);
		title_tv = (TextView)view.findViewById(R.id.title_tv);
		
		relative_person_main = (RelativeLayout)view.findViewById(R.id.relative_person_main);
		relative_setting = (RelativeLayout)view.findViewById(R.id.relative_setting);
		relative_verylabel_data = (RelativeLayout)view.findViewById(R.id.relative_verylabel_data);
		relative_person_data = (RelativeLayout)view.findViewById(R.id.relative_person_data);
		
		relative_authorize = (RelativeLayout)view.findViewById(R.id.relative_authorize);
		
		relative_my_cardcode = (RelativeLayout)view.findViewById(R.id.relative_my_cardcode);
		
		relative_my_release = (RelativeLayout)view.findViewById(R.id.relative_my_release);
		relative_my_apply = (RelativeLayout)view.findViewById(R.id.relative_my_apply);
		
	}
	
	/**
	 * 设置控件属性
	 */
	private void setAttribute(){
		btn_back.setVisibility(View.INVISIBLE);
		title_tv.setText("我的");
		
		relative_person_main.setOnClickListener(onClickListener);
		relative_setting.setOnClickListener(onClickListener);
		relative_verylabel_data.setOnClickListener(onClickListener);
		relative_person_data.setOnClickListener(onClickListener);
		
		relative_my_release.setOnClickListener(onClickListener);
		relative_my_apply.setOnClickListener(onClickListener);
		relative_authorize.setOnClickListener(onClickListener);
		
		relative_my_cardcode.setOnClickListener(onClickListener);
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.relative_person_main:
				PersonalMainActivity.intoActivity(activity);
				break;
			case R.id.relative_setting:
				SettingActivity.intoActivity(activity);
				break;
			case R.id.relative_verylabel_data:
				getUserVeryLabel();
				break;
			case R.id.relative_person_data:
				PersonDataActivity.intoActivity(activity);
				break;
			case R.id.relative_my_release:
				MyReleaseActivity.intoActivity(activity);
				break;
			case R.id.relative_my_apply:
				MyApplyActivity.intoActivity(activity);
				break;
			case R.id.relative_authorize:
				AuthorActivity.intoActivity(activity);
				break;
			case R.id.relative_my_cardcode:
				 MyCardCodeDialog dialog = new MyCardCodeDialog(activity,"139821798172984719");
	             dialog.show();
				break;
			default:
				break;
			}
			
		}
	};
	
	/** 获取用户丽质资料 **/
	private void getUserVeryLabel(){

		if (!AndroidUtil.isNetworkAvailable(getActivity())) {
			ToastUtil.show(getActivity(),
					getResources().getString(R.string.no_connector));
			return;
		}
		showProgress("", "", true);
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", mShPreferencesUtil.getUserId());

		Map<String, String> lastParams = NetworkUtil
				.generaterPostRequestParams(getActivity(), params);

		String postUrl = HttpUrlConstant.getPostUrl(getActivity(),
				HttpUrlConstant.GET_USERY_STATURE);
		OnResponseListener<StatureResponse> listener = new OnResponseListener<StatureResponse>(getActivity()) {

			@Override
			public void onSuccess(StatureResponse response) {
				
				fillData(response.getBody().getResult());
				
			}

			@Override
			public void onError(String code, String message) {

				ToastUtil.show(getActivity(), message);
			}

			@Override
			public void onCompleted() {

				dismissProgress();
			}
		};
		executeRequest(new FastJsonRequest<StatureResponse>(
				Request.Method.POST, postUrl, StatureResponse.class, listener,
				listener).setParams(lastParams));
		
	}
	
	private void fillData(Stature mStature){
		if (mStature != null) {
			
			if (mStature.getStatureLabel() != null && !mStature.getStatureLabel().equals("")) {
				mVeryLabelTags.clear();
				String[] labels = mStature.getStatureLabel().split(",");
				for (int i = 0; i < labels.length; i++) {
					mVeryLabelTags.add(labels[i]);
				}
			}
			LabelVOTypeActivity.intoActivityForResult(getActivity(), "verylabel_type",mVeryLabelTags, "丽质资料",0);
		}
	}
}
