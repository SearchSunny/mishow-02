package com.mishow.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.mishow.R;
import com.mishow.activity.AnnunciateDetailActivity;
import com.mishow.adapter.AnnunciateFragmentAdapter;
import com.mishow.bean.Announcement;
import com.mishow.bean.AnnunciateBean;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜索通告结果
 */
public class SearchAnnunciateFragment extends BaseFragment{

	private Activity mActivity;
	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_annunciate;
	/** 数据为空 **/
	private EmptyDataLayout ll_search_annunciate_empty;
	/** 通告列表 **/
	private MyListView lv_search_annunciate;
	/** 搜索通告列表Adapter **/
	private AnnunciateFragmentAdapter annunciateFragmentAdapter;
	/** 通告列表 **/
	private ArrayList<Announcement> annunciateBeans;
	
	public static SearchAnnunciateFragment newInstance() {
		
		SearchAnnunciateFragment fragment = new SearchAnnunciateFragment();
		return fragment;
	}
	
	public static SearchAnnunciateFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		SearchAnnunciateFragment fragment = new SearchAnnunciateFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	@Deprecated
	public void onAttach(Activity activity) {
		
		this.mActivity = activity;
		super.onAttach(activity);
		
	}
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override  
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){  
        super.onCreateView(inflater, container, savedInstanceState);  
        View mView = inflater.inflate(R.layout.fragment_search_annunciate, container,false);
        
		initView(mView);
        initData();
        setAttribute();
        return mView;      
    }  
    @Override  
    public void onActivityCreated(Bundle savedInstanceState){  
        super.onActivityCreated(savedInstanceState);  
    }  
    /**
     * 初始化view
     * @param view
     */
    private void initView(View view){
    	
    	refreshLayout_annunciate = (PullPushRefreshLayout)view.findViewById(R.id.refreshLayout_annunciate);
    	ll_search_annunciate_empty = (EmptyDataLayout)view.findViewById(R.id.ll_search_annunciate_empty);
    	lv_search_annunciate = (MyListView)view.findViewById(R.id.lv_search_annunciate);
    }
    
    private void initData(){
    	
    	annunciateBeans = new ArrayList<Announcement>();
		
		for (int i = 0; i < 10; i++) {
			
			Announcement bean = new Announcement();
			annunciateBeans.add(bean);
		}
		
		annunciateFragmentAdapter = new AnnunciateFragmentAdapter(mActivity, annunciateBeans);
		lv_search_annunciate.setAdapter(annunciateFragmentAdapter);
    }
    /**
     * 设置属性
     */
    private void setAttribute(){
    	
    	lv_search_annunciate.setOnItemClickListener(onItemClickListener);
    }
    
    /**
	 * 通告列表点击
	 */
	private OnItemClickListener onItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			int announcementId = annunciateBeans.get(position).getId();
			AnnunciateDetailActivity.intoActivity(getActivity(),announcementId+"");
		}
	};
}
