package com.mishow.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishow.R;
import com.mishow.adapter.NearbyRedsFragmentAdapter;
import com.mishow.adapter.RedsFragmentAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜附近红人
 */
public class SearchNearbyRedsFragment extends BaseFragment{
	

	private Activity mActivity;
	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_reds;
	/** 数据为空 **/
	private EmptyDataLayout ll_search_reds_empty;
	/** 红人列表 **/
	private MyListView lv_search_reds;
	/** 搜索红人列表Adapter **/
	private NearbyRedsFragmentAdapter mRedsFragmentAdapter;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static SearchNearbyRedsFragment newInstance() {
		
		SearchNearbyRedsFragment fragment = new SearchNearbyRedsFragment();
		return fragment;
	}
	
	public static SearchNearbyRedsFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		SearchNearbyRedsFragment fragment = new SearchNearbyRedsFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onAttach(Activity activity) {
		
		this.mActivity = activity;
		super.onAttach(activity);
		
	}
	
	@Override  
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){  
        super.onCreateView(inflater, container, savedInstanceState);  
        View mView = inflater.inflate(R.layout.fragment_search_nearby_reds, container,false); 
        
        initView(mView);
        initData();
        setAttribute();
        
        return mView;      
    }  
    @Override  
    public void onActivityCreated(Bundle savedInstanceState){  
        super.onActivityCreated(savedInstanceState);  
    } 
    
    /**
     * 初始化view
     * @param view
     */
    private void initView(View view){
    	
    	refreshLayout_reds = (PullPushRefreshLayout)view.findViewById(R.id.refreshLayout_reds);
    	ll_search_reds_empty = (EmptyDataLayout)view.findViewById(R.id.ll_search_reds_empty);
    	lv_search_reds = (MyListView)view.findViewById(R.id.lv_search_reds);
    }
    
    
    private void initData(){
    	
    	mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 15; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mRedsFragmentAdapter = new NearbyRedsFragmentAdapter(mActivity, mRedsBeans);
		lv_search_reds.setAdapter(mRedsFragmentAdapter);
    }
    
    private void setAttribute(){
    	
    	
    }


}
