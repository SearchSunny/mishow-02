package com.mishow.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.mishow.R;
import com.mishow.activity.NearbyShowDetailActivity;
import com.mishow.adapter.NearbyShowFragmentAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜表演
 */
public class SearchNearbyShowFragment extends BaseFragment{
	

	private Activity mActivity;
	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_reds;
	/** 数据为空 **/
	private EmptyDataLayout ll_search_reds_empty;
	/** 附近表演列表 **/
	private MyListView lv_search_reds;
	/** 搜附近表演列表Adapter **/
	private NearbyShowFragmentAdapter mNearbyShowFragmentAdapter;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static SearchNearbyShowFragment newInstance() {
		
		SearchNearbyShowFragment fragment = new SearchNearbyShowFragment();
		return fragment;
	}
	
	public static SearchNearbyShowFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		SearchNearbyShowFragment fragment = new SearchNearbyShowFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onAttach(Activity activity) {
		
		this.mActivity = activity;
		super.onAttach(activity);
		
	}
	
	@Override  
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){  
        super.onCreateView(inflater, container, savedInstanceState);  
        View mView = inflater.inflate(R.layout.fragment_search_nearby_show, container,false); 
        
        initView(mView);
        initData();
        setAttribute();
        
        return mView;      
    }  
    @Override  
    public void onActivityCreated(Bundle savedInstanceState){  
        super.onActivityCreated(savedInstanceState);  
    } 
    
    /**
     * 初始化view
     * @param view
     */
    private void initView(View view){
    	
    	refreshLayout_reds = (PullPushRefreshLayout)view.findViewById(R.id.refreshLayout_reds);
    	ll_search_reds_empty = (EmptyDataLayout)view.findViewById(R.id.ll_search_reds_empty);
    	lv_search_reds = (MyListView)view.findViewById(R.id.lv_search_reds);
    }
    
    
    private void initData(){
    	
    	mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 3; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mNearbyShowFragmentAdapter = new NearbyShowFragmentAdapter(mActivity, mRedsBeans);
		lv_search_reds.setAdapter(mNearbyShowFragmentAdapter);
    }
    
    private void setAttribute(){
    	
    	lv_search_reds.setOnItemClickListener(onItemClickListener);
    }

    
    private OnItemClickListener onItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			
			
			NearbyShowDetailActivity.intoActivity(mActivity);
		}
    	
    	
    	
	};

}
