package com.mishow.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishow.R;
import com.mishow.adapter.OrganizationFragmentAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜索机构结果
 */
public class SearchOrganizationFragment extends BaseFragment{

	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_organization;
	/** 数据为空 **/
	private EmptyDataLayout ll_search_organization_empty;
	/** 机构列表 **/
	private MyListView lv_search_organization;
	/** 搜索机构列表Adapter **/
	private OrganizationFragmentAdapter mOrganizationFragmentAdapter ;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static SearchOrganizationFragment newInstance() {
		
		SearchOrganizationFragment fragment = new SearchOrganizationFragment();
		return fragment;
	}
	
	public static SearchOrganizationFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		SearchOrganizationFragment fragment = new SearchOrganizationFragment();
		fragment.setArguments(args);
		return fragment;
	}
	@Override  
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){  
        super.onCreateView(inflater, container, savedInstanceState);  
        View mView = inflater.inflate(R.layout.fragment_search_organization, container,false); 
        
        initView(mView);
        initData();
        setAttribute();
        
        return mView;      
    }  
    @Override  
    public void onActivityCreated(Bundle savedInstanceState){  
        super.onActivityCreated(savedInstanceState);  
    } 
	
    /**
     * 初始化view
     * @param view
     */
    private void initView(View view){
    	
    	refreshLayout_organization = (PullPushRefreshLayout)view.findViewById(R.id.refreshLayout_organization);
    	ll_search_organization_empty = (EmptyDataLayout)view.findViewById(R.id.ll_search_organization_empty);
    	lv_search_organization = (MyListView)view.findViewById(R.id.lv_search_organization);
    }
    
    
    private void initData(){
    	
    	mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 10; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mOrganizationFragmentAdapter = new OrganizationFragmentAdapter(getActivity(), mRedsBeans);
		lv_search_organization.setAdapter(mOrganizationFragmentAdapter);
    }
    
    private void setAttribute(){
    	
    	
    }
}
