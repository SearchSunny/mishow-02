package com.mishow.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishow.R;
import com.mishow.adapter.RedsFragmentAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：搜索红人结果
 */
public class SearchRedsFragment extends BaseFragment{

	private Activity mActivity;
	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_reds;
	/** 数据为空 **/
	private EmptyDataLayout ll_search_reds_empty;
	/** 通告列表 **/
	private MyListView lv_search_reds;
	/** 搜索红人列表Adapter **/
	private RedsFragmentAdapter mRedsFragmentAdapter;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static SearchRedsFragment newInstance() {
		
		SearchRedsFragment fragment = new SearchRedsFragment();
		return fragment;
	}
	
	public static SearchRedsFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		SearchRedsFragment fragment = new SearchRedsFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onAttach(Activity activity) {
		
		this.mActivity = activity;
		super.onAttach(activity);
		
	}
	
	@Override  
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){  
        super.onCreateView(inflater, container, savedInstanceState);  
        View mView = inflater.inflate(R.layout.fragment_search_reds, container,false); 
        
        initView(mView);
        initData();
        setAttribute();
        
        return mView;      
    }  
    @Override  
    public void onActivityCreated(Bundle savedInstanceState){  
        super.onActivityCreated(savedInstanceState);  
    } 
    
    /**
     * 初始化view
     * @param view
     */
    private void initView(View view){
    	
    	refreshLayout_reds = (PullPushRefreshLayout)view.findViewById(R.id.refreshLayout_reds);
    	ll_search_reds_empty = (EmptyDataLayout)view.findViewById(R.id.ll_search_reds_empty);
    	lv_search_reds = (MyListView)view.findViewById(R.id.lv_search_reds);
    }
    
    
    private void initData(){
    	
    	mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 15; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mRedsFragmentAdapter = new RedsFragmentAdapter(mActivity, mRedsBeans);
		lv_search_reds.setAdapter(mRedsFragmentAdapter);
    }
    
    private void setAttribute(){
    	
    	
    }
}
