package com.mishow.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishow.R;
import com.mishow.adapter.SpecialFragmentAdapter;
import com.mishow.bean.RedsBean;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.refresh.PullPushRefreshLayout;
import com.mishow.widget.EmptyDataLayout;
import com.mishow.widget.MyListView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜索专题结果
 */
public class SearchSpecialFragment extends BaseFragment{

	/** 下拉刷新 **/
	private PullPushRefreshLayout refreshLayout_special;
	/** 数据为空 **/
	private EmptyDataLayout ll_search_special_empty;
	/** 专题列表 **/
	private MyListView lv_search_special;
	
	/** 搜索专题列表Adapter **/
	private SpecialFragmentAdapter mSpecialFragmentAdapter;
	
	private ArrayList<RedsBean> mRedsBeans;
	
	public static SearchSpecialFragment newInstance() {
		
		SearchSpecialFragment fragment = new SearchSpecialFragment();
		return fragment;
	}
	
	public static SearchSpecialFragment newInstance(int status) {
		
		Bundle args = new Bundle();
		args.putInt("", status);
		SearchSpecialFragment fragment = new SearchSpecialFragment();
		fragment.setArguments(args);
		return fragment;
	}
	@Override  
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){  
        super.onCreateView(inflater, container, savedInstanceState);  
        View mView = inflater.inflate(R.layout.fragment_search_special, container,false);  
        
        initView(mView);
        initData();
        setAttribute();
        
        return mView;      
    }  
    @Override  
    public void onActivityCreated(Bundle savedInstanceState){  
        super.onActivityCreated(savedInstanceState);  
    }  
    
    /**
     * 初始化view
     * @param view
     */
    private void initView(View view){
    	
    	refreshLayout_special = (PullPushRefreshLayout)view.findViewById(R.id.refreshLayout_special);
    	ll_search_special_empty = (EmptyDataLayout)view.findViewById(R.id.ll_search_special_empty);
    	lv_search_special = (MyListView)view.findViewById(R.id.lv_search_special);
    }
    
    private void initData(){
    	
    	mRedsBeans = new ArrayList<RedsBean>();
		
		for (int i = 0; i < 20; i++) {
			
			RedsBean bean = new RedsBean();
			mRedsBeans.add(bean);
		}
		
		mSpecialFragmentAdapter = new SpecialFragmentAdapter(getActivity(), mRedsBeans);
		lv_search_special.setAdapter(mSpecialFragmentAdapter);
    }
    
    private void setAttribute(){
    	
    	
    }
}
