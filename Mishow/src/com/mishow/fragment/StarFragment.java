package com.mishow.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.fragment.base.BaseFragment;
import com.mishow.log.MyLog;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：星秀Fragment
 */
public class StarFragment extends BaseFragment{

	/**
	 * 关注
	 */
	private TextView tv_star_attention;
	/**
	 * 最热
	 */
	private TextView tv_star_hot;
	/**
	 * 最新
	 */
	private TextView tv_star_news;

	private FrameLayout content;
	

	/** fm对象 **/
	private FragmentManager fragmentManager;


	private StarResultFragment mStartResultFragment;


	public static StarFragment newInstance() {

		StarFragment fragment = new StarFragment();
		return fragment;
	}

	public static StarFragment newInstance(int status) {

		Bundle args = new Bundle();
		args.putInt("", status);
		StarFragment fragment = new StarFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MyLog.d("onCreateFragment====");
		fragmentManager = getChildFragmentManager();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		View mView = inflater.inflate(R.layout.fragment_star, container, false);
		initView(mView);
		setAttribute();
		return mView;
	}

	/**
	 * 初始化View
	 * 
	 * @param view
	 */
	private void initView(View view) {

		tv_star_attention = (TextView) view.findViewById(R.id.tv_star_attention);
		tv_star_hot = (TextView) view.findViewById(R.id.tv_star_hot);
		tv_star_news = (TextView) view.findViewById(R.id.tv_star_news);
		content = (FrameLayout)view.findViewById(R.id.content);

	}

	/**
	 * 设置控件属性
	 */
	private void setAttribute() {

		tv_star_attention.setOnClickListener(new MyOnClickListener(0));
		tv_star_hot.setOnClickListener(new MyOnClickListener(1));
		tv_star_news.setOnClickListener(new MyOnClickListener(2));
		
		showOrderByStatus(0);
	}

	private class MyOnClickListener implements OnClickListener {

		private int index = 0;

		public MyOnClickListener(int i) {

			index = i;
		}

		@Override
		public void onClick(View v) {
			
			showOrderByStatus(index);
		}

	}
	
	
	private void showOrderByStatus(int status) {
		
		showTabColors(status);
		
		mStartResultFragment = StarResultFragment.newInstance(status);
		
		showFragment(mStartResultFragment);
	}

	/**
	 * 显示fragment列表
	 */
	private void showFragment(StarResultFragment fragment) {
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content, fragment);
		transaction.commit();
	}
	/**
	 * tab字体颜色显示
	 */
	private void showTabColors(int index) {

		if (index == 0) {
			tv_star_attention.setSelected(true);
			tv_star_attention.setTextColor(getResources().getColor(R.color.color_tab_select));
		} else {
			tv_star_attention.setSelected(false);
			tv_star_attention.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if (index == 1) {
			tv_star_hot.setSelected(true);
			tv_star_hot.setTextColor(getResources().getColor(R.color.color_tab_select));
		} else {
			tv_star_hot.setSelected(false);
			tv_star_hot.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
		if (index == 2) {
			tv_star_news.setSelected(true);
			tv_star_news.setTextColor(getResources().getColor(R.color.color_tab_select));
		} else {
			tv_star_news.setSelected(false);
			tv_star_news.setTextColor(getResources().getColor(R.color.color_tab_noselect));
		}
	}

}
