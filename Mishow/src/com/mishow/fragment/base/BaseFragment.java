package com.mishow.fragment.base;

import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.net.NetworkUtil;
import com.mishow.volley.http.request.RequestExtension;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：Fragment基类 统一处理Fragment中的网络请求部分
 */
public class BaseFragment extends Fragment{



	private Dialog mProgressDialog;// 加载进度 对话框
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}

	/**
	 * 执行网络请求
	 * 
	 * @param request
	 */
	protected void executeRequest(RequestExtension<?> request) {
		executeRequest(request, this);
	}

	/**
	 * 执行带标志的网络请求
	 * 
	 * @param request
	 * @param tag
	 */
	protected void executeRequest(RequestExtension<?> request, Object tag) {
		
		NetworkUtil.executeRequest(getActivity(), request, tag);
	}

	/**
	 * 取消指定标志的网络请求
	 * 
	 * @param tag
	 */
	protected void cancelRequest(Object tag) {
		
		NetworkUtil.cancelRequest(tag);
	}
	
	/**
	 * 显示等待框
	 * 
	 * @param title
	 * @param message
	 */
	public void showProgress(String title, String message, boolean isCancel) {
		if (!isAdded()) {
			return;
		}
		
		try {
			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				return;
			}
			mProgressDialog = new Dialog(getActivity(), R.style.CustomDialog);
			mProgressDialog.setContentView(R.layout.dialog_progress);
			mProgressDialog.setCancelable(isCancel);
			TextView textView = (TextView) mProgressDialog
					.findViewById(R.id.progress_msg);
			if (TextUtils.isEmpty(message)) {
				textView.setVisibility(View.GONE);
			} else {
				textView.setVisibility(View.VISIBLE);
				textView.setText(message);
			}
			mProgressDialog.show();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		}
	}

	/**
	 * 取消等待框
	 */
	public void dismissProgress() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			try {
				mProgressDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Fragment返回事件 <br/>
	 * @param [参数1]-[参数1说明] <br/>
	 * @param [参数2]-[参数2说明] <br/>
	 */
	public boolean onBackPress(){return false;};
	
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		//cancelRequest("TAG_MSG_COUNT");
	}

}
