package com.mishow.inteface;

import com.mishow.bean.Area;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：通用级联菜单接口
 */
public interface CascadingMenuViewOnSelectListener {
	public void getValue(Area area);
}
