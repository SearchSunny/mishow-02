package com.mishow.inteface;

import java.util.ArrayList;
/**
 * 作者：wei.miao <br/>
 * 描述：选择标签回调接口
 */
public interface LabelSelectRefreshInteface {

	/**
	 * 选择标签回调
	 * @param selectTags 已选择的标签
	 */
	void refreshSelectedCount(ArrayList<String> selectTags);
}
