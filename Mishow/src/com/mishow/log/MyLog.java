package com.mishow.log;

import android.util.Log;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：自定义Log
 */
public class MyLog {

	public static final String TAG = MyLog.class.getSimpleName();
    /**
     * 控制变量，是否显示log日志
     */
    public static boolean isShowLog = false;
    public static String defaultMsg = "";
    public static final int V = 1;
    public static final int D = 2;
    public static final int I = 3;
    public static final int W = 4;
    public static final int E = 5;

    /**
     *  初始化控制变量
     * @param isShowLog
     */
    public static void init(boolean isShowLog) {
    	MyLog.isShowLog = isShowLog;
    }

    /**
     * 初始化控制变量和默认日志
     * @param isShowLog
     * @param defaultMsg
     */
    public static void init(boolean isShowLog, String defaultMsg) {
    	MyLog.isShowLog = isShowLog;
    	MyLog.defaultMsg = defaultMsg;
    }

    public static void v() {
    	logInfo(V, null, defaultMsg);
    }

    public static void v(Object obj) {
    	logInfo(V, null, obj);
    }

    public static void v(String tag, Object obj) {
    	logInfo(V, tag, obj);
    }

    public static void d() {
    	logInfo(D, null, defaultMsg);
    }

    public static void d(Object obj) {
    	logInfo(D, null, obj);
    }

    public static void d(String tag, Object obj) {
    	logInfo(D, tag, obj);
    }

    public static void i() {
    	logInfo(I, null, defaultMsg);
    }

    public static void i(Object obj) {
    	logInfo(I, null, obj);
    }

    public static void i(String tag, String obj) {
    	logInfo(I, tag, obj);
    }

    public static void w() {
    	logInfo(W, null, defaultMsg);
    }

    public static void w(Object obj) {
    	logInfo(W, null, obj);
    }

    public static void w(String tag, Object obj) {
    	logInfo(W, tag, obj);
    }

    public static void e() {
    	logInfo(E, null, defaultMsg);
    }

    public static void e(Object obj) {
    	logInfo(E, null, obj);
    }

    public static void e(String tag, Object obj) {
    	logInfo(E, tag, obj);
    }



    /**
     * 执行打印方法
     * @param type
     * @param tagStr
     * @param obj
     */
    public static void logInfo(int type, String tagStr, Object obj) {
        String msg;
        if (!isShowLog) {
            return;
        }
        //获取堆栈信息
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();

        int index = 4;
        //类名
        String className = stackTrace[index].getFileName();
        //方法名
        String methodName = stackTrace[index].getMethodName();
        //代码行数
        int lineNumber = stackTrace[index].getLineNumber();

        String tag = (tagStr == null ? className : tagStr);
        methodName = methodName.substring(0, 1).toUpperCase() + methodName.substring(1);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ (").append(className).append(":").append(lineNumber).append(")#").append(methodName).append(" ] ");

        if (obj == null) {
            msg = "Log with null Object";
        } else {
            msg = obj.toString();
        }
        if (msg != null) {
            stringBuilder.append(msg);
        }

        String logStr = stringBuilder.toString();

        switch (type) {
            case V:
                Log.v(tag, logStr);
                break;
            case D:
                Log.d(tag, logStr);
                break;
            case I:
                Log.i(tag, logStr);
                break;
            case W:
                Log.w(tag, logStr);
                break;
            case E:
                Log.e(tag, logStr);
                break;
        }
    }
}
