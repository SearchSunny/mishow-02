package com.mishow.net;

import java.util.Map;

import com.mishow.log.MyLog;

import android.content.Context;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：所有请求服务器url
 */
public class HttpUrlConstant {
	
	/** 请求接口ip地址 **/
	 private final static String SERVER_IP = "weibotie.com";
	/** 登录url **/
	public static final String URL_LOGING = "/user/login";
	/**  获取短信验证码 **/
    public static final String GET_VERIFYCODE_STRING = "/ditchsms/sendSms";
    /** 获取首页数据 **/
    public static final String INDEX_PAGE_DATA = "/common/getIndexContent";
    /** 获取个人资料 **/
    public static final String USER_INFO = "/user/getUserInfo";
    /** 更新个人资料 **/
    public static final String UPDATE_USER_INFO = "/user/update";
    /** 获取用户丽质资料 **/
    public static final String GET_USERY_STATURE = "/stature/getUserStature";
   
    /** 更新用户丽质资料 **/
    public static final String UPDATE_USER_STATURE="/stature/updateUserStature";
    
    /** 获取所有丽质标签 **/
    public static final String GET_VERY_LABEL = "/label/getLabel";
    
    /** 获取通告才艺类型 **/
    public static final String GET_QUERY_VOCATIONAL = "/vocational/queryVocational";
    
    /** 获取职业类型 **/
    public static final String GET_PERFESSION_TYPE = "/profession/getProfession";
    
    /** 更新用户头像  **/
    public static final String UPDATE_USER_PIC = "/user/updateUserPic";
    
    /** 发布通告 **/
    public static final String SAVE_ANNOUNCEMENT =  "/announcement/saveAnnouncement";
    
    /** 获取通告分页信息 **/
    public static final String GET_QUERY_ANNOUNCEMENT_PAGE = "/announcement/queryAnnouncementPage";
    
    /** 获取通告详情信息 **/
    public static final String GET_QUERY_ANNOUNCEMENT_DETAILS = "/announcement/queryAnnouncementDetails";
    
    /** 通告报名 **/
    public static final String SAVE_ANNOUNCEMENT_ENLIST = "/announcementEnlist/saveAnnouncementEnlist";
    
    /** 发布动态 **/
    public static final String SAVE_CHAT = "/chat/saveChat";
    /** 上传图片 **/
    public static final String SAVE_UPLOAD_IMG = "/upload/imgArr";
    
    /** 获取星秀分页信息  */
    public static final String GET_QUERY_CHATPAGE= "/chat/queryChatPage";
    
    /** 添加星秀评论功能 **/
    public static final String SAVE_COMMENT = "/comment/saveComment";
    
    /** 添加星秀点赞功能 **/	
    public static final String SAVE_LIKED = "/liked/saveLiked";
    
    /** 星秀详情 **/
    public static final String GET_CHAT_DETAILS = "/chat/queryChatDetails";
    
    /** 评论分页详情 **/
    public static final String GET_COMMNET_DETAILPAGE = "/comment/queryChatCommentDetailPage";
    
    /** 关注/取消关注 */
    public static final String SAVE_CONCERN = "/concern/userConcern";
    
    /** 添加精选 */
    public static final String SAVE_SELECT_PHOTO = "/photo/savePhoto";
    
    /** 获取精选分页信息  */
    public static final String GET_QUERY_PHTOTPAGE= "/photo/queryPhotoPage";
    /** 更新用户位置信息 **/
    public static final String UPDATE_LOCATION = "/user/updateLocal";
    /** 搜索附近的人 **/
    public static final String GET_DISTANCE_USER = "/user/getDistanceUser";
    /** 用户主页面-获取个人中心数据 **/
    public static final String GET_USER_CENTER = "/user/getUserCentre";
    
	/** 热门搜索关键词 **/
	public static final String HOT_WORDS = "";
	
	
	
	/**
	 * 获取接口对应的GET带参数URL
	 * 
	 * @param context
	 *            上下文环境
	 * @param pathStr
	 *            接口路径
	 * @param params
	 *            请求参数
	 * @return 返回完整的接口路径
	 */
	public static String getUrl(Context context, String pathStr, Map<String, Object> params) {
		StringBuffer sb = new StringBuffer("http://");
		sb.append(SERVER_IP);
		sb.append(pathStr);
		String prefixUrl = sb.toString();
		String url = NetworkUtil.getRequestUrl(context, prefixUrl, params);
		MyLog.d(url);
		return url;
	}

	/**
	 * 获取接口对应的Post带参数URL
	 * 
	 * @param context
	 *            上下文环境
	 * @param pathStr
	 *            接口路径
	 * @param params
	 *            请求参数
	 * @return 返回完整的接口路径
	 */
	public static String getPostUrl(Context context, String pathStr) {
		StringBuffer sb = new StringBuffer("http://");
		sb.append(SERVER_IP);
		sb.append(pathStr);
		String prefixUrl = sb.toString();
		MyLog.d(prefixUrl);
		return prefixUrl;
	}
}
