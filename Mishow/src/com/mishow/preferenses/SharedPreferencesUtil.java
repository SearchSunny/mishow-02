package com.mishow.preferenses;

import android.content.ContentValues;
import android.content.Context;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：SharedPreferences存储公共类
 */
public class SharedPreferencesUtil extends BasePreferences {

	private Context mContext;
	/** 首选项文件名 **/
	private static final String PERFERENCE_NAME = "mishow_perferences";
	
	/** 用户ID**/
	public static final String USER_ID = "userid";

	/** 登录账号 **/
	public static final String LASTACCOUNT = "lastAccount";
	/** 密码 **/
	public static final String PASSWORD = "password";
	
	/**
	 * 登录状态
	 */
    private final static String LOGIN_STATE = "login_state";
    
	/** 性别 **/
	public static final String SEX = "sex";
	
	/** 经度 **/
	private static final String LOCATION_LONGITUDE = "logintude";
	/** 纬度 **/
	private static final String LOCATION_LATITUDE = "latitude";
	
	/** 搜索列表历史搜索关键字 **/
	public final static String HOSTORY_SEARCH ="hostory_search"; 
	
	/**
     * 消息红点
     */
    private final static String HOME_MINE_NEWLABEL = "home_red_message";

	public SharedPreferencesUtil(Context context) {

		super(context, PERFERENCE_NAME);
		this.mContext = context;

	}

	/**
	 * 保存上次登录账号
	 * @param lastAccount 登录账号
	 */
	public void setLoginAccount(String lastAccount) {
		if (null == preferenses) {
			throw new NullPointerException("SharedPreferences is null!");
		}
		ContentValues values = new ContentValues();
		values.put(SharedPreferencesUtil.LASTACCOUNT, lastAccount);
		try {
			write(values);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存上次登录账号和密码
	 * @param lastAccount 登录账号
	 * @param password 密码
	 */
	public void setLoginAccountAndPassword(String lastAccount,String password) {
		if (null == preferenses) {
			throw new NullPointerException("SharedPreferences is null!");
		}
		ContentValues values = new ContentValues();
		values.put(SharedPreferencesUtil.LASTACCOUNT, lastAccount);
		values.put(SharedPreferencesUtil.PASSWORD, password);

		try {
			write(values);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 保存上次登录用户id
	 * @param userId 用户id
	 */
	public void setLoginfUserId(String userId) {
		if (null == preferenses) {
			throw new NullPointerException("SharedPreferences is null!");
		}
		ContentValues values = new ContentValues();
		values.put(SharedPreferencesUtil.USER_ID, userId);

		try {
			write(values);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 获取登录USSER_ID **/
	public String getUserId() {
		
		return getString(SharedPreferencesUtil.USER_ID);
	}
	
	/** 获取登录账号 **/
	public String getLastAccount() {
		
		return getString(SharedPreferencesUtil.LASTACCOUNT);
	}
	/** 清除登录账号 **/
	public void clearLastAccount(Context context) {
		if (null == preferenses) {
			throw new NullPointerException("SharedPreferences is null!");
		}

		if (null == context) {
			throw new NullPointerException("context is null!");
		}

		preferenses.edit().remove(SharedPreferencesUtil.LASTACCOUNT).commit();
	}

	public ContentValues getContentValues(String username, String password) {
		ContentValues values = new ContentValues();
		values.put(SharedPreferencesUtil.LASTACCOUNT, username);
		values.put(SharedPreferencesUtil.PASSWORD, password);
		return values;
	}
	
	//------------------------------------------------------
	
	/**
	 * 设置登录状态
	 * @param login
	 */
    public void setLoginState(boolean login) {
        if (null == preferenses) {
            throw new NullPointerException("SharedPreferences is null!");
        }
        ContentValues values = new ContentValues();
        values.put(LOGIN_STATE, login);
        try {
            write(values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取登录状态
     * @return
     */
    public boolean getLoginState() {
        return getBoolean(LOGIN_STATE, false);
    }
    
    //----------------------------------------------------
	/**
	 * 保存历史搜索内容
	 * @param lastsearch
	 * @param last
	 */
	public void saveLastSearch(String lastsearch,String last){
		if(null == preferenses){
			throw new NullPointerException("SharedPreferences is null!");
		}
		
		if(null == mContext){
			throw new NullPointerException("Context is null");
		}
		
		ContentValues values = new ContentValues();
		values.put(last, lastsearch);
		try{
			write(values);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/**
	 * 获取历史搜索内容
	 */
	public String getLastSearch(String last){
		return getString(last);
	}
	/** 
	 * 清除历史搜索内容
	 * @param last
	 */
	public void clearLastSearch(String last){
		if(null == preferenses){
			throw new NullPointerException("SharedPreferences is null!");
		}
		
		if(null == mContext){
			throw new NullPointerException("Context is null");
		}
		preferenses.edit().remove(last).commit();
	}
	
	/**
	 * 清除登录状态及登录用户ID
	 */
	public void clearUserInfo() {
		if (null == preferenses) {
			throw new NullPointerException("SharedPreferences is null!");
		}

		if (null == mContext) {
			throw new NullPointerException("context is null!");
		}
		preferenses.edit().remove(USER_ID).remove(LOGIN_STATE).commit();
	}
	//-----------------------------
	/**
	 * 保存经纬度
	 * @param Longitude 经度
	 * @param Latitude 纬度
	 */
	public void saveLoginLongitudeAndLatitude(String Longitude,String Latitude) {
		if (null == preferenses) {
			throw new NullPointerException("SharedPreferences is null!");
		}
		ContentValues values = new ContentValues();
		values.put(SharedPreferencesUtil.LOCATION_LONGITUDE, Longitude);
		values.put(SharedPreferencesUtil.LOCATION_LATITUDE, Latitude);
		try {
			write(values);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** 获取经度 **/
	public String getLongitude() {
		
		return getString(SharedPreferencesUtil.LOCATION_LONGITUDE);
	}
	
	/** 获取纬度 **/
	public String getLatitude() {
		
		return getString(SharedPreferencesUtil.LOCATION_LATITUDE);
	}
	
	//-----------------------------------------------------------------
	
	public void setHomeRedMessage(boolean haveNew) {
        if (null == preferenses) {
            throw new NullPointerException("SharedPreferences is null!");
        }

        if (null == mContext) {
            throw new NullPointerException("context is null!");
        }

        ContentValues values = new ContentValues();
        values.put(HOME_MINE_NEWLABEL, haveNew);
        try {
            write(values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	 /**
     * 首页消息红点显示<br/>
     */
    public boolean getHomeRedMessage() {
        return getBoolean(HOME_MINE_NEWLABEL, false);
    }
	
}
