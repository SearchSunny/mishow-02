package com.mishow.receiver;

import com.mishow.log.MyLog;
import com.mishow.utils.PushManagerUtil;
import com.mishow.utils.ToastUtil;

import cn.jpush.android.api.JPushInterface;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * 作者：wei.miao <br/>
 * 描述：接收极光推送广播
 */
public class MyPushMessageReceiver extends BroadcastReceiver {
	

	private static final String TAG = "MyPushMessageReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {

		Bundle bundle = intent.getExtras();

		MyLog.d(TAG, "onReceive - " + intent.getAction());

		if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
			String regId = bundle
					.getString(JPushInterface.EXTRA_REGISTRATION_ID);
			MyLog.d(TAG, "[MyReceiver] 接收Registration Id : " + regId);
			
		} else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent
				.getAction())) {
			
			String title = bundle.getString(JPushInterface.EXTRA_TITLE);
			/**
			 * 
			 */
			String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
			MyLog.d(TAG,"收到了自定义消息。消息内容是："+ message);
			// 自定义消息不会展示在通知栏，完全要开发者写代码去处理
			//消息处理
	        PushManagerUtil manager = new PushManagerUtil(context);
	        manager.dispathMessage(message, "");
			
		} else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent
				.getAction())) {
			MyLog.d(TAG, "收到了通知");
			
			// 在这里可以做些统计，或者做些其他工作
		} else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent
				.getAction())) {
			MyLog.d(TAG, "用户点击打开了通知");
			//保存服务器推送下来的附加字段。这是个 JSON 字符串。
			String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
			ToastUtil.show(context, "extras=="+extras);
			// 在这里可以自己写代码去定义用户点击后的行为
			// 自定义打开的界面
			//消息处理
	        PushManagerUtil manager = new PushManagerUtil(context);
	        manager.dispathMessage(extras, "");
		
		} else {
			
			MyLog.d(TAG, "Unhandled intent - " + intent.getAction());
		}

	}

}
