package com.mishow.refresh;

import com.mishow.R;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.AbsListView.OnScrollListener;

/**
 * 
 * 作者：wei.miao<br/>
 * 描述：下拉-上推布局
 */
public class PullPushRefreshLayout extends SwipeRefreshLayout implements OnScrollListener{

	/** 表示一个距离；滑动的时候，手的移动要大于这个距离才开始移动控件 */
	private int mTouchSlop;
	/** 列表 */
	private ListView mListView;
	/** 加载更多 监听事件 */
	private OnLoadListener mOnLoadListener;
	/** 触点开始位置 */
	private float firstTouchY;
	/** 触点结束位置 */
	private float lastTouchY;
	/** 是否正在加载 */
	private boolean isLoading = false;
	/** 是否可以加载更多 */
	private boolean canLoadMore = true;
	/** 底部加载布局 */
	private View footerLayout;
	/** 底部刷新进度条 */
	private ProgressBar progressBar;
	/** 上下文对象 */
	private Context mContext;
	
	/* 左滑动显示“删除按钮” */
	private float minDis = 10;
	private float mLastMotionX;// 记住上次X触摸屏的位置
	private float mLastMotionY;// 记住上次Y触摸屏的位置
	private boolean isLock = false;
	/* 左滑动显示“删除按钮” */

	public PullPushRefreshLayout(Context context) {
		super(context);
		if (isInEditMode()) { return; } 
		init(context);
		
	}

	public PullPushRefreshLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (isInEditMode()) { return; } 
		init(context);
	}
	
	private void init(Context context) {
		this.mContext = context;
		mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
		setColorScheme(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
				android.R.color.holo_orange_light, android.R.color.holo_red_light);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);

	}

	/**
	 * 初始化ListView对象
	 */
	public void setListView(ListView listView) {
		mListView = listView;
		mListView.setFooterDividersEnabled(false);
		mListView.setOnScrollListener(this);
		footerLayout = LayoutInflater.from(mContext).inflate(R.layout.refleshlayout_footer, null);
		progressBar = (ProgressBar) footerLayout.findViewById(R.id.load_progress_bar);
		// 这里可以替换为自定义的footer布局
		// you can custom FooterView
		mListView.addFooterView(footerLayout);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		final int action = event.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			firstTouchY = event.getRawY();
			break;

		case MotionEvent.ACTION_MOVE:
			lastTouchY = event.getRawY();
//			if (canLoadMore()) {
//				loadData();
//			}
			break;
		default:
			break;
		}

		return super.dispatchTouchEvent(event);
	}

	/**
	 * 判断是否可以满足加载更多
	 * 
	 * @return
	 */
	private boolean canLoadMore() {
		return canLoadMore && isBottom() && !isLoading && isPullingUp();
	}

	/**
	 * 判断ListView是否滑动到底部
	 */
	private boolean isBottom() {
		if (mListView == null)
			return false;
		if (mListView.getCount() > 0) {
			if (mListView.getLastVisiblePosition() == mListView.getAdapter().getCount() - 1
					&& mListView.getChildAt(mListView.getChildCount() - 1).getBottom() <= mListView.getHeight()) {
				return true;
			}
		}
		return false;
	}

	private boolean isPullingUp() {
		return (firstTouchY - lastTouchY) >= mTouchSlop;
	}

	/**
	 * 开始加载数据
	 */
	private void loadData() {
		if (mOnLoadListener != null) {
			setLoading(true);
		}
	}

	/**
	 * 设置加载状态
	 * 
	 * @param loading
	 */
	public void setLoading(boolean loading) {
		if (mListView == null)
			return;
		isLoading = loading;
		if (loading) {// 开始加载更多，显示底部进度条
			if (isRefreshing()) {
				setRefreshing(false);
			}
			mListView.setSelection(mListView.getAdapter().getCount() - 1);
			mOnLoadListener.onLoad();
			startLoadMore();
		} else {// 结束加载更多
			firstTouchY = 0;
			lastTouchY = 0;
			stopLoadMore();
		}
	}

	/**
	 * @param loadListener
	 */
	public void setOnLoadListener(OnLoadListener loadListener) {
		mOnLoadListener = loadListener;
	}

	/**
	 * 上拉加载更多监听事件
	 */
	public interface OnLoadListener {
		public void onLoad();
	}

	/**
	 * 开始刷新
	 */
	private void startLoadMore() {
		if (progressBar != null) {
			progressBar.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 结束刷新
	 */
	private void stopLoadMore() {
		if (progressBar != null) {
			progressBar.setVisibility(View.GONE);
		}
	}

	/**
	 * 设置是否可以加载更多
	 * 
	 * @param canLoadMore
	 */
	public void setCanLoadMore(boolean canLoadMore) {
		this.canLoadMore = canLoadMore;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (canLoadMore()) {
			loadData();
		}
	}
	
	/**
	 * 如果一个ViewGroup的onInterceptTouchEvent()方法返回true，说明Touch事件被截获，
	 * 子View不再接收到Touch事件，而是转向本ViewGroup的
	 * onTouchEvent()方法处理。从Down开始，之后的Move，Up都会直接在onTouchEvent()方法中处理。
	 * 先前还在处理touch event的child view将会接收到一个 ACTION_CANCEL。
	 * 如果onInterceptTouchEvent()返回false，则事件会交给child view处理。
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (!isIntercept(ev)) {
			return false;
		}
		return super.onInterceptTouchEvent(ev);
	}
	
	/**
	 * 检测是ListView滑动还是item滑动 isLock 一旦判读是item滑动，则在up之前都是返回false
	 */
	private boolean isIntercept(MotionEvent ev) {
		float x = ev.getX();
		float y = ev.getY();
		int action = ev.getAction();
		switch (action) {
			case MotionEvent.ACTION_DOWN :
				mLastMotionX = x;
				mLastMotionY = y;
				break;
			case MotionEvent.ACTION_MOVE :
				if (!isLock) {
					float deltaX = Math.abs(mLastMotionX - x);
					float deltay = Math.abs(mLastMotionY - y);
					mLastMotionX = x;
					mLastMotionY = y;
					if (deltaX > deltay && deltaX > minDis) {
						isLock = true;
						return false;
					}
				} else {
					return false;
				}
				break;
			case MotionEvent.ACTION_UP :
				isLock = false;
				break;
			case MotionEvent.ACTION_CANCEL :
				isLock = false;
				break;
		}
		return true;
	}

}
