package com.mishow.request.core;

/**
 * 版权：融贯资讯 <br/>
 * 作者：xingjian.lan@rogrand.com <br/>
 * 生成日期：2014-12-24 <br/>
 * 描述：java Model 基类，统一处理head和mac
 */

public class BaseBean {

	private String mac;
	private Head head;
	protected Body body;

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public Head getHead() {
		return head;
	}

	public void setHead(Head head) {
		this.head = head;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Head {
		private String method;
		private String serialNumber;
		private String version;

		public String getMethod() {
			return method;
		}

		public void setMethod(String method) {
			this.method = method;
		}

		public String getSerialNumber() {
			return serialNumber;
		}

		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}
	}

	// 返回体内容部分
	public static class Body {
		
		private String code;
		private String message;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

	}
}
