package com.mishow.request.response;



import com.mishow.bean.AnnouncementDetails;
import com.mishow.request.core.Response;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：通告详情返回数据
 */
public class AnnouncementDetailsResponse extends Response{
	
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		private AnnouncementDetails  result;

		public AnnouncementDetails getResult() {
			return result;
		}

		public void setResult(AnnouncementDetails result) {
			this.result = result;
		}
	}
}
