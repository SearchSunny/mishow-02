package com.mishow.request.response;

import com.mishow.request.core.Response;
/**
 * 作者：wei.miao <br/>
 * 描述：验证码请求响应返回
 */
public class AuthCodeResponse extends Response {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		
		private Object result;
		
		public Object getResult() {
			return result;
		}
		public void setResult(Object result) {
			this.result = result;
		}

	}
	
}
