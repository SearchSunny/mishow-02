package com.mishow.request.response;

import com.mishow.bean.ChatDetail;
import com.mishow.request.core.Response;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：星秀详情返回数据
 */
public class ChatDetailResponse extends Response{
	
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		private ChatDetail  result;

		public ChatDetail getResult() {
			return result;
		}

		public void setResult(ChatDetail result) {
			this.result = result;
		}
	}
}
