package com.mishow.request.response;

import java.util.ArrayList;
import java.util.List;

import com.mishow.bean.Announcement;
import com.mishow.bean.ChatVO;
import com.mishow.request.core.Response;
/**
 * 
 * 作者：wei.mia <br/>
 * 描述：星秀列表返回数据
 */
public class ChatResponse extends Response {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}

	}

	public static class Result {

		private ArrayList<ChatVO> dataList;
		private int total;

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public ArrayList<ChatVO> getDataList() {
			return dataList;
		}

		public void setDataList(ArrayList<ChatVO> dataList) {
			this.dataList = dataList;
		}
	}

}
