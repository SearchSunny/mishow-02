package com.mishow.request.response;

import java.util.ArrayList;

import com.mishow.bean.ChatVO;
import com.mishow.bean.CommentChatDetail;
import com.mishow.request.core.Response;
import com.mishow.request.response.ChatResponse.Result;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：星秀详情返回数据
 */
public class CommentChatDetailResponse extends Response {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}
	}

	public static class Result {

		private ArrayList<CommentChatDetail> dataList;

		private int total;

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public ArrayList<CommentChatDetail> getDataList() {

			return dataList;
		}

		public void setDataList(ArrayList<CommentChatDetail> dataList) {

			this.dataList = dataList;
		}
	}
}
