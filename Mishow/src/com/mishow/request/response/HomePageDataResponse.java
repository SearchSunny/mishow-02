package com.mishow.request.response;

import java.util.ArrayList;

import com.mishow.bean.HomeBanner;
import com.mishow.bean.HomeStarShow;
import com.mishow.request.core.Response;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：首页返回数据
 */
public class HomePageDataResponse extends Response {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}

	}

	public static class Result {
		/**
		 * 顶部banner图片
		 */
		private ArrayList<HomeBanner> topbanner;
		/**
		 * 中间广告图
		 */
		private ArrayList<HomeBanner> conbanner;
		/**
		 * 底部列表
		 */
		private ArrayList<HomeStarShow> userlist;
		
		
		
		public ArrayList<HomeBanner> getTopbanner() {
			return topbanner;
		}
		public void setTopbanner(ArrayList<HomeBanner> topbanner) {
			this.topbanner = topbanner;
		}
		public ArrayList<HomeBanner> getConbanner() {
			return conbanner;
		}
		public void setConbanner(ArrayList<HomeBanner> conbanner) {
			this.conbanner = conbanner;
		}
		public ArrayList<HomeStarShow> getUserlist() {
			return userlist;
		}
		public void setUserlist(ArrayList<HomeStarShow> userlist) {
			this.userlist = userlist;
		}
		
		
	}

}
