package com.mishow.request.response;

import com.mishow.request.core.Response;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：热门搜索Response
 */
public class HotSearchResponse extends Response {
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		private Result result;
		public static class Result{
			
		}

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}

	}
}
