package com.mishow.request.response;


import java.util.ArrayList;

import com.mishow.request.core.Response;
import com.mishow.request.result.LabelVO;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：所有丽质标签
 */
public class LabelVOResponse extends Response{
	
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		private ArrayList<LabelVO>  result;

		public ArrayList<LabelVO> getResult() {
			return result;
		}
		public void setResult(ArrayList<LabelVO> result) {
			this.result = result;
		}
	}

}
