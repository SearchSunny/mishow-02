package com.mishow.request.response;

import com.mishow.request.core.Response;
import com.mishow.request.result.User;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：登录请求响应返回
 */
public class LoginResponse extends Response {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		private User result;

		public User getResult() {
			return result;
		}
		public void setResult(User result) {
			this.result = result;
		}
	}

}
