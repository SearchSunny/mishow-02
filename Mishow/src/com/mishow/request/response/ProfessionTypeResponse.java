package com.mishow.request.response;


import java.util.ArrayList;

import com.mishow.request.core.Response;
import com.mishow.request.result.Profession;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：标签类型
 */
public class ProfessionTypeResponse extends Response{
	
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		private ArrayList<Profession> result;

		public ArrayList<Profession> getResult() {
			return result;
		}
		public void setResult(ArrayList<Profession> result) {
			this.result = result;
		}
	}

}
