package com.mishow.request.response;

import java.util.ArrayList;

import com.mishow.bean.ChatVO;
import com.mishow.bean.PhotoVO;
import com.mishow.request.core.Response;
/**
 * 
 * 作者：wei.mia <br/>
 * 描述：精选列表返回数据
 */
public class SelectPhotoResponse extends Response {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}

	}

	public static class Result {

		private ArrayList<PhotoVO> dataList;
		private int total;

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public ArrayList<PhotoVO> getDataList() {
			return dataList;
		}

		public void setDataList(ArrayList<PhotoVO> dataList) {
			this.dataList = dataList;
		}
	}

}
