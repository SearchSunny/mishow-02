package com.mishow.request.response;

import com.mishow.request.core.Response;
import com.mishow.request.result.Stature;
/**
 * 
 * 作者：wei.mia <br/>
 * 描述：用户丽质资料请求响应返回
 */
public class StatureResponse extends Response{
	
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {
		private Stature result;

		public Stature getResult() {
			return result;
		}
		public void setResult(Stature result) {
			this.result = result;
		}
	}

}
