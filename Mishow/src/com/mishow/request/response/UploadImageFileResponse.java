package com.mishow.request.response;

import java.util.ArrayList;

import com.mishow.request.core.Response;
/**
 * 作者：wei.miao <br/>
 * 描述：上传图片接口返回
 */
public class UploadImageFileResponse extends Response {

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}

	}
	
	public static class Result {

		private ArrayList<String> dataList;
		

		public ArrayList<String> getDataList() {
			return dataList;
		}

		public void setDataList(ArrayList<String> dataList) {
			this.dataList = dataList;
		}
	}

}
