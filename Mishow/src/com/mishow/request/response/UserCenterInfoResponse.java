package com.mishow.request.response;

import java.util.ArrayList;

import com.mishow.request.core.Response;
import com.mishow.request.result.Stature;
import com.mishow.request.result.User;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：个人主页返回数据
 */
public class UserCenterInfoResponse extends Response{
	
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}
	}
	
	public static class Result {
		/** 用户数据 **/
		private User user;
		/** 丽质资料 **/
		private Stature stature;
		/** 精选 **/
		private ArrayList<String> piclist;
		/** 个人动态 **/
		private ArrayList<String> chats;
		/** 风格标签 **/
		private String stylelabel;
		/** 外貌标签 **/
		private String earancelabel;
		/** 体型标签 **/
		private String shapelabel;
		/** 魅力部位 **/
		private String charmlabel;
		
		
		
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public Stature getStature() {
			return stature;
		}
		public void setStature(Stature stature) {
			this.stature = stature;
		}
		public ArrayList<String> getPiclist() {
			return piclist;
		}
		public void setPiclist(ArrayList<String> piclist) {
			this.piclist = piclist;
		}
		public ArrayList<String> getChats() {
			return chats;
		}
		public void setChats(ArrayList<String> chats) {
			this.chats = chats;
		}
		public String getStylelabel() {
			return stylelabel;
		}
		public void setStylelabel(String stylelabel) {
			this.stylelabel = stylelabel;
		}
		public String getEarancelabel() {
			return earancelabel;
		}
		public void setEarancelabel(String earancelabel) {
			this.earancelabel = earancelabel;
		}
		public String getShapelabel() {
			return shapelabel;
		}
		public void setShapelabel(String shapelabel) {
			this.shapelabel = shapelabel;
		}
		public String getCharmlabel() {
			return charmlabel;
		}
		public void setCharmlabel(String charmlabel) {
			this.charmlabel = charmlabel;
		}
		
	}

}
