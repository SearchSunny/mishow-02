package com.mishow.request.response;


import java.util.ArrayList;

import com.mishow.bean.UserDistanceVO;
import com.mishow.request.core.Response;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：搜索附近返回结果
 */
public class UserDistanceResponse extends Response{

	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body extends Response.Body {

		private Result result;

		public Result getResult() {
			return result;
		}

		public void setResult(Result result) {
			this.result = result;
		}
		
	}
	
	public static class Result {

		private ArrayList<UserDistanceVO> dataList;
		private int total;

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public ArrayList<UserDistanceVO> getDataList() {
			return dataList;
		}

		public void setDataList(ArrayList<UserDistanceVO> dataList) {
			this.dataList = dataList;
		}
	}
	
}
