package com.mishow.request.result;

import java.io.Serializable;

public class Label implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3531824694088964821L;
	
	/** ID */
	private Integer id;

	/** 用户个人标签(0-为公用标签,其他为用户ID) */
	private Integer userId;

	/** 标签模块(0-精选,1-个人标签) */
	private Integer labelModule;

	/** 标签分类(0-精选标签,1-风格标签,2-外貌标签,3-体型标签,4-魅力部位) */
	private Integer labelClass;

	/** 标签描述 */
	private String labelName;

	/** 父级标签 */
	private String labelParent;
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getLabelModule() {
		return this.labelModule;
	}

	public void setLabelModule(Integer labelModule) {
		this.labelModule = labelModule;
	}

	public Integer getLabelClass() {
		return this.labelClass;
	}

	public void setLabelClass(Integer labelClass) {
		this.labelClass = labelClass;
	}

	public String getLabelName() {
		return this.labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public String getLabelParent() {
		return labelParent;
	}

	public void setLabelParent(String labelParent) {
		this.labelParent = labelParent;
	}

}
