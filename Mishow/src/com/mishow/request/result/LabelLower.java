package com.mishow.request.result;

import java.io.Serializable;

public class LabelLower implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5166532823365655974L;
	
	private String labelname;
	
	private Integer labeValue;

	public String getLabelname() {
		return labelname;
	}

	public void setLabelname(String labelname) {
		this.labelname = labelname;
	}

	public Integer getLabeValue() {
		return labeValue;
	}

	public void setLabeValue(Integer labeValue) {
		this.labeValue = labeValue;
	}

}
