package com.mishow.request.result;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：所有丽质标签
 */
public class LabelVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -327506102557894860L;
	
	private String labelClass;
	
	private List<LabelLower> labelLowers;

	public String getLabelClass() {
		return labelClass;
	}

	public void setLabelClass(String labelClass) {
		this.labelClass = labelClass;
	}

	public List<LabelLower> getLabelLowers() {
		return labelLowers;
	}

	public void setLabelLowers(List<LabelLower> labelLowers) {
		this.labelLowers = labelLowers;
	}

}
