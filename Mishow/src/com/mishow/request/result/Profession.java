package com.mishow.request.result;

import java.io.Serializable;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：职业类型
 */
public class Profession implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4305761772691337746L;

	/** 主键ID */
	private Integer professionId;

	/** 职业昵称 */
	private String professionName;

	public Integer getProfessionId() {
		return professionId;
	}

	public void setProfessionId(Integer professionId) {
		this.professionId = professionId;
	}

	public String getProfessionName() {
		return professionName;
	}

	public void setProfessionName(String professionName) {
		this.professionName = professionName;
	}
	
	

}
