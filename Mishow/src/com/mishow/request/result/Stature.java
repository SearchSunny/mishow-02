package com.mishow.request.result;

import java.io.Serializable;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：用户丽质资料
 */
public class Stature implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3015421772023609109L;
	
	
	/** 用户ID */
	private Integer userId;

	/** 身高(单位：cm) */
	private Integer statureHeight;

	/** 体重(单位：kg) */
	private Integer statureWeight;

	/** 胸围 */
	private Integer statureBust;

	/** 腰围 */
	private Integer statureWaist;

	/** 臀围 */
	private Integer statureHips;

	/** 鞋码 */
	private Integer statureFeet;

	/** 三围(40—40—40) */
	private String statureMeasurements;

	/** 罩杯(A、B、C、D、E、F) */
	private String statureCup;

	/** 腿长(单位：cm) */
	private Integer statureLeg;

	/** 擅长 */
	private String statureExcelled;
	
	/** 擅长 字符*/
	private String statureExcelledStr;
	
	/** 标签 */
	private String statureLabel;
	
	/** 标签字符 */
	private String statureLabelStr;
	
	/** 学校 */
	private String statureSchool;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getStatureHeight() {
		return statureHeight;
	}

	public void setStatureHeight(Integer statureHeight) {
		this.statureHeight = statureHeight;
	}

	public Integer getStatureWeight() {
		return statureWeight;
	}

	public void setStatureWeight(Integer statureWeight) {
		this.statureWeight = statureWeight;
	}

	public Integer getStatureBust() {
		return statureBust;
	}

	public void setStatureBust(Integer statureBust) {
		this.statureBust = statureBust;
	}

	public Integer getStatureWaist() {
		return statureWaist;
	}

	public void setStatureWaist(Integer statureWaist) {
		this.statureWaist = statureWaist;
	}

	public Integer getStatureHips() {
		return statureHips;
	}

	public void setStatureHips(Integer statureHips) {
		this.statureHips = statureHips;
	}

	public Integer getStatureFeet() {
		return statureFeet;
	}

	public void setStatureFeet(Integer statureFeet) {
		this.statureFeet = statureFeet;
	}

	public String getStatureMeasurements() {
		return statureMeasurements;
	}

	public void setStatureMeasurements(String statureMeasurements) {
		this.statureMeasurements = statureMeasurements;
	}

	public String getStatureCup() {
		return statureCup;
	}

	public void setStatureCup(String statureCup) {
		this.statureCup = statureCup;
	}

	public Integer getStatureLeg() {
		return statureLeg;
	}

	public void setStatureLeg(Integer statureLeg) {
		this.statureLeg = statureLeg;
	}

	public String getStatureExcelled() {
		return statureExcelled;
	}

	public void setStatureExcelled(String statureExcelled) {
		this.statureExcelled = statureExcelled;
	}

	public String getStatureExcelledStr() {
		return statureExcelledStr;
	}

	public void setStatureExcelledStr(String statureExcelledStr) {
		this.statureExcelledStr = statureExcelledStr;
	}

	public String getStatureLabel() {
		return statureLabel;
	}

	public void setStatureLabel(String statureLabel) {
		this.statureLabel = statureLabel;
	}

	public String getStatureSchool() {
		return statureSchool;
	}

	public void setStatureSchool(String statureSchool) {
		this.statureSchool = statureSchool;
	}

	
}
