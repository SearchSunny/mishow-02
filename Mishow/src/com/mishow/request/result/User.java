package com.mishow.request.result;

import java.io.Serializable;
import java.util.Date;


public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1812478981400825419L;

	private Integer userId;//用户ID
	
	private Integer userState;//状态(0-正常,-1-禁用)
	
	private String userAccount;//用户帐号
	
	private String userPass;//用户密码
	
	private String userPhone;//手机号
	
	private String userCname;//用户昵称
	
	private Integer userSex;//性别
	
	private String userHead;//用户头像
	
	private String userTopImg;//用户封面
	
	private String userInvite;//用户邀请码
	
	private String userInviteCode;//被邀请码
	
	private Date lastLoginTime;//最后一次登录时间
	
	private Date createTime;//创建时间
	
	private Date updateTime;//更新时间
	
	private String userQq;//qq
	
	private String userWeibo;//微博
	
	private String userWeixin;//微信
	
	private String userOpenid;//openid
	
	private String userJgTag;//极光Tag
	
	private String userJgRegisterid;//极光注册ID
	
	private String userProvince;//省名称
	
	private String userProvinceCode;//省编码
	
	private String userCity;//城市名称
	
	private String userCityCode;//市编码
	
	private String userDistrict;//区名称
	
	private String userDistrictCode;//区编码
	
	
	private Date userBirth;//出生年月
	
	private String userAutograph;//个性签名
	
	private Integer totalIntegral;//总积分
	private Integer isSign;//是否签到
	private String authType;//登录类型(0-手机登录，1-QQ 2-微信 3-微博)
	private String snCode;//短信验证码
	private String newUserPhone;//新手机号
	
	private String userProfession;//职业ID
	
	private String userProfessionStr;//职业类型名称
	
	
	
	
	public String getUserProfessionStr() {
		return userProfessionStr;
	}
	public void setUserProfessionStr(String userProfessionStr) {
		this.userProfessionStr = userProfessionStr;
	}
	
	
	public String getUserTopImg() {
		return userTopImg;
	}
	public void setUserTopImg(String userTopImg) {
		this.userTopImg = userTopImg;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getUserState() {
		return userState;
	}
	public void setUserState(Integer userState) {
		this.userState = userState;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUserCname() {
		return userCname;
	}
	public void setUserCname(String userCname) {
		this.userCname = userCname;
	}
	public Integer getUserSex() {
		return userSex;
	}
	public void setUserSex(Integer userSex) {
		this.userSex = userSex;
	}
	public String getUserHead() {
		return userHead;
	}
	public void setUserHead(String userHead) {
		this.userHead = userHead;
	}
	public String getUserInvite() {
		return userInvite;
	}
	public void setUserInvite(String userInvite) {
		this.userInvite = userInvite;
	}
	public String getUserInviteCode() {
		return userInviteCode;
	}
	public void setUserInviteCode(String userInviteCode) {
		this.userInviteCode = userInviteCode;
	}
	public Date getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getUserQq() {
		return userQq;
	}
	public void setUserQq(String userQq) {
		this.userQq = userQq;
	}
	public String getUserWeibo() {
		return userWeibo;
	}
	public void setUserWeibo(String userWeibo) {
		this.userWeibo = userWeibo;
	}
	public String getUserWeixin() {
		return userWeixin;
	}
	public void setUserWeixin(String userWeixin) {
		this.userWeixin = userWeixin;
	}
	public String getUserOpenid() {
		return userOpenid;
	}
	public void setUserOpenid(String userOpenid) {
		this.userOpenid = userOpenid;
	}
	public String getUserJgTag() {
		return userJgTag;
	}
	public void setUserJgTag(String userJgTag) {
		this.userJgTag = userJgTag;
	}
	public String getUserJgRegisterid() {
		return userJgRegisterid;
	}
	public void setUserJgRegisterid(String userJgRegisterid) {
		this.userJgRegisterid = userJgRegisterid;
	}
	public String getUserProvince() {
		return userProvince;
	}
	public void setUserProvince(String userProvince) {
		this.userProvince = userProvince;
	}
	public String getUserProvinceCode() {
		return userProvinceCode;
	}
	public void setUserProvinceCode(String userProvinceCode) {
		this.userProvinceCode = userProvinceCode;
	}
	public String getUserCity() {
		return userCity;
	}
	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}
	public String getUserCityCode() {
		return userCityCode;
	}
	public void setUserCityCode(String userCityCode) {
		this.userCityCode = userCityCode;
	}
	public String getUserDistrict() {
		return userDistrict;
	}
	public void setUserDistrict(String userDistrict) {
		this.userDistrict = userDistrict;
	}
	public String getUserDistrictCode() {
		return userDistrictCode;
	}
	public void setUserDistrictCode(String userDistrictCode) {
		this.userDistrictCode = userDistrictCode;
	}
	public Date getUserBirth() {
		return userBirth;
	}
	public void setUserBirth(Date userBirth) {
		this.userBirth = userBirth;
	}
	public String getUserAutograph() {
		return userAutograph;
	}
	public void setUserAutograph(String userAutograph) {
		this.userAutograph = userAutograph;
	}
	public Integer getTotalIntegral() {
		return totalIntegral;
	}
	public void setTotalIntegral(Integer totalIntegral) {
		this.totalIntegral = totalIntegral;
	}
	public Integer getIsSign() {
		return isSign;
	}
	public void setIsSign(Integer isSign) {
		this.isSign = isSign;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getSnCode() {
		return snCode;
	}
	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}
	public String getNewUserPhone() {
		return newUserPhone;
	}
	public void setNewUserPhone(String newUserPhone) {
		this.newUserPhone = newUserPhone;
	}
	public String getUserProfession() {
		return userProfession;
	}
	public void setUserProfession(String userProfession) {
		this.userProfession = userProfession;
	}
	
	
}
