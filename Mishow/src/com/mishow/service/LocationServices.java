package com.mishow.service;

import java.util.HashMap;
import java.util.Map;

import com.amap.api.location.AMapLocation;
import com.android.volley.Request;
import com.mishow.R;
import com.mishow.log.MyLog;
import com.mishow.map.location.CustomMyLocation;
import com.mishow.net.HttpUrlConstant;
import com.mishow.net.NetworkUtil;
import com.mishow.preferenses.SharedPreferencesUtil;
import com.mishow.request.core.Response;
import com.mishow.request.listener.OnResponseListener;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.ToastUtil;
import com.mishow.volley.http.request.FastJsonRequest;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述： 定位服务
 */
public class LocationServices extends Service{

	private SharedPreferencesUtil mSharedPreferencesUtil;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		MyLog.d("LocationServices===onCreate");
		mSharedPreferencesUtil = new SharedPreferencesUtil(this);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		MyLog.d("LocationServices===onStartCommand");
		CustomMyLocation.getInstanceLocked(this, mOnLocationListener).activate(null);
		return START_NOT_STICKY;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		CustomMyLocation.getInstanceLocked(this, mOnLocationListener).deactivate();
	}
	
	
	
	/**
     * 定位回调
     */
    private CustomMyLocation.OnLocationListener mOnLocationListener = new CustomMyLocation.OnLocationListener() {
        @Override
        public void OnLocationSuccess(AMapLocation amapLocation) {
            double latitude = amapLocation.getLatitude();
            double longitude = amapLocation.getLongitude();
            MyLog.d("latitude==" + latitude + "-" + "longitude==" + longitude);
            
            //ToastUtil.show(LocationServices.this,"latitude==" + latitude + "-" + "longitude==" + longitude);
            mSharedPreferencesUtil.saveLoginLongitudeAndLatitude(longitude+"", latitude+"");
            if (mSharedPreferencesUtil.getLoginState()) {
            	
            	doPostLocation(longitude+"", latitude+"");
            }
            
        }

        @Override
        public void OnLocationFail() {
        	MyLog.d("OnLocationFail=====定位失败");
        }
    };
    
    
    /**
	 * 更新用户位置信息
	 */
	private void doPostLocation(String longitude,String latitude) {

		if (!AndroidUtil.isNetworkAvailable(this)) {
			ToastUtil.show(this,getResources().getString(R.string.no_connector));
			return;
		}
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", mSharedPreferencesUtil.getUserId());// 当前登陆用户ID
		params.put("userLongitude", 0+"");//经度
		params.put("userLatitude","");//纬度

		Map<String, String> lastParams = NetworkUtil.generaterPostRequestParams(this, params);

		String postUrl = HttpUrlConstant.getPostUrl(this,HttpUrlConstant.UPDATE_LOCATION);
		OnResponseListener<Response> listener = new OnResponseListener<Response>(this) {

			@Override
			public void onSuccess(Response response) {
				MyLog.d("更新用户位置信息=====成功");
			}

			@Override
			public void onError(String code, String message) {
				MyLog.d("更新用户位置信息=====失败");
			}

			@Override
			public void onCompleted() {

			}
		};
		NetworkUtil.executeRequest(this,new FastJsonRequest<Response>(Request.Method.POST,
				postUrl, Response.class, listener, listener)
				.setParams(lastParams));

	}
}
