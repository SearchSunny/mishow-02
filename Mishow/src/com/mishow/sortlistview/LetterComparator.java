package com.mishow.sortlistview;

import java.util.Comparator;

/**
 * 版权：融贯资讯 <br/>
 * 作者：tao.li@rograndec.com <br/>
 * 生成日期：2016-11-15 <br/>
 * 描述：
 */
public class LetterComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        if ("@".equals(o1) || "#".equals(o2)) {
            return -1;
        } else if ("#".equals(o1) || "@".equals(o2)) {
            return 1;
        } else {
            return o1.compareTo(o2);
        }
    }

}
