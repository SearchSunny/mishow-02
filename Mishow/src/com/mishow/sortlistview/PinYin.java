package com.mishow.sortlistview;

import android.text.TextUtils;

import com.mishow.sortlistview.HanziToPinyin.Token;

import java.util.ArrayList;

import static android.R.attr.text;

public class PinYin {
    private static int currentapiVersion = android.os.Build.VERSION.SDK_INT;//当前api版本
    public final static String[] polyphones = new String[]{"重庆", "厦门"};//多音字处理
    public final static String[] pys = new String[]{"chongqing", "xiamen"};//多音字处理

    //汉字返回拼音，字母原样返回，都转换为小写
    public static String getPinYin(String input) {

        if (!TextUtils.isEmpty(input)) {
            for (int i = 0; i < polyphones.length; i++) {
                if (input.contains(polyphones[i])) {
                    input = input.replaceAll(polyphones[i], pys[i]);
                }
            }
        }

        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return HanziToPinyinHApi.getInstance().transliterate(input);
        }

        ArrayList<Token> tokens = HanziToPinyin.getInstance().get(input);
        StringBuilder sb = new StringBuilder();
        if (tokens != null && tokens.size() > 0) {
            for (Token token : tokens) {
                if (Token.PINYIN == token.type) {
                    sb.append(token.target);
                } else {
                    sb.append(token.source);
                }
            }
        }
        return sb.toString().toLowerCase();
    }
}
