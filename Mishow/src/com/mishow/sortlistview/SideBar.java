package com.mishow.sortlistview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import com.mishow.utils.DeviceUtil;

public class SideBar extends View {
    // 触摸事件
    private OnTouchingLetterChangedListener onTouchingLetterChangedListener;
    // 26个字母
    public static String[] b = {"A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "#"};
    private List<String> mNewLetterList;
    private int choose = -1;
    private Paint paint = new Paint();
    private int mTextSize;
    private int totalHeight;
    private int mVerticalSpacing;

    private TextView mTextDialog;

    public void setTextView(TextView mTextDialog) {
        this.mTextDialog = mTextDialog;
    }

    public SideBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SideBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SideBar(Context context) {
        super(context);
        init();
    }

    public void setTextSize(int textSize) {
        mTextSize = DeviceUtil.sp2px(getContext().getApplicationContext(), textSize);
        this.paint.setTextSize(mTextSize);
    }

    public int getTextSize() {
        return mTextSize;
    }

    public void setVerticalSpacing(int verticalSpacing) {
        mVerticalSpacing = DeviceUtil.dip2px(getContext().getApplicationContext(), verticalSpacing);
    }

    public int getVerticalSpacing() {
        return mVerticalSpacing;
    }

    public void setLetterList(List<String> newLetterList) {
        mNewLetterList = newLetterList;
        invalidate();
    }

    public List<String> getLetterList() {
        return mNewLetterList;
    }

    public void setChoicePosition(int position) {
        choose = position;
        invalidate();
    }

    private void init() {
        setTextSize(12);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);

        ArrayList<String> mDefaultLetterList = new ArrayList<>();
        for (String s : b) {
            mDefaultLetterList.add(s);
        }
        mNewLetterList = mDefaultLetterList;
    }

    /**
     * 重写这个方法
     */
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.TRANSPARENT);
        // 获取焦点改变背景颜色.
        int width = getWidth(); // 获取对应宽度
        Paint.FontMetricsInt fontMetrics = paint.getFontMetricsInt();
        int baseY = fontMetrics.bottom + (fontMetrics.descent - fontMetrics.ascent) / 2;
        baseY = baseY + mVerticalSpacing;

        for (int i = 0; i < mNewLetterList.size(); i++) {
            String letter = mNewLetterList.get(i);
            paint.setColor(Color.parseColor("#333333"));
            // 选中的状态
            if (i == choose) {
                paint.setColor(Color.parseColor("#3399ff"));
                paint.setFakeBoldText(true);
            }
            // x坐标等于中间-字符串宽度的一半.
            float xPos = width / 2 - paint.measureText(letter) / 2;
            float yPos = baseY * i + baseY;
            totalHeight = (int) yPos;
            canvas.drawText(letter, xPos, yPos, paint);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        final float y = event.getY();//
        final int oldChoose = choose;
        final OnTouchingLetterChangedListener listener = onTouchingLetterChangedListener;
        final int c = (int) (y / totalHeight * mNewLetterList.size());

        switch (action) {
            case MotionEvent.ACTION_UP:
                setBackgroundDrawable(new ColorDrawable(0xffffffff));
//                choose = -1;//
                invalidate();
                if (mTextDialog != null) {
                    mTextDialog.setVisibility(View.INVISIBLE);
                }
                break;

            default:
                // setBackgroundResource(R.drawable.sidebar_background);
                if (oldChoose != c) {
                    if (c >= 0 && c < mNewLetterList.size()) {
                        String letter = mNewLetterList.get(c);
                        if (listener != null) {
                            listener.onTouchingLetterChanged(letter);
                        }
                        if (mTextDialog != null) {
                            mTextDialog.setText(letter);
                            mTextDialog.setVisibility(View.VISIBLE);
                        }

                        choose = c;
                        invalidate();
                    }
                }

                break;
        }
        return true;
    }

    /**
     * 向外公开的方法
     *
     * @param onTouchingLetterChangedListener
     */
    public void setOnTouchingLetterChangedListener(
            OnTouchingLetterChangedListener onTouchingLetterChangedListener) {
        this.onTouchingLetterChangedListener = onTouchingLetterChangedListener;
    }

    /**
     * 接口
     *
     * @author coder
     */
    public interface OnTouchingLetterChangedListener {
        void onTouchingLetterChanged(String s);
    }

}