package com.mishow.utils;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：描述：二维码常量定义
 */
public class CodeConstants {

    /**
     * 二维码跳转地址url
     */
    public static final String SCAN_CODE_STRING = "http://img.kkmaiyao.com/kkmy/upload/erweimaxiazai/download.html";

    /**
     * 用户邀请码
     */
    public final static int USER_CODE_TYPE = 101;
    /**
     * 店员邀请码
     */
    public final static int DY_CODE_TYPE = 102;
    /**
     * 药店二维码
     */
    public final static int MERCHANT_CODE_TYPE = 103;
    /**
     * 订单提货码
     */
    public final static int ORDER_CODE_TYPE = 104;
}
