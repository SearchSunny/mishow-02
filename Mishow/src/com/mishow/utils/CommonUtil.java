package com.mishow.utils;

import java.util.Iterator;
import java.util.List;

import com.mishow.log.MyLog;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

public class CommonUtil {

	/**
	 * toast
	 * @param mContext
	 * @param text
	 */
	public static void makeText(Context mContext,String text){
		
		Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * 获取当前运行APP名称
	 * @param mContext
	 * @param pID
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getAppName(Context mContext,int pID) {
		String processName = null;
		ActivityManager am = (ActivityManager) mContext.getSystemService("ACTIVITY_SERVICE");
		List<RunningAppProcessInfo> processList = am.getRunningAppProcesses();
		
		Iterator<RunningAppProcessInfo> process = processList.iterator();
		PackageManager pm = mContext.getPackageManager();
		while (process.hasNext()) {
			ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (process.next());
			try {
				if (info.pid == pID) {
					processName = info.processName;
					return processName;
				}
			} catch (Exception e) {
				MyLog.e(e);
			}
		}
		return processName;
	}
}
