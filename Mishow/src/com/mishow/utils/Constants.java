package com.mishow.utils;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：所有常量公共类
 */
public class Constants {
	
	/** 微信 **/
	public static final String WX_APPID = "";
	
	public static final String WX_APPSECRET = "";
	
	/** 微博分享start **/
	public static final String WB_APPID = "1089057952";
	
	public static final String WB_APPSECRET = "e97f45cd5aeaabd6ceae8558a413dfc8";
	
	public static final String WB_URL = "http://weibo.com";
	
	/** 微博分享end **/
	
	/**
	 * 标签类型
	 */
	public static final String TAG_TYPE = "tag_type";
	/**
	 * 每个页面的标题
	 */
	public static final String TAG_TITLE = "tag_title";
	/**
	 * 标签是否单选或多选
	 */
	public static final String TAG_IS_RADIO = "tag_is_radio";
	/**
	 * 被选择的标签
	 */
	public static final String SELECTED_TAGS = "selected_tags";
	//用于设置各个第三方平台的appkey
	
	/**
	 * 向服务端请求的图片尺寸
	 */
	public static final String[] DEVICE_RESOLUTION = new String[]{"240480", "480800", "640960", "7201280", "10801920"};
	
	
	/**
	 * 选择拍照操作
	 **/
	public static final int REQUEST_CODE_TAKE_PHOTO = 0x1;
	/**
	 * 选择从手机相册操作
	 **/
	public static final int REQUEST_CODE_PICK_PHOTO = 0x2;
	
	/**
	 * 职业类型标签
	 */
	public static final int REQUEST_TAG_PROFESSION_TYPE = 0x3;
	
	/**
	 * 跳转城市列表code
	 */
	public static final int CITYLIST_CODE = 0x4;
	public static final String CITY_NAME  = "cityName";
	public static final String CITY_CODE  = "cityCode";
	
	/** 通告地址选择 **/
	public static final int ANNUNCIATE_LOCATION_REQUEST_CODE = 0x5;
	public static final String ANNUNCIATE_LOCATION_EXTRA_NAME = "location_name";
	public static final String ANNUNCIATE_LOCATION_EXTRA_CODE = "location_CODE";
	
	/** 才艺类型 **/
	public static final int   SKILLTYPE_REQUEST_CODE = 0x6;
	public static final String SKILLTYPE_EXTRA_NAME = "skill_type_name";
	
	/** 通告ID **/
	public static final String  ANNOUN_ID =  "announcement_Id";
	
	/** 通告名称 **/
	public static final String  ANNOUN_NAME =  "announcement_Name";
	
	/** 星秀ID **/
	public static final String  STAR_ID =  "star_Id";
	
	/** 全部 **/
	public static final int FROM_STATUS_ALL = 0;
	/** 招募中 **/
	public static final int FROM_STATUS_RECRUIT = 1;
	/** 已过期 **/
	public static final int FROM_STATUS_PAST = 2;
	/** 已关闭 **/
	public static final int FROM_STATUS_CLOSE = 3;
	
	/** 发布选择动态或精选图片 **/
	public static final String IMAGE_RELEASE_TYPE = "release_type";
	/** 动态发布CODE **/
	//public static final int IMAGE_TYPE_DYNAMIC_REQUEST_CODE = 0x12;
	/** 动态发布 **/
	public static final int IMAGE_RELEASE_TYPE_DYNAMIC = 0x7;
	/** 精选发布CODE **/
	//public static final int IMAGE_TYPE_RELEASE_REQUEST_CODE = 0x13;
	/** 精选发布 **/
	public static final int IMAGE_RELEASE_TYPE_SELECT = 0x8;
	/** 进入图片预览页面 **/
	//public static final int GALLERY_REQEUST_CODE = 0x14;
	/** 登录请求 **/
	public static final int LOGIN_REQUEST_CODE = 0x9;
	
	/** 首页第一个tab索引  **/
	public static final int MAIN_FIRST_TAB = 0;
	/** 首页第二个tab索引 **/
    public static final int MAIN_STAR_TAB = 1;
    /** 首页第三个tab索引 **/
    public static final int MAIN_MSG_TAB = 2;
    /** 首页第四个tab索引 **/
    public static final int MAIN_MY_TAB = 3;
    /** 登录返回 **/
    public static final String LOGIN_RESPONSE_NAME = "login_response_name";
    
    
    /**
     * 主页-首页
     **/
    public final static int TAG_INDEX_FRAGMENT = 0x11;
    /** 跳转到登录 **/
    public static final int REQUESTCODE_LOGIN = 1;
	
	
}
