package com.mishow.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：设备分辨率工具类
 */
public class DeviceUtil {

	/**
	 * 获取屏幕的像素密度 <br/>
	 * 
	 * @param [cx]-[上下文对象]
	 *            <br/>
	 * @return 屏幕像素密度
	 */
	public static float getDeviceDisplayDensity(Context cx) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = cx.getApplicationContext().getResources().getDisplayMetrics();
		return dm.density;
	}

	/**
	 * 获取屏幕的宽度 <br/>
	 * 
	 * @param [cx]-[上下文对象]
	 *            <br/>
	 * @return 屏幕宽度（单位px）
	 */
	public static float getDeviceWidth(Context cx) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = cx.getApplicationContext().getResources().getDisplayMetrics();
		return dm.widthPixels;
	}

	/**
	 * 获取屏幕的高度 <br/>
	 * 
	 * @param [cx]-[上下文对象]
	 *            <br/>
	 * @return 屏幕高度（单位px）
	 */
	public static float getDeviceHight(Context cx) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = cx.getApplicationContext().getResources().getDisplayMetrics();
		return dm.heightPixels;
	}

	/**
	 * 获取屏幕的像素密度 ,一般情况下与density相同，字体大小转换时会有相应的变化，所以一般转换字体大小使用scaledDensity<br/>
	 * 
	 * @param [cx]-[上下文对象]
	 *            <br/>
	 * @return 屏幕宽度（单位px）
	 */
	public static float getDeviceScaledDensity(Context cx) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = cx.getApplicationContext().getResources().getDisplayMetrics();
		return dm.scaledDensity;
	}

	/** 
     * 将px值转换为sp值，保证文字大小不变 
     *  
     * @param pxValue 
     * @param fontScale 
     *            （DisplayMetrics类中属性scaledDensity） 
     * @return 
     */  
    public static int px2sp(Context context, float pxValue) {  
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;  
        return (int) (pxValue / fontScale + 0.5f);  
    } 
	/** 
     * 将sp值转换为px值，保证文字大小不变 
     *  
     * @param spValue 
     * @param fontScale 
     *            （DisplayMetrics类中属性scaledDensity） 
     * @return 
     */  
    public static int sp2px(Context context, float spValue) {  
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;  
        return (int) (spValue * fontScale + 0.5f);  
    }

	/**
	 * 将dp值转换为px值，保证文字大小不变
	 * 
	 * @param cx
	 * @param dipValue
	 * @return
	 */
	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	/**
	 * 将px值转换为dp值，保证文字大小不变
	 * 
	 * @param cx
	 * @param pxValue
	 * @return
	 */
	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}
	/**
	 * 设置margin值
	 * @param v
	 * @param l
	 * @param t
	 * @param r
	 * @param b
	 */
	public static void setMargins (View v, int l, int t, int r, int b) {  
	    if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {  
	        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();  
	        p.setMargins(l, t, r, b);  
	        v.requestLayout();  
	    }  
	} 

}
