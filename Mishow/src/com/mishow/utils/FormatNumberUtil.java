package com.mishow.utils;

import java.text.NumberFormat;

import android.text.Html;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：格式化数字工具类
 */
public class FormatNumberUtil {

	/** 普通数字类型 **/
	public static final int TYPE_NUMBER = 0;
	/** 货币类型 **/
    public static final int TYPE_CURRENCY = 1;

    private static final int DEFAULT_FRACTION_DIGITS = 2;

    private NumberFormat nf;
    private int currentType;

    private FormatNumberUtil(int type) {
        currentType = type;
//        if (type == TYPE_NUMBER) {
            nf = NumberFormat.getInstance();
//        } else {
//            nf = NumberFormat.getCurrencyInstance(Locale.CHINA);
//        }
        nf.setMinimumFractionDigits(DEFAULT_FRACTION_DIGITS);
        nf.setMaximumFractionDigits(DEFAULT_FRACTION_DIGITS);
    }

    public static FormatNumberUtil getInstance(int type) {
        return new FormatNumberUtil(type);
    }

    public void setFractionDigits(int value) {
        nf.setMinimumFractionDigits(value);
        nf.setMaximumFractionDigits(value);
    }

    /**
     * 格式化数字，精确到小数点后两位数
     *
     * @param number
     * @return
     */
    public String format(double number) {
        if (currentType == TYPE_NUMBER) {
            return nf.format(number);
        }
        return String.format(Html.fromHtml("&#165;%1$s").toString(), nf.format(number));
    }

}
