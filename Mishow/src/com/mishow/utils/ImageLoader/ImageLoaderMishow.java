package com.mishow.utils.ImageLoader;

import android.content.Context;
import android.widget.ImageView;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：图片加载器功能接口
 * 添加或者替换新的图片加载器实现该接口即可
 */
public interface ImageLoaderMishow {
	
	/**
	 * 初始化ImageLoader
	 * @param context
	 */
	void init(Context context);
	/**
	 * 显示 image
	 * @param imageUrl 
	 * @param imageView
	 * @param defaultImage
	 */
	void displayImage(String imageUrl,ImageView imageView,int defaultImage);
}
