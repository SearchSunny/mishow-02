package com.mishow.utils.ImageLoader;

import android.content.Context;
import android.widget.ImageView;
/**
 * 作者：wei.miao <br/>
 * 描述：图片加载代理类，所有的图片操作都通过该代理类实现。
 * 如果要改变图片加载框架，只需要在该类里面替换相应的图片加载框架即可 
 * 客户端所有引用的图片操作都不需要修改
 */
public class ImageLoaderProxy implements ImageLoaderMishow {

	/**
	 * 代理对象
	 */
	private ImageLoaderMishow imageLoader;
	
	private static ImageLoaderProxy imageLoaderProxy;
	/**
	 *  获取image代理对象
	 * @return
	 */
	public static ImageLoaderProxy getInstance(Context context){
		if (imageLoaderProxy == null) {
			
			imageLoaderProxy = new ImageLoaderProxy(context);
		}
		return imageLoaderProxy;
	}
	
	public ImageLoaderProxy(Context context){
		imageLoader = new PicassoImageLoader();
		init(context);
		
	}
	
	@Override
	public void init(Context context) {
		
		imageLoader.init(context);
	}
	/**
	 * 显示image
	 */
	@Override
	public void displayImage(String imageUrl, ImageView imageView,
			int defaultImage) {
		
		imageLoader.displayImage(imageUrl, imageView, defaultImage);
	}

}
