package com.mishow.utils.ImageLoader;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
/**
 * 作者：wei.miao <br/>
 * 描述：Picasso图片加载框架
 */
public class PicassoImageLoader implements ImageLoaderMishow{

	private Context mContext;
	@Override
	public void init(Context context) {
		mContext = context;
	}

	@Override
	public void displayImage(String imageUrl, ImageView imageView,int defaultImage) {
		
		//Placeholders-空白或者错误占位图片：picasso提供了两种占位图片，未加载完成placeholder或者加载发生错误error的时需要一张图片作为提示。
		Picasso.with(mContext)
		.load(imageUrl)
		.fit() //减少内存开销
		.placeholder(defaultImage)
		//.transform(new CropSquareTransformation())
		.centerCrop()
		//.resize(768, 432) //指定加载的图片大小的
		.into(imageView);
	}

	/**
	 * 
	 * 作者：wei.miao <br/>
	 * 描述：自定义转换图片以适应布局大小并减少内存占用
	 */
	public class CropSquareTransformation implements Transformation {
		  @Override public Bitmap transform(Bitmap source) {
		    int size = Math.min(source.getWidth(), source.getHeight());
		    int x = (source.getWidth() - size) / 2;
		    int y = (source.getHeight() - size) / 2;
		    Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
		    if (result != source) {
		      source.recycle();
		    }
		    return result;
		  }
		  @Override public String key() { return "square()"; }
		}
}
