package com.mishow.utils.ImageLoader;

import java.io.File;
import java.io.IOException;

import com.mishow.R;
import com.mishow.utils.FileUtil;
import com.nostra13.universalimageloader.cache.disc.impl.ext.LruDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：Universalimageloader图片加载框架
 */
public class Universalimageloader implements ImageLoaderMishow{
	
	private Context mContext;
	/**
	 * //ImageLoader对象
	 */
	private ImageLoader imageLoader = ImageLoader.getInstance();

	@Override
	public void init(Context context) {
		mContext = context;
		initImageLoader(mContext);
	}

	@Override
	public void displayImage(String imageUrl, ImageView imageView,
			int defaultImage) {
		
		imageLoader.displayImage(imageUrl, imageView, initDisplayOptions(true, defaultImage));
	}
	
	/**
	 * 初始化ImageLoader <br/>
	 * 
	 * @param [参数1]-[参数1说明] <br/>
	 * @param [参数2]-[参数2说明] <br/>
	 */
	private void initImageLoader(Context context) {
		if (!imageLoader.isInited()) {
			
			// 获取本地缓存的目录，该目录在SDCard的根目录下
			File cacheDir = new File(FileUtil.getImageCachePath());

			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
					context);
			// 设置线程数量
			// 设定线程等级比普通低一点
			builder.threadPriority(Thread.NORM_PRIORITY - 1);
			builder.tasksProcessingOrder(QueueProcessingType.LIFO);
			builder.memoryCache(new LruMemoryCache(2 * 1024 * 1024));
			builder.threadPoolSize(5);
			builder.denyCacheImageMultipleSizesInMemory();
			// 设定缓存的SDcard目录，
			try {
				builder.diskCache(new LruDiskCache(cacheDir, new HashCodeFileNameGenerator(), 50 * 1024 * 1024));
			} catch (IOException e) {
				e.printStackTrace();
			}
			//设定sd卡缓存大小
			//builder.diskCacheSize(50 * 1024);
			// 设定网络连接超时 timeout: 8s 读取网络连接超时read timeout: 15s
			builder.imageDownloader(new BaseImageDownloader(context, 8000, 30000));
			// 设置ImageLoader的配置参数
			builder.defaultDisplayImageOptions(initDisplayOptions(true,R.drawable.app_icon));

			// 初始化ImageLoader
			ImageLoader.getInstance().init(builder.build());
		}
		
	}

	/**
	 * 返回默认的参数配置
	 * 
	 * @param isDefaultShow
	 *            true：显示默认的加载图片 false：不显示默认的加载图片
	 * @return
	 */
	public static DisplayImageOptions initDisplayOptions(boolean isShowDefault,
			int resId) {
		DisplayImageOptions.Builder displayImageOptionsBuilder = new DisplayImageOptions.Builder();
		// 设置图片缩放方式
		// EXACTLY: 图像将完全按比例缩小的目标大小
		// EXACTLY_STRETCHED: 图片会缩放到目标大小
		// IN_SAMPLE_INT: 图像将被二次采样的整数倍
		// IN_SAMPLE_POWER_OF_2: 图片将降低2倍，直到下一减少步骤，使图像更小的目标大小
		// NONE: 图片不会调整
		displayImageOptionsBuilder.imageScaleType(ImageScaleType.EXACTLY);
		if (isShowDefault) {
			// 默认显示的图片
			displayImageOptionsBuilder.showImageOnLoading(resId);
			// 地址为空的默认显示图片
			displayImageOptionsBuilder.showImageForEmptyUri(resId);
			// 加载失败的显示图片
			displayImageOptionsBuilder.showImageOnFail(resId);
		}
		// 开启内存缓存
		displayImageOptionsBuilder.cacheInMemory(true);
		// 开启SDCard缓存
		displayImageOptionsBuilder.cacheOnDisk(true);
		displayImageOptionsBuilder.considerExifParams(true);
		return displayImageOptionsBuilder.build();
	}
	/**
	 * 返回默认的参数配置
	 * 
	 * @param isDefaultShow
	 *            true：显示默认的加载图片 false：不显示默认的加载图片
	 * @return
	 */
	public static DisplayImageOptions initDisplayOptions(int resId) {
		DisplayImageOptions.Builder displayImageOptionsBuilder = new DisplayImageOptions.Builder();
		displayImageOptionsBuilder.bitmapConfig(Bitmap.Config.ARGB_8888);
		// 设置图片缩放方式
		// EXACTLY: 图像将完全按比例缩小的目标大小
		// EXACTLY_STRETCHED: 图片会缩放到目标大小
		// IN_SAMPLE_INT: 图像将被二次采样的整数倍
		// IN_SAMPLE_POWER_OF_2: 图片将降低2倍，直到下一减少步骤，使图像更小的目标大小
		// NONE: 图片不会调整
		displayImageOptionsBuilder.imageScaleType(ImageScaleType.IN_SAMPLE_INT);
		// 默认显示的图片
		displayImageOptionsBuilder.showImageOnLoading(resId);
		// 地址为空的默认显示图片
		displayImageOptionsBuilder.showImageForEmptyUri(resId);
		// 加载失败的显示图片
		displayImageOptionsBuilder.showImageOnFail(resId);
		// 开启内存缓存
		displayImageOptionsBuilder.cacheInMemory(true);
		// 开启SDCard缓存
		displayImageOptionsBuilder.cacheOnDisk(true);
		displayImageOptionsBuilder.considerExifParams(true);
		return displayImageOptionsBuilder.build();
	}
	
	/**
	 * @param uri 图片下载地址
	 * @param imageView 图片容器
	 * @param options 下载的参数配置
	 */
	public void loadImage(String uri, ImageView imageView,
			DisplayImageOptions options) {
//		if (!AndroidUtils.isWifi(context) && settingPf.getPerferenceIswifi()) {//将uri置空，显示默认图片
//			uri = null;
//		}
		imageLoader.displayImage(uri, imageView, options);
	}
	
	/**
	 * 
	 * @param uri 图片下载地址
	 * @param imageView 图片容器
	 * @param defaultImage 默认图片
	 */
	public void loadImage(String uri, ImageView imageView,
			int defaultImage) {
//		if (!AndroidUtils.isWifi(context) && settingPf.getPerferenceIswifi()) {//将uri置空，显示默认图片
//			uri = null;
//		}
		imageLoader.displayImage(uri, imageView, initDisplayOptions(true, defaultImage));
	}
	
	/**
	 * @param uri 图片下载地址
	 * @param imageView 图片容器
	 */
	public void loadImage(String uri, ImageView imageView) {
//		if (!AndroidUtils.isWifi(context) && settingPf.getPerferenceIswifi()) {//将uri置空，显示默认图片
//			uri = null;
//		}
		imageLoader.displayImage(uri, imageView);
	}
	
	/**
	 * @param uri 图片下载地址
	 * @param imageView 图片容器
	 */
	public void loadImage(String uri, ImageView imageView, boolean showDefault) {
//		if (!AndroidUtils.isWifi(context) && settingPf.getPerferenceIswifi()) {//将uri置空，显示默认图片
//			uri = null;
//		}
		imageLoader.displayImage(uri, imageView, initDisplayOptions(showDefault, 0));
	}
	
	/**
	 * 
	 * @param uri 图片下载地址
	 * @param imageView 图片容器
	 * @param defaultImage 默认图片
	 */
	public void loadImage(String uri, ImageView imageView,
			int defaultImage, ImageLoadingListener listener) {
//		if (!AndroidUtils.isWifi(context) && settingPf.getPerferenceIswifi()) {//将uri置空，显示默认图片
//			uri = null;
//		}
		imageLoader.displayImage(uri, imageView, initDisplayOptions(true, defaultImage), listener);
	}

	
}
