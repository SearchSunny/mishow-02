package com.mishow.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：通知的优化
 */
public class NotificationCenter {

	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mBuilder;

	private NotificationCenter() {
	}

	private static NotificationCenter instance;

	public static NotificationCenter getInstance() {
		if (instance == null) {
			instance = new NotificationCenter();
		}
		return instance;
	}

	public void showNotification(Context context, String title, String content,
			PendingIntent intent, int id, int icon) {
		if (mNotificationManager == null) {
			mNotificationManager = (NotificationManager) context
					.getSystemService(Activity.NOTIFICATION_SERVICE);
		}
		if (mBuilder == null) {
			mBuilder = new NotificationCompat.Builder(context)
					.setSmallIcon(icon)
					.setDefaults(
							Notification.DEFAULT_SOUND
									| Notification.DEFAULT_VIBRATE)
					.setOngoing(false).setAutoCancel(true);
		}
		mBuilder.setContentTitle(title).setContentText(content)
				.setTicker(content).setContentIntent(intent);
		mNotificationManager.cancel(id);
		mNotificationManager.notify(id, mBuilder.build());
	}

	/**
	 * 
	 * @param context
	 * @param title 通知的标题
	 * @param content 通知的内容
	 * @param intent
	 * @param id
	 * @param smallIcon
	 * @param largeIcon
	 */
	public void showNotification(Context context, String title, String content,
			PendingIntent intent, int id, int smallIcon, int largeIcon) {
		if (mNotificationManager == null) {
			mNotificationManager = (NotificationManager) context
					.getSystemService(Activity.NOTIFICATION_SERVICE);
		}
		if (mBuilder == null) {
			mBuilder = new NotificationCompat.Builder(context);
		}
		mBuilder.setSmallIcon(smallIcon)
				.setLargeIcon(
						BitmapFactory.decodeResource(context.getResources(),
								largeIcon))
				.setDefaults(
						Notification.DEFAULT_SOUND
								| Notification.DEFAULT_VIBRATE)
				.setOngoing(false).setAutoCancel(true).setContentTitle(title)
				.setContentText(content).setTicker(content)
				.setContentIntent(intent).setShowWhen(true)
				.setWhen(System.currentTimeMillis());
		mNotificationManager.cancel(id);
		mNotificationManager.notify(id, mBuilder.build());
	}
}
