package com.mishow.utils;

import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.mishow.activity.MainActivity;

import android.content.Context;
import android.content.Intent;

/**
 * 作者：wei.miao <br/>
 * 描述：打开界面统一入口，用于界面跳转
 */
public class OpenActivityManager {

	/**
     * 首页
     **/
    public final static String ID_HOME_INDEX = "code_101";

    /** 药品id **/
    private final static String STRING_GID = "gId";
    /** 活动id **/
    private final static String ACTIVITY_ID = "activityId";
    /** web页面标题 **/
    private final static String WEB_TITLE = "title";
    /** 配置文件名称 **/
    public final static String FILE_NAME = "activity_setting.properties";
    /** 配置文件对象 **/
    private Properties prop;
    
    private Context mContext;
    
    public OpenActivityManager(Context mContext) {
        this.mContext = mContext;
        prop = loadConfig(mContext, FILE_NAME);
    }
    
    /**
     * 读取配置文件
     */
    public static Properties loadConfig(Context context, String fileName) {
        Properties properties = new Properties();
        try {
            InputStream is = context.getAssets().open(fileName);
            properties.load(is);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return properties;
    }
    
    
    
    /**
     * @param adPgCode
     * @param adPageParam
     * @return
     */
    private Intent getOpenIntent(Context mContext, String adPgCode, String adPageParam) {
        Intent intent = new Intent();
        intent.setClassName(mContext, getPropValue(adPgCode));
        initIntentExtras(intent, adPgCode, adPageParam);
        return intent;
    }
    
    /**
     * 获取配置文件数据
     *
     * @param adPgCode
     * @return
     */
    private String getPropValue(String adPgCode) {
        String proValue = prop.getProperty(adPgCode, MainActivity.class.getName());
        return proValue;
    }
    
    /**
     * 设置传参
     * @param intent
     * @param adPgCode
     * @param adPageParam
     */
    public void initIntentExtras(Intent intent, String adPgCode, String adPageParam) {
        if (adPgCode != null && !adPgCode.startsWith("code_")) {
            adPgCode = "code_" + adPgCode;
        }
        switch (adPgCode) {
            case ID_HOME_INDEX:// 首页
                intent.putExtra("tag", Constants.TAG_INDEX_FRAGMENT);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                break;
            default:
                putExtras(intent, adPageParam);
                break;
        }
    }
    /**
     * 参数封装
     * @param intent
     * @param adPageParam
     */
    public void putExtras(Intent intent, String adPageParam) {
        // 参数传递
        Set<Entry<String, Object>> sets = getParams(adPageParam);
        try {
            if (sets != null && sets.size() > 0) {
                for (Entry<String, Object> entry : sets) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    if (STRING_GID.equals(key) || "serviceId".equals(key) || "dialogId".equals(key)
                            || "obId".equals(key) || "suId".equals(key) || "oId".equals(key)
                            || "orderStatus".equals(key) || "mvId".equals(key)
                            || ACTIVITY_ID.equals(key) || "orderId".equals(key) || "urlType".equals(key)
                            || "drugId".equals(key) || "merchantId".equals(key)) {// value转换
                        intent.putExtra(key, Integer.parseInt(String.valueOf(value)));
                    } else if (WEB_TITLE.equals(key)) {// 标题转换
                        intent.putExtra("titleStr", String.valueOf(value));
                    } else if ("hasTitle".equals(key)) {// 是否隐藏标题栏
                        if ("1".equals(String.valueOf(value)) || "true".equals(String.valueOf(value))) {
                            intent.putExtra("hideNav", false);
                        } else {
                            intent.putExtra("hideNav", true);
                        }
                    } else {
                        intent.putExtra(key, String.valueOf(value));
                    }
                    //标记活动推送、指定跳转、搜索结果
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    }
    
    /**
     * 拆解传参
     *
     * @param jsonStr
     * @return
     */
    public static Set<Entry<String, Object>> getParams(String jsonStr) {
        if (jsonStr == null || jsonStr.length() == 0) {
            return null;
        }
        JSONObject jsonObject;
        try {
            jsonObject = JSONObject.parseObject(jsonStr);
            return jsonObject.entrySet();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 打开界面
     *
     * @param mContext
     * @param adPgCode    Activity对应的id
     * @param adPageParam 界面传参
     */
    public void openActivity(Context mContext, String adPgCode, String adPageParam) {

        mContext.startActivity(getOpenIntent(mContext, adPgCode, adPageParam));

    }
	
}
