package com.mishow.utils;


public class StringUtil {

	/**
	 * 处理字符串中包含","
	 * @param str
	 * @return
	 */
	 public static String replaceString(String str) {
		 String strReplace = "";
		 if (str != null && !str.equals("")) {
			 if (str.contains(",")) {
				 strReplace = str.replace(",", " ");
			}else{
				strReplace = str;
			}
		}
		 return strReplace;
	 }
}
