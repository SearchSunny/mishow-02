package com.mishow.utils;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：文本颜色处理
 */
public class TextColorUtil {

	
	/*Spannable.SPAN_EXCLUSIVE_EXCLUSIVE：前后都不包括，即在指定范围的前面和后面插入新字符都不会应用新样式 
	Spannable.SPAN_EXCLUSIVE_INCLUSIVE	：前面不包括，后面包括。即仅在范围字符的后面插入新字符时会应用新样式
	Spannable.SPAN_INCLUSIVE_EXCLUSIVE	：前面包括，后面不包括。
	Spannable.SPAN_INCLUSIVE_INCLUSIVE	：前后都包括。*/
	
	/**
	 * 获取标颜色的字符串
	 * @param origin
	 * @param boldWords
	 * @return
	 */
    public static SpannableStringBuilder getMarkStr(int color,String origin, String boldWords) {
        SpannableStringBuilder ss = new SpannableStringBuilder(origin);
        int start = origin.indexOf(boldWords);
        ss.setSpan(new ForegroundColorSpan(color), start, start + boldWords.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return ss;
    }
    
    /**
     * 特殊处理-组合使用
     * @param boldWords
     * @param firstName
     * @param secondName
     * @return
     */
    public static SpannableStringBuilder getMarkStr(int color,String boldWords,String firstName,String secondName) {
        
       SpannableStringBuilder spannable=new SpannableStringBuilder(boldWords);  
       //再构造一个改变字体颜色的Span  
       ForegroundColorSpan spanfirst = new ForegroundColorSpan(color);
       
       ForegroundColorSpan spanSecond = new ForegroundColorSpan(color);
       
       int indexContext = boldWords.indexOf("回复");
       int indexSym = boldWords.indexOf("：");
       //将这个Span应用于指定范围的字体  
       spannable.setSpan(spanfirst, 0, firstName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
       spannable.setSpan(spanSecond, indexContext+2, indexSym, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
      //设置给EditText显示出来  
      
       return spannable;
    }
    
}
