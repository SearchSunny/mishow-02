package com.mishow.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import com.mishow.log.MyLog;

@SuppressLint("SimpleDateFormat")
public class TimeUtil {
	
	/** 60分钟 */
	public static final int TIME = 60;
	/**24小时 */
	public static final int TIME24 = TIME * 24;
	/** 48小时 */
	public static final int TIME48 = TIME * 48;
	
	private static SimpleDateFormat currentFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static String currentTime = currentFormat.format(new Date());
	
	/**
	 * 处理时间戳
	 * @param time
	 * @return
	 */
	public static String proTime(String time,boolean isDisplayDate){
		
		StringBuilder sBuilder = new StringBuilder(time);
		String strdel = sBuilder.delete(sBuilder.lastIndexOf("."),
				sBuilder.lastIndexOf(".") + 2).toString();

		MyLog.e("MV", "strdel" + strdel);
		if (isDisplayDate) {
			
			SimpleDateFormat proFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date parseTime = proFormat.parse(strdel);
				String tempTime = proFormat.format(parseTime);

				return tempTime;
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
		}
		
		return strdel;
	}

	/**
	 * 日期对比
	 * @param time1 消息创建时间
	 * @param time2 当前时间
	 * @return
	 */
	public static long timeContrast(String time1, String time2) {
		try {

			String strTime = proTime(time1,false);
			// 时间对比
			SimpleDateFormat objFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			Date d1 = objFormat.parse(strTime);
			Date d2 = objFormat.parse(time2);
			long d3 = d2.getTime() - d1.getTime();
			long abs = Math.abs((d3 / (1000 * 60)));
			long minute = abs;
			return minute;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 处理更新时间
	 * @param createtime
	 * @return
	 */
	public static String update_time(String createtime){
		String update_time = "";
		long messageTime = TimeUtil.timeContrast(createtime,currentTime);
		if (messageTime < TimeUtil.TIME) {
			
			update_time = messageTime+"分钟前发布";
			
		}else if(messageTime > TimeUtil.TIME && messageTime < TimeUtil.TIME24){
			
			update_time = (messageTime/TimeUtil.TIME)+"小时前发布";
			
		}else if(messageTime > TimeUtil.TIME24 && messageTime < TimeUtil.TIME48){
			
			update_time = "昨天发布";
			
		}else if(messageTime > TimeUtil.TIME48){
			
			update_time = TimeUtil.proTime(createtime,true)+"发布";
		}
		return update_time;
	}
	
}
