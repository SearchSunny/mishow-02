package com.mishow.utils;
import android.content.Context;
import android.widget.Toast;

public class ToastUtil {

	public static void show(final Context ctx, final String message) {
		Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
	}

	public static void showLong(final Context ctx, final String message) {
		Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
	}
}
