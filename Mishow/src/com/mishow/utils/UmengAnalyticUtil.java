package com.mishow.utils;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.umeng.analytics.MobclickAgent;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：友盟统计：自定义事件
 */
public class UmengAnalyticUtil {



	/**
	 * 统计发生次数 <br/>
	 * @param [context]-[Context] <br/>
	 * @param [id]-[为自定义事件ID] <br/>
	 */
	public static void onEventTimes(Context context, String id) {
		MobclickAgent.onEvent(context, id);
	}
	
	/**
	 * 统计点击行为各属性被触发的次数 <br/>
	 * @param [context]-[Context] <br/>
	 * @param [id]-[为自定义事件ID] <br/>
	 * @param [map]-[为当前事件的属性和取值（Key-Value键值对）] <br/>
	 */
	public static void onEvent(Context context, String id, HashMap<String,String> map) {
		MobclickAgent.onEvent(context, id, map);
	}
	
	/**
	 * 统计一个数值类型的连续变量（该变量必须为整数），用户每次触发的数值的分布情况，如事件持续时间、每次付款金额等， <br/>
	 * @param [id]-[为自定义事件ID] <br/>
	 * @param [map]-[为当前事件的属性和取值] <br/>
	 * @param [du]-[为当前事件的数值，取值范围是-2,147,483,648 到 +2,147,483,647 之间的有符号整数，即int 32整型，如果du数据值超过该范围，会造成数据丢包，影响数据统计的准确性。] <br/>
	 */
	public static void onEventValue(Context context, String id,
			Map<String, String> map, int du) {
		MobclickAgent.onEventValue(context, id, map, du);
	}

}
