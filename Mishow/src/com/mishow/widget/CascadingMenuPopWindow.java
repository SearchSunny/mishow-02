package com.mishow.widget;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;

import com.mishow.R;
import com.mishow.bean.Area;
import com.mishow.inteface.CascadingMenuViewOnSelectListener;
import com.mishow.log.MyLog;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：提供省份级联popupWindow
 */
public class CascadingMenuPopWindow extends PopupWindow{

	private Context mContext;
	private CascadingMenuView cascadingMenuView;
	private ArrayList<Area> areas=null;
    /**
     * 提供给外的接口
     */
	private CascadingMenuViewOnSelectListener menuViewOnSelectListener;
	
	public void setMenuItems(ArrayList<Area> areas) {
		this.areas = areas;
	}
	public void setMenuViewOnSelectListener(
			CascadingMenuViewOnSelectListener menuViewOnSelectListener) {
		this.menuViewOnSelectListener = menuViewOnSelectListener;
	}
     
	/**
	 * 提供省份级联popupWindow
	 * @param context
	 * @param list 级联数据
	 */
	public CascadingMenuPopWindow(Context context,ArrayList<Area> list) {
		super(context);
		this.mContext=context;
		this.areas=list;
		init();
	}
	
	public void init(){
		//实例化级联菜单
		cascadingMenuView=new CascadingMenuView(mContext,areas);
		setContentView(cascadingMenuView);
		setWidth(LayoutParams.MATCH_PARENT);
		setHeight(LayoutParams.MATCH_PARENT);
		setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(R.color.hot_nation_pop_bg)));
		setOutsideTouchable(true);
		setFocusable(true);
		cascadingMenuView.setOnTouchListener(onTouchListener);
		
		//设置回调接口
		cascadingMenuView.setCascadingMenuViewOnSelectListener(new MCascadingMenuViewOnSelectListener());
	}
	
	/**
	 * 作者：wei.miao <br/>
	 * 描述：级联菜单选择回调接口
	 */
	class MCascadingMenuViewOnSelectListener implements CascadingMenuViewOnSelectListener{

		@Override
		public void getValue(Area menuItem) {
			if(menuViewOnSelectListener!=null){
				menuViewOnSelectListener.getValue(menuItem);
				//dismiss();
			}
		}
		
	}
	/**
	 * mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
	 */
	private OnTouchListener onTouchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View mMenuView, MotionEvent event) {
			//mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框  
			 int height = mMenuView.findViewById(R.id.listView).getTop();  
            MyLog.d("height="+height);
            int y=(int) event.getY(); 
            MyLog.d("y="+y);
            /*if(event.getAction()==MotionEvent.ACTION_UP){  
                if(y<height){  
                    dismiss();  
                }  
            } */    
            dismiss();
            return true; 
		}
	};
}
