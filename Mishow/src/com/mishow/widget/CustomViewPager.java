package com.mishow.widget;

import java.util.List;

import com.mishow.R;
import com.mishow.adapter.MyViewPagerAdapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：自定义ViewPager
 */
public class CustomViewPager extends LinearLayout{

	private Context mContext;
	
	/** 动画图片 **/
	private ImageView image_cursor;
	
	/** 页卡内容 * */
	private ViewPager viewPager;
	
	/**
	 * 动画图片宽度
	 */
	private int bmpw;
	/**
	 * 动画图片偏移量
	 */
	private int offset = 0; //动画图片偏移量
	/**
	 * 当前页卡编号
	 */
	private int currIndex = 0;//当前页卡编号
	
	public CustomViewPager(Context context) {
		super(context,null);
		this.mContext = context;
	}
	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
		initView();
	}
	
	/** 初始化 **/
	private void initView(){

		View view = LayoutInflater.from(mContext).inflate(R.layout.widget_viewpager, null);
		
		image_cursor = (ImageView)view.findViewById(R.id.image_cursor);
		viewPager = (ViewPager)view.findViewById(R.id.vPager);
		
		addView(view);
	}

	/**
	 * 设置viewpager属性
	 * @param views 要加载的view
	 * @param resId 滑动时的图片
	 */
	public void setAttribute(List<View> views,int resId){
		
		//获取图片的宽度
		bmpw = BitmapFactory.decodeResource(getResources(), resId).getWidth();
		
		DisplayMetrics dm = new DisplayMetrics();
		((Activity)mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
		int screenW = dm.widthPixels; //获取分辨率宽度
		offset = (screenW / views.size() - bmpw) /2;//计算偏移量
		
		Matrix matrix = new Matrix();
		matrix.postTranslate(offset, 0);
		image_cursor.setImageMatrix(matrix);//设置动画初始位置
		
		viewPager.setAdapter(new MyViewPagerAdapter(views));
		
		viewPager.setCurrentItem(0);
		viewPager.setOnPageChangeListener(new MyOnPageChangeListener());
	}
	
	public class MyOnPageChangeListener implements OnPageChangeListener{

		int one = offset * 2 + bmpw;//页卡1 --> 页卡2 偏移量
		int two = one * 2 ;//页卡1 --> 页卡3 偏移量
		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			
		}

		@Override
		public void onPageSelected(int arg0) {
			
			Animation animation = new TranslateAnimation(one * currIndex, one*arg0, 0, 0);
			currIndex = arg0;
			animation.setFillAfter(true);//True 图片停在动画结束位置
			animation.setDuration(300);
			image_cursor.startAnimation(animation);
			
			onGetCurrentItem.getCurrentItem(viewPager.getCurrentItem());
		}
	}
	
	/**
	 * 
	 * 设置ViewPager选择项
	 * @param currentItem
	 * 
	 */
	public void setCurrentItem(int currentItem){
		
		viewPager.setCurrentItem(currentItem);
	}
	
	/** 获取当前项 **/
	private OnGetCurrentItem onGetCurrentItem;
	
	
	public void setCurrentItemInterface(OnGetCurrentItem onGetCurrentItem){
		
		this.onGetCurrentItem = onGetCurrentItem;
	}
	
	public interface OnGetCurrentItem{
		
		void getCurrentItem(int currentItem);
	}

}
