package com.mishow.widget;

import com.mishow.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：数据为空时的布局
 */
public class EmptyDataLayout extends LinearLayout {

	private Context mContext;
	private ImageView emptyDataImage;
	private Button btn; // 按钮
	private TextView tv_line_1; // 第一行字
	private TextView tv_line_2; // 第二行字
	private String text1;
	private String text2;
	private int textResId1;
	private int textResId2;
	private int imageSrc;

	public EmptyDataLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.emptylayout);
		text1 = array.getString(R.styleable.emptylayout_text1);
		text2 = array.getString(R.styleable.emptylayout_text2);
		textResId1 = array.getResourceId(R.styleable.emptylayout_text1, -1);
		textResId2 = array.getResourceId(R.styleable.emptylayout_text2, -1);
		imageSrc = array.getResourceId(R.styleable.emptylayout_src, -1);
		array.recycle();
		initView();
		fillData();
	}

	private void initView() {
		LayoutInflater inflater = LayoutInflater.from(mContext);
		View view = inflater.inflate(R.layout.empty_data, null);
		emptyDataImage = (ImageView) view.findViewById(R.id.empty_data_image);
		btn = (Button) view.findViewById(R.id.btn);
		tv_line_1 = (TextView) view.findViewById(R.id.tv_line_1);
		tv_line_2 = (TextView) view.findViewById(R.id.tv_line_2);
		btn.setOnClickListener(new WidgetClickListener());
		setOrientation(VERTICAL);
		addView(view);
	}

	private void fillData() {
		if (!TextUtils.isEmpty(text1)) {
			tv_line_1.setText(text1);
		}
		if (textResId1 != -1) {
			tv_line_1.setText(textResId1);
		}
		if (!TextUtils.isEmpty(text2)) {
			tv_line_2.setText(text2);
		}
		if (textResId2 != -1) {
			tv_line_2.setText(textResId2);
		}
		if (imageSrc != -1) {
			emptyDataImage.setImageResource(imageSrc);
		}
	}

	public void setImageResource(int resId) {
		emptyDataImage.setImageResource(resId);
	}
	
	public void setText1Color(int color){
		tv_line_1.setTextColor(color);
	}

	class WidgetClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

		}

	}

}
