package com.mishow.widget;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mishow.R;
import com.mishow.adapter.HotAnnunciationAllAdapter;
import com.mishow.adapter.HotAnnunciationSmartAdapter;
import com.mishow.adapter.MenuItemAdapter;
import com.mishow.bean.Area;
import com.mishow.bean.MenuItemBean;
import com.mishow.inteface.CascadingMenuViewOnSelectListener;

/**
 * 热通告智能排序
 * 
 */
public class HotAnnunciateSmartMenuView extends LinearLayout {
	
	private static final String TAG = HotAnnunciateSmartMenuView.class.getSimpleName();
	// 三级菜单选择后触发的接口，即最终选择的内容
	private CascadingMenuViewOnSelectListener mOnSelectListener;
	
	/** 显示父数据列表 **/
	private MyGridView gridview_all_layout;
	
	/** 显示数据内容 **/
	private ArrayList<MenuItemBean> menuContents;
	/** 数据点击事件 **/
	private OnItemClickListener itemClickListener;
	/** 重置 **/
	private Button btn_all_restart;
	/** 确定 **/
	private Button btn_all_confirm;

	private Context mContext;
	
	private HotAnnunciationSmartAdapter smartAdapter;

	/**
	 * 实例化级联菜单
	 * @param context 上下文
	 */
	public HotAnnunciateSmartMenuView(Context context, ArrayList<MenuItemBean> menuList) {
		super(context);
		this.menuContents = menuList;
		this.mContext = context;
		init(context);
	}

	public HotAnnunciateSmartMenuView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
		init(context);
	}

	private void init(final Context context) {
		
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.pop_nation_all_listview, this, true);
		
		gridview_all_layout = (MyGridView)findViewById(R.id.gridview_all_layout);
		btn_all_restart = (Button)findViewById(R.id.btn_all_restart);
		btn_all_confirm = (Button)findViewById(R.id.btn_all_confirm);
		
		
		smartAdapter = new HotAnnunciationSmartAdapter(mContext, menuContents);
		gridview_all_layout.setAdapter(smartAdapter);
	}


	public void setCascadingMenuViewOnSelectListener(
			CascadingMenuViewOnSelectListener onSelectListener) {
		
		mOnSelectListener = onSelectListener;
	}
}
