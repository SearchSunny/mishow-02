package com.mishow.widget;

import android.app.Dialog;
import android.content.Context;

import com.mishow.R;

public class LoadingDialog extends Dialog{


    public LoadingDialog(Context context, int themeResId) {
        super(context, R.style.CustomProgressDialog);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private void init() {
        setContentView(R.layout.dialog_progress);
    }
}
