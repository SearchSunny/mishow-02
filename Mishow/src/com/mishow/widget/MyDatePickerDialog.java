package com.mishow.widget;

import android.app.DatePickerDialog;
import android.content.Context;

/**
 * 作者：wei.miao <br/>
 * 描述：继承DatePickerDialog重写生命周期方法onStop()
 * 解决在部分手机中点击框外区域出现回调方法onDateSet()
 */

public class MyDatePickerDialog extends DatePickerDialog {


    public MyDatePickerDialog(Context context, OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth) {
        super(context, listener, year, monthOfYear, dayOfMonth);
    }

    public MyDatePickerDialog(Context context, int themeResId, OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth) {
        super(context, themeResId, listener, year, monthOfYear, dayOfMonth);
    }

    @Override
    protected void onStop() {
        //super.onStop();
    }
}
