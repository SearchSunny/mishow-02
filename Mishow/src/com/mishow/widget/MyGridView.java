package com.mishow.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：自定义GridView,重新计算view大小
 */
public class MyGridView extends GridView {

	
	 public MyGridView(Context context) { 
	        super(context); 
	 }
   
	public MyGridView(Context context, AttributeSet attrs) {
		super(context, attrs);

	}
	
	public MyGridView(Context context, AttributeSet attrs, int defStyle) { 
		
        super(context, attrs, defStyle); 
    }   

	/**
	 * widthMeasureSpec\heightMeasureSpec\由ViewGroup中的layout_width，layout_height和padding以及View自身的layout_margin共同决定。
	 * 权值weight也是尤其需要考虑的因素，有它的存在情况可能会稍微复杂点
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		//heightMeasureSpec作说明。这个值由高32位和低16位组成，高32位保存的值叫specMode，可以通过如代码中所示的MeasureSpec.getMode()获取；
		//低16位为specSize，同样可以由MeasureSpec.getSize()获取
		MeasureSpec.getMode(widthMeasureSpec);
		
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
		
	}

}
