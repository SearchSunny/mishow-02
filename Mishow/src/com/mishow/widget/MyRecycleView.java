package com.mishow.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：自定义RecycleView,目前主要实现列表 水平显示
 */
public class MyRecycleView extends RecyclerView{
	
	public MyRecycleView(Context context) {
		super(context);
	}
	
	public MyRecycleView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyRecycleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	

}
