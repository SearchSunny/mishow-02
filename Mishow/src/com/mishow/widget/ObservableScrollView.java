package com.mishow.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：监听ScrollView的滑动
 */
public class ObservableScrollView extends ScrollView{

	private int downX;  
    private int downY;  
    private int mTouchSlop; 
	/**
	 * 描述：自定义监听ScrollView的滑动
	 */
	public interface ScrollViewListener {
		void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);
	}

	private ScrollViewListener scrollViewListener = null;

	public ObservableScrollView(Context context) {
		super(context);
	}

	public ObservableScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ObservableScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setScrollViewListener(ScrollViewListener scrollViewListener) {
		this.scrollViewListener = scrollViewListener;
	}

	@Override
	protected void onScrollChanged(int x, int y, int oldx, int oldy) {
		super.onScrollChanged(x, y, oldx, oldy);
		if (scrollViewListener != null) {
			scrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
		}
	}

}
