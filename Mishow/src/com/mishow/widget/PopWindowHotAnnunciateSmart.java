package com.mishow.widget;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils.TruncateAt;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.mishow.R;
import com.mishow.bean.Area;
import com.mishow.bean.MenuItemBean;
import com.mishow.log.MyLog;
import com.mishow.utils.DeviceUtil;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：热通告全部排序弹出窗口
 */
public class PopWindowHotAnnunciateSmart extends PopupWindow{
	
	/** Context对象 **/
	private Context mContext;
	
	/** 显示数据内容 **/
	private ArrayList<MenuItemBean> menuContents;
	/** 数据点击事件 **/
	private OnItemClickListener itemClickListener;
	
	private  HotAnnunciateSmartMenuView smartMenuView;

	public PopWindowHotAnnunciateSmart(Context mContext, ArrayList<MenuItemBean> menuContents) {
		this.mContext = mContext;
		this.menuContents = menuContents;
		initView();
	}

	/**
	 * 初始化
	 */
	private void initView() {
		
		smartMenuView = new HotAnnunciateSmartMenuView(mContext, menuContents);
		setContentView(smartMenuView);
		setWidth(LayoutParams.MATCH_PARENT);
		setHeight(LayoutParams.MATCH_PARENT);
		// 必须设置。改变弹出窗口的背景，如果设置成null，点击空白处pop不消失。
		//menuPw.setBackgroundDrawable(new ColorDrawable(-00000));
		setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(R.color.hot_nation_pop_bg)));
		setOutsideTouchable(true);
		setFocusable(true);

		smartMenuView.setOnTouchListener(onTouchListener);
	}

	/**
	 * 关闭popwindow
	 */
	public void closePopWin() {
		try {
			if (this != null && this.isShowing()) {
				this.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 显示弹出框
	 * 
	 * @param view
	 */
	public void showPopWin(View view) {
		try {
			// 显示pop
			this.showAsDropDown(view, 0, DeviceUtil.dip2px(mContext, 5));
			this.update();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void showPopWin(View view,int x,int y) {
		try {
			// 显示pop
			this.showAsDropDown(view, 0, DeviceUtil.dip2px(mContext, 5));
			this.update();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 设置点击事件监听
	 * @param itemClickListener
	 */
	public void setItemClickListener(OnItemClickListener itemClickListener) {
		this.itemClickListener = itemClickListener;
		
	}
	
	/**
	 * mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
	 */
	private OnTouchListener onTouchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View mMenuView, MotionEvent event) {
			//mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框  
			 int height = mMenuView.findViewById(R.id.gridview_all_layout).getTop();  
            MyLog.d("height="+height);
            int y=(int) event.getY(); 
            MyLog.d("y="+y);
            /*if(event.getAction()==MotionEvent.ACTION_UP){  
                if(y<height){  
                    dismiss();  
                }  
            } */    
            dismiss();
            return true; 
		}
	};
	
}
