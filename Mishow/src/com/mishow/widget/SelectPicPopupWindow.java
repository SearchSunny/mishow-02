package com.mishow.widget;

import com.mishow.R;
import com.mishow.log.MyLog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;

public class SelectPicPopupWindow extends PopupWindow  {
	/** 动态-通知-精选 **/
	private Button btn_main_dynamic, btn_main_annunciate, btn_main_selection;  
    private View mMenuView;  
  
    private Context mContext;
    
    private PopupDismissListener mPopupDismissListener;
    /*public static SelectPicPopupWindow getInstance(Context context){
    	
    	SelectPicPopupWindow menuWindow= new SelectPicPopupWindow(context, null);
    	menuWindow.showAtLocation(((Activity) context).findViewById(R.id.relatvie_index), Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置 
		return menuWindow;
    }*/
    
    public SelectPicPopupWindow(Context context,OnClickListener itemsOnClick,PopupDismissListener dismissListener) {  
        super(context);  
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context  
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
        mMenuView = inflater.inflate(R.layout.alert_pop_dialog, null);  
        btn_main_dynamic = (Button) mMenuView.findViewById(R.id.btn_main_dynamic);  
        btn_main_annunciate = (Button) mMenuView.findViewById(R.id.btn_main_annunciate);  
        btn_main_selection = (Button) mMenuView.findViewById(R.id.btn_main_selection); 
        
        btn_main_dynamic.setOnClickListener(itemsOnClick);
        btn_main_annunciate.setOnClickListener(itemsOnClick);  
        btn_main_selection.setOnClickListener(itemsOnClick);
        
        mPopupDismissListener = dismissListener;
        
        /*btn_main_dynamic.setOnClickListener(new OnClickListener() {  
      	  
            public void onClick(View v) {  
                //销毁弹出框  
                dismiss();  
            }  
        });
        
        btn_main_annunciate.setOnClickListener(new OnClickListener() {  
        	  
            public void onClick(View v) {  
                //销毁弹出框  
                dismiss();  
            }  
        }); 
        //取消按钮  
        btn_main_selection.setOnClickListener(new OnClickListener() {  
  
            public void onClick(View v) {  
                //销毁弹出框  
                dismiss();  
            }  
        });*/  
        //设置SelectPicPopupWindow的View  
        this.setContentView(mMenuView);  
        //设置SelectPicPopupWindow弹出窗体的宽  
        this.setWidth(LayoutParams.MATCH_PARENT);  
        //设置SelectPicPopupWindow弹出窗体的高  
        this.setHeight(LayoutParams.MATCH_PARENT);  
        //设置SelectPicPopupWindow弹出窗体可点击  
        this.setFocusable(true);  
        //设置SelectPicPopupWindow弹出窗体动画效果  
        //this.setAnimationStyle(R.style.AnimBottom);  
        //实例化一个ColorDrawable颜色为半透明  
        ColorDrawable dw = new ColorDrawable(0xb0000000);  
        //设置SelectPicPopupWindow弹出窗体的背景  
        this.setBackgroundDrawable(dw);  
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框  
        mMenuView.setOnTouchListener(new OnTouchListener() {  
              
            public boolean onTouch(View v, MotionEvent event) {  
                  
                int height = mMenuView.findViewById(R.id.pop_layout).getTop();  
                MyLog.d("height="+height);
                int y=(int) event.getY(); 
                MyLog.d("y="+y);
                /*if(event.getAction()==MotionEvent.ACTION_UP){  
                    if(y<height){  
                        dismiss();  
                    }  
                } */    
                dismiss();
                return true;  
            }  
        });  
  
    }  
    
    @Override
    public void dismiss() {
    	super.dismiss();
    	mPopupDismissListener.dismissListener();
    	
    }
    /**
     * 
     * 作者：wei.miao<br/>
     * 描述：PopupView dismiss回调
     */
    public interface PopupDismissListener{
    	/**
    	 * PopupView dismiss回调
    	 */
    	void dismissListener();
    }
}
