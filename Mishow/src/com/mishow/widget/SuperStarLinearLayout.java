package com.mishow.widget;

import com.mishow.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
/**
 * 
 * 作者：wei.miao <br/>
 * 描述：自定义view超星秀场
 */
public class SuperStarLinearLayout extends LinearLayout{

	private Context mContext;
	public SuperStarLinearLayout(Context context) {
		super(context);
		mContext = context;
	}
	
	public SuperStarLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		intiView();
	}
	
	
	private void intiView(){
		
		View view = LayoutInflater.from(mContext).inflate(R.layout.layout_superstar, null);
		
		addView(view);
	}

}
