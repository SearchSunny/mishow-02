package com.mishow.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.mishow.R;
import com.mishow.utils.ToastUtil;

/**
 * 作者：wei.miao <br/>
 * 描述：上传图片对话框
 */
public class UploadImageDialog extends Dialog implements View.OnClickListener {

	/** 拍照按钮 **/
	private Button btnTakePhoto;
	/** 本地照片按钮 **/
	private Button btnPickPhoto;
	/** 取消按钮 **/
	private Button btnCancel;
	
	private View.OnClickListener mOnTakePhotoClickListener;
	private View.OnClickListener mOnPickPhotoClickListener;
	
	
	/**
	 * 上传图片对话框
	 * @param context 上下文环境
	 * @param theme 样式
	 * @param operationType 操作类型(false=照片 true=视频 )
	 */
	public UploadImageDialog(Context context, int theme) {
		super(context, theme);
		initView();
	}
	/**
	 * 拍照
	 * @param onTakePhotoClickListener
	 */
	public void setOnTakePhotoClickListener(View.OnClickListener onTakePhotoClickListener) {
		mOnTakePhotoClickListener = onTakePhotoClickListener;
	}
	/**
	 * 本地图片
	 * @param onPickPhotoClickListener
	 */
	public void setOnPickPhotoClickListener(View.OnClickListener onPickPhotoClickListener) {
		mOnPickPhotoClickListener = onPickPhotoClickListener;
	}
	
	private void initView() {
		View view = getLayoutInflater().inflate(R.layout.picture_popupwindow, null);
		btnTakePhoto = (Button) view.findViewById(R.id.btn_take_photo);
		btnPickPhoto = (Button) view.findViewById(R.id.btn_pick_photo);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		
		setContentView(view);
		Window window = getWindow();
		window.setGravity(Gravity.BOTTOM);
		// 设置dialog占满父容器
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.width = LayoutParams.MATCH_PARENT;
		window.setAttributes(lp);

		// 设置点击外围消散
		setCanceledOnTouchOutside(true);
		
		btnTakePhoto.setOnClickListener(this);
		btnPickPhoto.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_take_photo:// 拍照按钮
			dismiss();
			if (mOnTakePhotoClickListener != null) {
				mOnTakePhotoClickListener.onClick(v);
			}
			break;
		case R.id.btn_pick_photo:// 本地照片按钮
			dismiss();
			if (mOnPickPhotoClickListener != null) {
				mOnPickPhotoClickListener.onClick(v);
			}
			break;
		case R.id.btn_cancel:// 取消按钮
			dismiss();
			break;
		}
	}

}
