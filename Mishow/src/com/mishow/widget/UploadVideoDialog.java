package com.mishow.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.mishow.R;
import com.mishow.utils.ToastUtil;

/**
 * 作者：wei.miao <br/>
 * 描述：上传视频对话框
 */
public class UploadVideoDialog extends Dialog implements View.OnClickListener {

	/** 小视频按钮 **/
	private Button btnTakePhoto;
	/** 优酷链接按钮 **/
	private Button btnPickPhoto;
	/** 取消按钮 **/
	private Button btnCancel;
	
	/** 发布小视频 **/
	private View.OnClickListener mOnPublishClickListener;
	/** 优酷链接 **/
	private View.OnClickListener mOnYouKuLinkClickListener;
	
	/**
	 * 上传视频对话框
	 * @param context 上下文环境
	 * @param theme 样式
	 */
	public UploadVideoDialog(Context context, int theme) {
		super(context, theme);
		initView();
	}
	
	public void setOnPublishClickListener(View.OnClickListener onPublishClickListener) {
		mOnPublishClickListener = onPublishClickListener;
	}

	public void setOnYouKuLinkClickListener(View.OnClickListener onYouKuLinkClickListener) {
		mOnYouKuLinkClickListener = onYouKuLinkClickListener;
	}

	private void initView() {
		View view = getLayoutInflater().inflate(R.layout.picture_popupwindow, null);
		btnTakePhoto = (Button) view.findViewById(R.id.btn_take_photo);
		btnPickPhoto = (Button) view.findViewById(R.id.btn_pick_photo);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
			
		btnTakePhoto.setText("拍小视频");
		btnPickPhoto.setText("发布优酷视频链接");
			
		setContentView(view);
		Window window = getWindow();
		window.setGravity(Gravity.BOTTOM);
		// 设置dialog占满父容器
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.width = LayoutParams.MATCH_PARENT;
		window.setAttributes(lp);

		// 设置点击外围消散
		setCanceledOnTouchOutside(true);
		
		btnTakePhoto.setOnClickListener(this);
		btnPickPhoto.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_take_photo:
			dismiss();
			if (mOnPublishClickListener != null) {
				
				mOnPublishClickListener.onClick(v);
			}
			break;
		case R.id.btn_pick_photo:
			dismiss();
			if (mOnYouKuLinkClickListener != null) {
				
				mOnYouKuLinkClickListener.onClick(v);
			}
			break;
		case R.id.btn_cancel:// 取消按钮
			dismiss();
			break;
		}
	}

}
