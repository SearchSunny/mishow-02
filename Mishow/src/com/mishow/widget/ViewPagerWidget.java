package com.mishow.widget;

import com.mishow.log.MyLog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：重写v4.viewpager
 */
@SuppressLint("ClickableViewAccessibility")
public class ViewPagerWidget extends ViewPager{
	
	private ViewGroup parent; 
	
	private boolean isCanScroll;
	
	public ViewPagerWidget(Context context) {
		super(context);
	}
	
	public ViewPagerWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	//是否禁用掉事件拦截的功能
	//requestDisallowInterceptTouchEvent 如果为true的时候 表示:父view 不拦截子view的touch 事件 
	
	 /**
     * 设置其是否能滑动换页
     * @param isCanScroll false 不能换页， true 可以滑动换页
     */
    public void setScanScroll(boolean isCanScroll) {
        this.isCanScroll = isCanScroll;
    }
	
	public void setNestedpParent(ViewGroup parent) {  
        this.parent = parent;  
    }  
  
    @Override  
    public boolean dispatchTouchEvent(MotionEvent ev) {  
        if (parent != null) {  
            MyLog.d("touch_event_dispatch","---"+ev.getAction());  
        }  
        return super.dispatchTouchEvent(ev);  
    }  
  
    
    /**
	 * 实现此方法可拦截所有触摸屏幕事件。这允许您监听事件传递给你的子类，并且在任何点取得当前手势的所有权
	 */
	@Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
		//当isCanScroll为false时,返回false,否则返回true
		// if isCanScroll=true return super else return isCanScroll
        return isCanScroll && super.onInterceptTouchEvent(ev);
    }
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		return isCanScroll && super.onTouchEvent(event);
	}
  
	
}
