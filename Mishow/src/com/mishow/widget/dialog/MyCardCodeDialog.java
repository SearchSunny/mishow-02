package com.mishow.widget.dialog;

import com.google.zxing.WriterException;
import com.mishow.R;
import com.mishow.bean.ScanCodeBean;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.CodeConstants;
import com.mishow.utils.DeviceUtil;
import com.mishow.utils.EncodingHandler;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：我的名片
 */
public class MyCardCodeDialog extends Dialog implements android.view.View.OnClickListener{

    private Context context;
    private View convertView;
    private ImageView qrcode_image;
    private String pickGoodNo;

    public MyCardCodeDialog(Context context,String cardCode) {
        super(context, R.style.ShareDialog);
        this.context = context;
        pickGoodNo = cardCode;
        initView();
    }

    public void initView() {
        convertView = LayoutInflater.from(context).inflate(R.layout.dialog_qrcode_ziti, null);
        qrcode_image = (ImageView) convertView.findViewById(R.id.qrcode_image);
      
        setContentView(convertView, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT));
        Window window = getWindow();
        // 设置显示动画
        window.setWindowAnimations(R.style.shareAnimation);
        window.setGravity(Gravity.CENTER);
        // 设置dialog占满父容器
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = (int) (DeviceUtil.getDeviceWidth(context) * 4 / 5);
        window.setAttributes(lp);

        try {
            //二维码类型  提货码
            ScanCodeBean bean = new ScanCodeBean(CodeConstants.ORDER_CODE_TYPE, pickGoodNo);
            String kkmyqrCode = AndroidUtil.getScanCodeUrl(bean);
            Bitmap qrCodeBitmap = EncodingHandler.createQRCode(kkmyqrCode,
                    DeviceUtil.dip2px(context, 210));
            qrcode_image.setImageBitmap(qrCodeBitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

       
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.qrcode_image:
                MyCardCodeDialog.this.dismiss();
                break;

            default:
                break;
        }
    }


}
