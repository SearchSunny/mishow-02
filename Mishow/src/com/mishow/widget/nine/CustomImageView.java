package com.mishow.widget.nine;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
/**
 * 
 * 作者：wei.miao<br/>
 * 描述：自定义ImageView,实现图片在点击的时候是有一个灰色的蒙版
 * 复写onTouchEvent方法，在onKeyDown的时候添加一个colorfilter，然后再onKeyUp的时候clear掉
 */
public class CustomImageView extends ImageView {

	private String url;
	private boolean isAttachedToWindow;
	private Context mContext;

	public CustomImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
	}

	public CustomImageView(Context context) {
		super(context);
		this.mContext = context;
	}
	
	
	public String getUrl(){
		
		return this.url;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			Drawable drawable = getDrawable();
			if (drawable != null) {
				drawable.mutate().setColorFilter(Color.GRAY,PorterDuff.Mode.MULTIPLY);
			}
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			Drawable drawableUp = getDrawable();
			if (drawableUp != null) {
				drawableUp.mutate().clearColorFilter();
			}
			break;
		}

		return super.onTouchEvent(event);
	}

	@Override
	public void onAttachedToWindow() {
		isAttachedToWindow = true;
		setImageUrl(url);
		super.onAttachedToWindow();
	}

	@Override
	public void onDetachedFromWindow() {
		Picasso.with(mContext).cancelRequest(this);
		isAttachedToWindow = false;
		setImageBitmap(null);
		super.onDetachedFromWindow();
	}

	public void setImageUrl(String url) {
		if (!TextUtils.isEmpty(url)) {
			this.url = url;
			if (isAttachedToWindow) {
				Picasso.with(mContext)
						.load(url)
						.placeholder(new ColorDrawable(Color.parseColor("#f5f5f5")))
						.into(this);
			}
		}
	}
}
