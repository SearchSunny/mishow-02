package com.mishow.widget.share;

import com.mishow.R;
import com.mishow.widget.share.ShareUtil.ShareListener;

import android.app.Activity;
import android.app.Dialog;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：分享自定义Dialog
 */
public class ShareDialog extends Dialog implements OnClickListener{
	
	protected View shareView;
	private ShareView shareLayout;// 分享布局
	private Button cancelBtn;//取消按钮
	
	public ShareDialog(Activity context, int type) {
		super(context, R.style.ShareDialog);
		initView();
		shareLayout.initShareData(type, context);
	}

	// 初始化控件
	private void initView() {
		shareView = getLayoutInflater().inflate(R.layout.share_dialog, null);
		shareLayout = (ShareView)shareView.findViewById(R.id.share_view);
		
		cancelBtn = (Button) shareView.findViewById(R.id.cancel_btn);
		setContentView(shareView, new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		Window window = getWindow();
		// 设置显示动画
		window.setWindowAnimations(R.style.shareAnimation);
		window.setGravity(Gravity.BOTTOM);
		// 设置dialog占满父容器
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.width = LayoutParams.MATCH_PARENT;
		window.setAttributes(lp);

		// 设置点击外围消散
		setCanceledOnTouchOutside(true);
		
		cancelBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.cancel_btn ://取消按钮
				this.cancel();
				break;

			default :
				break;
		}
	}

	/**
	 * 设置分享的内容和图片
	 * @param titleStr 标题
	 * @param content 内容
	 * @param shareUrl 分享点击后的链接
	 * @param 
	 */
	public void setShareContent(String titleStr, String content, String shareUrl,String imgPath) {
		shareLayout.setShareContent(titleStr, content, shareUrl, imgPath);
	}
	
	/**
	 * 设置分享回调监听
	 * @param shareListener
	 */
	public void setShareListener(ShareListener shareListener) {
		shareLayout.setShareListener(shareListener);
	}
}
