package com.mishow.widget.share;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import android.app.Activity;
import android.text.TextUtils;

public class ShareUtil {

	/** 分享成功 **/
	public final static int SHARE_SUCCESS = 0;
	/** 分享失败 **/
	public final static int SHARE_FAILED = 1;
	/** 分享取消 **/
	public final static int SHARE_CANCEL = 2;
	/** 上下文对象 */
	private Activity mContext;
	/**
	 * 友盟分享图片
	 */
	private UMImage shareImage;
	/**
	 * 分享的标题
	 */
	private String shareTitle = "";
	/**
	 * 分享的内容
	 */
	private String shareContent = "";
	/**
	 * 分享的连接
	 */
	private String shareUrl = "";

	/**
	 * 分享结果回调
	 */
	private ShareListener shareListener;


	public interface ShareListener {
		
		public void shareCallBack(SHARE_MEDIA platform, int result,String message);
	}

	/**
	 * 注意：sso登录进入分享授权， 传入参数必须为Activity实例
	 * @param mContext
	 */
	public ShareUtil(Activity mContext, int shareIcon, String wxAppId, String wxAppSecret) {
		this.mContext = mContext;
		initUmengShare(shareIcon, wxAppId, wxAppSecret);
	}

	/**
	 * 初始化分享
	 */
	private void initUmengShare(int shareIcon, String wxAppId, String wxAppSecret) {
		
		shareImage = new UMImage(mContext, shareIcon);
		
	}

	/**
	 * 设置分享的内容和图片
	 * 
	 * @param titleStr
	 *            标题
	 * @param content
	 *            内容
	 * @param shareUrl
	 *            分享点击后的链接
	 * @param
	 */
	public void setShareContent(String titleStr, String content, String shareUrl, String imgPath) {
		this.shareTitle = titleStr;
		this.shareContent = content;
		if (!TextUtils.isEmpty(imgPath)) {
			shareImage = new UMImage(mContext, imgPath);
		}
		if (!TextUtils.isEmpty(shareUrl)) {
			this.shareUrl = shareUrl;
		}
	}

	/**
	 * 设置微信平台的分享内容
	 */
	public void shareWx() {
		
		new ShareAction(mContext).setPlatform(SHARE_MEDIA.WEIXIN)
        .withText(shareTitle)
        .withMedia(shareImage)
        .withText(shareContent)
        .setCallback(umShareListener)
        .share();

	}

	/**
	 * 设置朋友圈的分享内容
	 */
	public void shareWxCircle() {
		
		new ShareAction(mContext).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE)
        .withText(shareTitle)
        .withMedia(shareImage)
        .withText(shareContent)
        .setCallback(umShareListener)
        .share();
	}

	/**
	 * 设置短信的分享内容
	 */
	public void shareSms() {
		
		new ShareAction(mContext).setPlatform(SHARE_MEDIA.SMS)
        .withText(shareTitle)
        .withMedia(shareImage)
        .withText(shareContent)
        .setCallback(umShareListener)
        .share();
	}
	
	/**
	 * 设置QQ空间分享
	 */
	public void shareQQZone() {
		
		new ShareAction(mContext).setPlatform(SHARE_MEDIA.QZONE)
        .withText(shareTitle)
        .withMedia(shareImage)
        .withText(shareContent)
        .setCallback(umShareListener)
        .share();
	}

	/**
	 * 设置新浪的分享内容
	 */
	public void shareSina() {
		
		UMWeb sinoWeb = new UMWeb(shareUrl);
		sinoWeb.setTitle(shareTitle);//标题
		sinoWeb.setThumb(shareImage);  //缩略图
		sinoWeb.setDescription(shareContent);//描述
		
		new ShareAction(mContext).setPlatform(SHARE_MEDIA.SINA)
        .setCallback(umShareListener)
        .withMedia(sinoWeb)
        .share();
		
		
	}
	/**
	 * 设置腾讯微博的分享内容
	 */
	public void shareTenWeibo() {
		
		new ShareAction(mContext).setPlatform(SHARE_MEDIA.TENCENT)
        .withText(shareTitle)
        .withMedia(shareImage)
        .withText(shareContent)
        .setCallback(umShareListener)
        .share();
	}
	/**
	 * 设置qq的分享内容
	 */
	public void shareTenQQ() {
		
		new ShareAction(mContext).setPlatform(SHARE_MEDIA.QQ)
        .withText(shareTitle)
        .withMedia(shareImage)
        .withText(shareContent)
        .setCallback(umShareListener)
        .share();
	}

	/**
	 * 分享结果回调
	 */
	private void shareCallBack(SHARE_MEDIA platform, int result,String message) {
		if (shareListener == null) {
			return;
		}
		shareListener.shareCallBack(platform, result,message);
	}

	/**
	 * 设置分享回调监听
	 * 
	 * @param shareListener
	 */
	public void setShareListener(ShareListener shareListener) {
		
		this.shareListener = shareListener;
		
	}
	
	private UMShareListener umShareListener = new UMShareListener() {
		
		@Override
		public void onStart(SHARE_MEDIA arg0) {
			
		}
		 //分享成功啦
		@Override
		public void onResult(SHARE_MEDIA platform) {
			
			shareCallBack(platform, SHARE_SUCCESS,"分享成功");
		}
		//分享失败啦
		@Override
		public void onError(SHARE_MEDIA platform, Throwable arg1) {
			try {
				shareCallBack(platform, SHARE_FAILED,arg1.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		//分享取消了
		@Override
		public void onCancel(SHARE_MEDIA platform) {
			shareCallBack(platform, SHARE_CANCEL,"分享取消");
		}
		
	};

}
