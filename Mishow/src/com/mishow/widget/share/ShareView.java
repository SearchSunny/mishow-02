package com.mishow.widget.share;



import com.mishow.R;
import com.mishow.utils.AndroidUtil;
import com.mishow.utils.Constants;
import com.mishow.widget.share.ShareUtil.ShareListener;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

/**
 * 
 * 作者：wei.miao <br/>
 * 描述：分享控件布局
 */
public class ShareView extends LinearLayout implements OnClickListener{
	
	/** 分享类型 **/
	public final static int TYPE_SHARE_MEDICINE = 0;
	public final static int TYPE_SHARE_ORDER = 1;
	public final static int TYPE_SHARE_APP = 2;
	public final static int TYPE_WEB_SHARE = 3;
	
	protected View shareView;
	
	/** 密秀好友圈分享 **/
	protected Button shareToMishow;
	
	/** 朋友圈分享 **/
	protected Button shareToCircle;
	
	/** 微信分享 **/
	protected Button shareToWx;
	
	/** qq空间 **/
	protected Button shareToZone;
	
	/** 新浪微博分享 **/
	protected Button shareToSina;
	/** 腾讯微博 **/
	protected Button shareToTenweibo;
	/** 腾讯qq **/
	protected Button shareToTenqq;

	//上下文对象
	private Context mContext;
	// 分享的标题
	private String shareTitle = "";
	// 分享的内容
	private String shareContent = "";
	
	//分享工具类
	private ShareUtil shareUtil;
	
	public ShareView(Context context) {
		super(context);
		this.mContext = context;
		initView(context);
	}

	public ShareView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
		initView(context);
	}

	// 初始化数据
	public void initShareData(int type, Activity activity) {
		shareUtil = new ShareUtil(activity, R.drawable.ic_launcher, Constants.WB_APPID, Constants.WB_APPSECRET);
		// 初始化分享的内容
		getShareContent(type);
	}

	// 获取分享的内容
	private void getShareContent(int type) {
		switch (type) {
		case TYPE_SHARE_MEDICINE:// 药品分享
			shareContent = "测试分享内容";
			shareTitle = "测试分享标题";
			break;
		case TYPE_SHARE_ORDER:// 订单分享
			
			break;
		case TYPE_SHARE_APP:// 产品分享
			shareContent = "测试分享内容";
			shareTitle = "测试分享标题";
			break;
		case TYPE_WEB_SHARE:
		default:
			break;
		}
		shareUtil.setShareContent(shareTitle, shareContent, null, null);
	}

	// 初始化控件
	private void initView(Context context) {
		shareView = LayoutInflater.from(context).inflate(R.layout.share_layout, null);
		
		shareToMishow = (Button) shareView.findViewById(R.id.shareToMishow);
		shareToCircle = (Button) shareView.findViewById(R.id.shareToCircle);
		shareToWx = (Button) shareView.findViewById(R.id.shareToWx);
		shareToZone = (Button) shareView.findViewById(R.id.shareToZone);
		
		shareToSina = (Button) shareView.findViewById(R.id.shareToSina);
		shareToTenweibo = (Button) shareView.findViewById(R.id.shareToTenweibo);
		shareToTenqq = (Button) shareView.findViewById(R.id.shareToTenqq);
		
		shareToMishow.setOnClickListener(this);
		shareToCircle.setOnClickListener(this);
		shareToWx.setOnClickListener(this);
		shareToZone.setOnClickListener(this);
		
		shareToSina.setOnClickListener(this);
		shareToTenweibo.setOnClickListener(this);
		shareToTenqq.setOnClickListener(this);
		
		addView(shareView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.shareToMishow://密秀好友圈
			Toast.makeText(mContext, "好友圈", Toast.LENGTH_SHORT).show();
			break;
		case R.id.shareToCircle:// 朋友圈分享
			// 判断网络是否连接
			if (!AndroidUtil.isNetworkAvailable(mContext)) {
				Toast.makeText(mContext, R.string.no_connector, Toast.LENGTH_SHORT).show();
				return;
			}
			shareUtil.shareWxCircle();
			break;
		case R.id.shareToWx:// 微信分享
			// 判断网络是否连接
			if (!AndroidUtil.isNetworkAvailable(mContext)) {
				Toast.makeText(mContext, R.string.no_connector, Toast.LENGTH_SHORT).show();
				return;
			}
			shareUtil.shareWx();
			break;
		case R.id.shareToZone://qq空间
			shareUtil.shareQQZone();
			break;
		case R.id.shareToSina:// 新浪分享
			// 判断网络是否连接
			if (!AndroidUtil.isNetworkAvailable(mContext)) {
				Toast.makeText(mContext, R.string.no_connector, Toast.LENGTH_SHORT).show();
				return;
			}
			shareUtil.shareSina();
			break;
		case R.id.shareToTenweibo://腾讯微博
			shareUtil.shareTenWeibo();
			break;
		case R.id.shareToTenqq://腾讯qq
			shareUtil.shareTenQQ();
			break;
		default:
			break;
		}
	}

	/**
	 * 设置分享的内容和图片
	 * @param titleStr 标题
	 * @param content 内容
	 * @param shareUrl 分享点击后的链接
	 * @param 
	 */
	public void setShareContent(String titleStr, String content, String shareUrl, String imgPath) {
		shareUtil.setShareContent(titleStr, content, shareUrl, imgPath);
	}
	
	/**
	 * 设置分享回调监听
	 * @param shareListener
	 */
	public void setShareListener(ShareListener shareListener) {
		shareUtil.setShareListener(shareListener);
	}
	

}
